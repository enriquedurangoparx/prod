# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* This repository is to test a custom CI / CD solution, developed with Bitbucket Pipelines + Metadata API + Docker
* Version 1.0


### How do I get set up? ###

Follow the documentation under [CI / CD with Bitbucket Pipelines + Metadata API + Docker](https://etrajo.atlassian.net/wiki/spaces/KB/pages/610140176/CI+CD+with+Bitbucket+Pipelines+Metadata+API+Docker)



### Who do I talk to? ###

* Repo owner or admin
Enrique Durango (enrique.durango@parx.com)