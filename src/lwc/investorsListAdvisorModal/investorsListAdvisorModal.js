import { LightningElement, api, track, wire } from 'lwc';
import { fireEvent } from 'c/pubsub';
import { CurrentPageReference } from 'lightning/navigation';
import updateInvestorsCall from '@salesforce/apex/InvestorsListController.updateInvestorsPriority';
import lookupSearch from '@salesforce/apex/LookupController.search';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';

import advisor from '@salesforce/label/c.InvestorsListAdvisorUpdate';
import advisorUpdateHeading from '@salesforce/label/c.InvestorsListAdvisorUpdateHeading';
import investorsAdvisorUpdated from '@salesforce/label/c.InvestorsListAdvisorUpdatedSuccess';
import close from '@salesforce/label/c.InvestorsListCloseLabel';
import cancel from '@salesforce/label/c.investorsListCancelLabel';
import update from '@salesforce/label/c.investorsListUpdateLabel';
import toastTitle from '@salesforce/label/c.InvestorsListPriorityModalToastTitle';

export default class InvestorsListAdvisorModal extends LightningElement {
    @wire(CurrentPageReference) pageRef;

    label = {
        close,
        cancel,
        update,
        toastTitle,
        toastMessage: "",
        advisor,
        advisorUpdateHeading,
        investorsAdvisorUpdated
    };

    @track options;
    @track value = '';
    @track showModal = false;
    @api recordsToUpdate;

    userField = {objectApiName: "AccountToOpportunity__c", apiName: "Advisor__c", isMultiEntry: false};
    selectedUserId = '';
    contactId = '';

    renderedCallback(){
        this.selectedUserId = '';
    }

    handleChange(event){
        this.value = event.target.value;
    }

    @api
    openModal(recordsToUpdate) {
        this.recordsToUpdate = recordsToUpdate;
        this.showModal = true;
    }

    closeModal() {
        this.showModal = false;
    }

    async updateInvestors(){
        let investors = [];
        for(let i=0; i<this.recordsToUpdate.length; i++){
            investors.push({Id: this.recordsToUpdate[i].Id, Advisor__c: this.selectedUserId});
        }
        // call update function
        const updateInvestorsResult = await updateInvestorsCall({
            investors : investors
        });

        const event = CustomEvent('investorsupdated', {
            composed: true,
            bubbles: true,
            cancelable: true,
            detail: {
                toastTitle: this.label.investorsAdvisorUpdated,
                toastVariant: 'success'
            },
        });
        this.dispatchEvent(event);
        this.closeModal();
    }

	handleUsersLookupSearch(event) {
        var searchParam = {
            parentObjectApiName: this.userField.objectApiName,
            lookupApiName: this.userField.apiName,
            searchTerm: event.detail.searchTerm
        };

        lookupSearch(searchParam)
            .then(results => {
                this.template
                    .querySelector('[data-id="nameSearchInput"]')
                    .setSearchResults(results);
            })
            .catch(error => {
                this.notifyUser(
                    'Lookup Error',
                    'An error occured while searching with the lookup field.\n' +
                        reduceErrors(error).join(', '),
                    'error'
                );
            });
    }

    handleUserSelectionChange(event){
        this.selectedUserId = '';
        if(event.target.selection && event.target.selection.length > 0) {
            this.selectedUserId = event.target.selection[0].id;
        }
    }

    notifyUser(title, message, variant) {
        const toastEvent = new ShowToastEvent({
            title,
            message,
            variant
        });
        this.dispatchEvent(toastEvent);
    }
}