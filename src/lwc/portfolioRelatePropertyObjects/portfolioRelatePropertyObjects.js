import { LightningElement, api, track, wire } from 'lwc';
import { getObjectInfo, getPicklistValues  } from 'lightning/uiObjectInfoApi';
import PROPERTY_OBJECT from '@salesforce/schema/PropertyObject__c';
import STATUS_FIELD from '@salesforce/schema/PropertyObject__c.Status__c';

import searchRelatedObjectsBySosl from '@salesforce/apex/LWCObjectHelper.searchRelatedObjectsBySosl';

import addToPortfolio from '@salesforce/label/c.PortfolioAddToPortfolio';
import searchPlaceholder from '@salesforce/label/c.PortfolioSearch';

export default class PortfolioRelatePropertyObjects extends LightningElement {
    @api recordId;
    
    @track objectInfo;

    @track data = [];
    @track searchTerm = '';
    @track columns = [
        { label: 'Name', fieldName: 'Name' },
        { label: 'Street', fieldName: 'Street__c' },
        { label: 'Postalcode', fieldName: 'PostalCode__c' },
        { label: 'City', fieldName: 'City__c' },
        { label: 'Status', fieldName: 'Status__c' },
    ];

    @track selectedRows = [];
    label = {
        addToPortfolio,
        searchPlaceholder,
    };
    
    @wire(getObjectInfo, { objectApiName: PROPERTY_OBJECT })
    wiredObjectInfo({ error, data }) {
        console.log('>>>PortfolioRelatePropertyObjects -- wiredObjectInfo.');

        if (data) {
            //console.log('>>>data: ' + JSON.stringify(data, null, '\t'));
            this.objectInfo = data;
            this.error = undefined;
        } else if (error) {
            this.error = error;
            //this.contacts = undefined;
        }
    }

    @wire(getPicklistValues, { recordTypeId: '012000000000000AAA', fieldApiName: STATUS_FIELD })
    wiredPicklistValues({ error, data }) {
        console.log('>>>AreaRelatePropertyObjects -- wiredPicklistValues.');

        if(data) {
            this.statusPicklistValues = data;
            console.log('>>>data: ' + JSON.stringify(data, null, '\t'));
            this.setDatableColumns();
            this.error = undefined;
        } else if(error) {
            console.error('>>>error: ' + JSON.stringify(error, null, '\t'));

            this.error = error;
        }
    }

    connectedCallback(){
        console.log('>>>PortfolioRelatePropertyObjects -- connectedCallback.');
        console.log('>>>this.recordId: ' + JSON.stringify(this.recordId, null, '\t'));

    }

    renderedCallback(){
        console.log('>>>PortfolioRelatePropertyObjects -- renderedCallback.');
        //console.log('>>>this.objectInfo: ' + JSON.stringify(this.objectInfo, null, '\t'));

    }

    /*
    * Set the columns for datatable, based on the object metadata to prevent custom labels. 
    */
    setDatableColumns(){
        console.log('>>>PortfolioRelatePropertyObjects -- setDatableColumns.');

        if(this.objectInfo){
            let tempColumns = [];

            this.columns.forEach(column => {
                column.label = this.objectInfo.fields[column.fieldName].label;
                tempColumns.push(column);
            });
            this.columns = tempColumns;
        }
        
    }
    // Initialize 
    /*@wire(searchRelatedObjectsBySosl, {searchTerm: '$searchTerm'})
    wiredRelatedObjects({ error, data }) {
        if (data) {
            this.data = data[0];
            this.error = undefined;
        } else if (error) {
            this.error = error;
            this.data = undefined;
        }
    }*/

    /**
     * This is triggered when the user hits a key and is filtered by enter key.
     */
    keyUpHandler(event){
        console.log('>>>PortfolioRelatePropertyObjects -- keyUpHandler.');
        //const value = event.value;
        //console.log('>>>event.details: ' + JSON.stringify(event.detail, null, '\t'));

        this.searchTerm = this.template.querySelector('lightning-input').value;
        //console.log('>>>this.searchTerm: ' + JSON.stringify(this.searchTerm, null, '\t'));

        const isEnterKey = event.keyCode === 13;

        if (isEnterKey) {
            this.loadData();
        }
    }

    rowSelectionHandler(event){
        const selectedRows = event.detail.selectedRows;
        this.selectedRows = [];

        selectedRows.forEach(row => {
            this.selectedRows.push(row.Id);
        })
        console.log('>>>this.selectedRows: ' + JSON.stringify(this.selectedRows, null, '\t'));
    }

    /**
     * This is triggered every time the input change.
     */
    /*inputChangeHandler(event){
        console.log('>>>PortfolioRelatePropertyObjects -- inputChangeHandler.');
        console.log('>>>event.details: ' + JSON.stringify(event.detail, null, '\t'));

        this.searchTerm = value;
    }*/

    /*
    * Load records, based on the searchTermn with a sosl.
    */
    loadData(){
        console.log('>>>PortfolioRelatePropertyObjects -- loadData.');
        //console.log('>>>this.searchTerm: ' + JSON.stringify(this.searchTerm, null, '\t'));

        searchRelatedObjectsBySosl({searchTerm: this.searchTerm})
            .then(result => {
                //console.log('>>>result: ' + JSON.stringify(result, null, '\t'));
                this.data = JSON.parse(JSON.stringify(result[0]));
                console.log('>>>this.data: ' + JSON.stringify(this.data, null, '\t'));

                this.convertPicklistValue();
            })
            .catch(error => {
                console.error('>>>error: ' + JSON.stringify(error, null, '\t'));
                this.error = error;
            });
        }

    /**
     * Handle translation for picklist value.
     */
    convertPicklistValue(){
        let tempData = [];
        this.data.forEach(record => {

            if(typeof record.Status__c !== 'undefined'){
                const picklistApiName = record.Status__c;

                this.statusPicklist.values.forEach(picklist => {
                    if(picklistApiName === picklist.value){
                        record.Status__c = picklist.label;
                    }
                })
            }

            tempData.push(record);
        });

        this.data = tempData;
    }
}