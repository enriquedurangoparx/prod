import { LightningElement, api, track, wire } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import { updateRecord } from 'lightning/uiRecordApi';
import { CurrentPageReference, NavigationMixin } from 'lightning/navigation';
import { fireEvent } from 'c/pubsub';

import updateObjectsRelation from '@salesforce/apex/LWCObjectHelper.updateObjectsRelation';

import generalLabelCancel from '@salesforce/label/c.GeneralLabelCancel';
import generalLabelClose from '@salesforce/label/c.GeneralLabelClose';
import generalLabelError from '@salesforce/label/c.GeneralLabelError';
import generalLabelSuccess from '@salesforce/label/c.GeneralLabelSuccess';
import generalLabelSuccessUpdateMessage from '@salesforce/label/c.PortfolioAreaRelatedObjectSuccessMessage';
import generalLabelSuccessDeleteMessage from '@salesforce/label/c.PortfolioAreaRelatedObjectSuccessDeletion';
import generalLabelConfirm from '@salesforce/label/c.GeneralLabelConfirm';
import generalLabelUnknownError from '@salesforce/label/c.GeneralLabelUnknownError';
import relatedObjectsConfirmModalHeader from '@salesforce/label/c.RelatedObjectsConfirmModalHeader';
import relatedObjectsClearRelationConfirmMessage from '@salesforce/label/c.RelatedObjectsClearRelationConfirmMessage';
import relatedObjectsAddRelationConfirmMessage from '@salesforce/label/c.RelatedObjectsAddRelationConfirmMessage';

export default class RelatedObjectsConfirmModal extends NavigationMixin(LightningElement)  
{
    @wire(CurrentPageReference) pageRef;

    @track data;
    @track showModal = false;
    @track isLoading = false;
    @track confirmButtonDisabled = false;
    
    /**
     * Obj params:
     * isDeleteMode
     * data
     * recordToDeleteRelation
     * currentRecordName
     * objectApiName
     */
    @track obj;

    label = {
        generalLabelCancel,
        generalLabelClose,
        generalLabelError,
        generalLabelSuccess,
        generalLabelSuccessUpdateMessage,
        generalLabelConfirm,
        generalLabelSuccessDeleteMessage,
        relatedObjectsConfirmModalHeader,
        generalLabelUnknownError,
        relatedObjectsClearRelationConfirmMessage,
        relatedObjectsAddRelationConfirmMessage
    }

    portfolioApiName = 'Portfolio__c';
    areaApiName = 'Area__c';

    @api
    openModal(obj) {
        // this.data = obj;
        this.obj = obj;

        if (!obj.isDeleteMode)
        {
            let tempData = [];
            obj.data.forEach(function(value, key) {
                tempData.push(value);
            });
            this.data = tempData;
        }
        this.showModal = true;
    }

    closeModal() {
        this.obj = {};
        this.data = [];
        this.showModal = false;
    }

    get confirmButtonLabel()
    {
        return this.obj.isDeleteMode ? 'Confirm' : 'Update';
    }

    get helperMessageLabel()
    {
        // ;
        let label = '';
        if (this.obj.isDeleteMode)
        {
            let tempArray = [this.obj.data.Name, this.obj.objectLabel, this.obj.currentRecordName];
            label = this.formatLabel(this.label.relatedObjectsClearRelationConfirmMessage, tempArray);
        }
        else
        {
            let tempArray = [this.obj.objectLabel, this.obj.currentRecordName];
            label = this.formatLabel(this.label.relatedObjectsAddRelationConfirmMessage, tempArray);
        }
        
        return label;
    }
    
    handleConfirm()
    {
        this.isLoading = true;
        this.confirmButtonDisabled = true;

        if (this.obj.isDeleteMode)
        {
            const fieldsToUpdate = {};
            fieldsToUpdate['Id'] = this.obj.recordToDeleteRelation;
            if (this.obj.objectApiName === this.areaApiName)
            {
                fieldsToUpdate['Area__c'] = null;
            }
            else if (this.obj.objectApiName === this.portfolioApiName)
            {
                fieldsToUpdate['Portfolio__c'] = null;
            }
            const recordToUpdate = { fields: fieldsToUpdate};

            updateRecord(recordToUpdate)
            .then(() => {
                fireEvent(this.pageRef, 'mapcomponent_refreshInitialData', null);

                this.isLoading = false;
                this.confirmButtonDisabled = false;

                const evnt = CustomEvent('recordsupdate', {
                    detail: {
                        isDeleteMode: true,
                    },
                });
                this.dispatchEvent(evnt);

                this.dispatchEvent(
                    new ShowToastEvent({
                        title: this.label.generalLabelSuccess,
                        message: this.formatLabel(this.label.generalLabelSuccessDeleteMessage, [this.obj.objectLabel]),
                        variant: 'success',
                    }),
                );
                this.closeModal();
                
            })
            .catch(error => {
                this.isLoading = false;
                this.confirmButtonDisabled = false;
                if (error.body 
                    && error.body.output 
                    && error.body.output.errors 
                    && error.body.output.errors.length > 0
                    && error.body.output.errors[0].errorCode 
                    && error.body.output.errors[0].message)
                {
                    this.notifyUser(
                        this.label.generalLabelError,
                        error.body.output.errors[0].errorCode + ': ' + error.body.output.errors[0].message,
                        'error'
                    );
                }
                else
                {
                    this.notifyUser(this.label.generalLabelError, generalLabelUnknownError, 'error');
                }
            });
        }
        else
        {
            let params = {
                objects: this.data,
                recordId: this.obj.recordId,
                objApiName: this.obj.objectApiName
            }
            updateObjectsRelation(params)
            .then(results => {
                fireEvent(this.pageRef, 'mapcomponent_refreshInitialData', null);

                this.response = results;
                if (results === 'success')
                {
                    this.isLoading = false;
                    this.confirmButtonDisabled = false;
                    const evnt = CustomEvent('recordsupdate', {
                        detail: {
                            isDeleteMode: false,
                        },
                    });
                    this.dispatchEvent(evnt);
    
                    this.dispatchEvent(
                        new ShowToastEvent({
                            title: this.label.generalLabelSuccess,
                            message: this.formatLabel(this.label.generalLabelSuccessUpdateMessage, [this.obj.objectLabel]),
                            variant: 'success',
                        }),
                    );
                    this.closeModal();
                }
                else
                {
                    this.isLoading = false;
                    this.confirmButtonDisabled = false;
                    this.notifyUser(this.label.generalLabelError, results, 'error');
                }
            })
        }
    }

    formatLabel(labelToFormat, arrayOfLabels)
    {
        let tempLabel = labelToFormat;
        arrayOfLabels.forEach(function (value, i) {
            tempLabel = tempLabel.replace(new RegExp("\\{" + i + "\\}", 'g'), value);
        });
        return tempLabel;
    }

    /**
     * notify user
     * @param {string} title
     * @param {string} message
     * @param {string} variant
     */
    notifyUser(title, message, variant) {
        const toastEvent = new ShowToastEvent({
            title,
            message,
            variant
        });
        this.dispatchEvent(toastEvent);
    }
}