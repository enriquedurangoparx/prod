import { LightningElement, wire, track } from 'lwc';
import search from '@salesforce/apex/GenericSearch.search';
import { CurrentPageReference} from'lightning/navigation';
import { registerListener, unregisterAllListeners } from 'c/pubsub';

import filesLabel from '@salesforce/label/c.FileListAllRecords';
import title from '@salesforce/label/c.FileListAllRecordsTitle';
import lastModified from '@salesforce/label/c.FileListAllRecordsLastModified';
import owner from '@salesforce/label/c.FileListAllRecordsOwner';
import fileType from '@salesforce/label/c.FileListAllRecordsFileType';
import size from '@salesforce/label/c.FileListAllRecordsSize';
import category from '@salesforce/label/c.FileListAllRecordsCategory';


export default class FileListAllRecords extends LightningElement {
    @wire(CurrentPageReference) pageRef;
    recordId;
    @track sortBy;
    @track sortDirection;
    @track fileData;
    columns = [{label: title, fieldName: 'fileId',  type: 'url', typeAttributes: { label: {fieldName: 'Title'}, target:'_blank'}, sortable: true},
                {label: category, fieldName: 'category', type: 'text', sortable: true },
                {label: lastModified, fieldName: 'LastModifiedDate', type: 'date', sortable: true },
                {label: owner, fieldName: 'ownerId',  type: 'url', typeAttributes: { label: {fieldName: 'ownerName'}, target:'_blank'}, sortable: true},
                {label: fileType, fieldName: 'FileType', type: 'text', sortable: true },
                {label: size, fieldName: 'size', type: 'text', sortable: true },
                {label: '', type: 'actionButtonRow', fixedWidth: 36, fieldName: 'Id', typeAttributes: { action: 'deleteFile' }},
                {label: '', type: 'actionButtonRow', fixedWidth: 36, fieldName: 'Id', typeAttributes: { action: 'details'}}
                ];

    label = {
        filesLabel
    }

    connectedCallback() {
        this.recordId = this.pageRef.state.c__recordId;
        this.getContentDocumentIds(this.recordId);
        registerListener('FileListAllRecords_refreshList', this.refreshList, this);
    }

    getContentDocumentIds(parentId){
        search({
            list_fieldApiName: ['ContentDocumentId'],
            string_objectApiName: 'ContentDocumentLink',
            string_searchTerm: parentId,
            string_whereApiName: 'LinkedEntityId',
            string_filter : null,
            string_soqlOperator: '=',
            string_orderBy: 'ContentDocument.CreatedDate DESC',
            integer_limit: 100,
            integer_offset: 0
        })
        .then(result => {
            let contentDocumentIds = '';
            result.forEach(element => {
                contentDocumentIds += element.ContentDocumentId + ',';
            });
            if(contentDocumentIds != ''){
                contentDocumentIds = contentDocumentIds.substring(0, contentDocumentIds.length - 1);
            }
            this.getContentDocuments(contentDocumentIds);
        }).catch(error => {
            console.error(JSON.stringify(error, null, '\t'));
        })
    }

    getContentDocuments(contentDocumentIds){
        search({
            list_fieldApiName: ['Id', 'Title', 'LastModifiedDate', 'OwnerId','Owner.Name', 'FileType', 'ContentSize', '(SELECT Category__c FROM ContentVersions Limit 1)'],
            string_objectApiName: 'ContentDocument',
            string_searchTerm: contentDocumentIds,
            string_whereApiName: 'ID',
            string_filter : '',
            string_soqlOperator: 'IN',
            string_orderBy: 'CreatedDate DESC',
            integer_limit: 1000,
            integer_offset: 0
        })
        .then(result => {
            const tempResult = JSON.parse(JSON.stringify(result));
            let _fileData = this._preprocessData(tempResult);
            this.fileData = JSON.parse(JSON.stringify(_fileData));

        }).catch(error => {
            console.error(JSON.stringify(error, null, '\t'));
        })
    }

    _preprocessData(result){
        let preprocessData = [];
        result.forEach(element => {
            element.fileId = '/' + element.Id;
            element.ownerId = '/' + element.OwnerId;
            element.ownerName = element.Owner.Name;

            element.size = (element.ContentSize / 1000000) + ' MB';

            if(typeof element.ContentVersions[0].Category__c !== 'undefined'){
                element.category = element.ContentVersions[0].Category__c;
            }

            preprocessData.push(element);
        });

       return preprocessData;
    }

    updateColumnSorting(event) {
        this.sortBy = event.detail.fieldName;
        this.sortDirection = event.detail.sortDirection;
        this.sortData(this.sortBy, this.sortDirection);
    }

    sortData(fieldname, direction) {
        let parseData = JSON.parse(JSON.stringify(this.fileData));

        let keyValue = (a) => {
        return a[fieldname];
        };

        let isReverse = direction === 'asc' ? 1: -1;

        parseData.sort((x, y) => {
        x = keyValue(x) ? keyValue(x) : '';
        y = keyValue(y) ? keyValue(y) : '';

        x = typeof x === 'string' ? x.toLowerCase() : x;
        y = typeof y === 'string' ? y.toLowerCase() : y;

        return isReverse * ((x > y) - (y > x));
        });

        this.fileData = parseData;
    }

    refreshList(){
        this.getContentDocumentIds(this.recordId);
    }
}