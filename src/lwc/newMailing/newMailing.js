/**
 * Created by npo in scope of US1910.
 */

import { LightningElement, wire, api, track } from 'lwc';
import { CurrentPageReference, NavigationMixin } from 'lightning/navigation';
import emailTemplateDELabel from '@salesforce/label/c.EmailTemplateDE';
import emailTemplateENLabel from '@salesforce/label/c.EmailTemplateEN';
import saveLabel from '@salesforce/label/c.NewMailingSave';
import newMailingChangesSavedLabel from '@salesforce/label/c.NewMailingChangesSaved';
import newMailingChangesNotSavedLabel from '@salesforce/label/c.NewMailingChangesNotSaved';
import generalLabelSuccess from '@salesforce/label/c.GeneralLabelSuccess';
import generalLabelError from '@salesforce/label/c.GeneralLabelError';
import generalLabelSend from '@salesforce/label/c.GeneralLabelSend';
import task from '@salesforce/label/c.MailingTask';
import newMailingOpportunityLabel from '@salesforce/label/c.NewMailingOpportunity';
import toInvestorsOverviewLabel from '@salesforce/label/c.ToInvestorsOverviewLabel';
import toInvestorSelectionLabel from '@salesforce/label/c.ToInvestorSelection';
import campaignIsActiveLabel from '@salesforce/label/c.CampaignIsActive';
import copyLabel from '@salesforce/label/c.Copy';
import newMailingSelectedTemplateNotValidLabel from '@salesforce/label/c.NewMailingSelectedTemplateNotValid';
import reportLabel from '@salesforce/label/c.Report';
import reportNotFoundLabel from '@salesforce/label/c.ReportNotFound';

import { ShowToastEvent } from 'lightning/platformShowToastEvent';

import selectEmailTemplates from '@salesforce/apex/EmailTemplateHelper.selectEmailTemplates';
import loadCampaignById from '@salesforce/apex/NewMailingController.loadCampaignById';
import loadReportId from '@salesforce/apex/NewMailingController.loadReportId';
import saveMailingTemplate from '@salesforce/apex/NewMailingController.saveMailingTemplate';
import startEmailsSendingProcess from '@salesforce/apex/NewMailingController.startEmailsSendingProcess';

import { getRecord } from 'lightning/uiRecordApi';

import Id from '@salesforce/user/Id';
import OPPORTUNITY_NAME_FIELD from '@salesforce/schema/Opportunity.Name';
import OPPORTUNITY_OWNER_ID_FIELD from '@salesforce/schema/Opportunity.OwnerId';

import lookupSearch from '@salesforce/apex/LookupController.searchWithReferenceObjectTypes';
import { reduceErrors } from 'c/lwcUtil';

export default class NewMailing extends NavigationMixin(LightningElement)  {

	templateField = { isMultiEntry: false};

    label = {
        emailTemplateDELabel,
        emailTemplateENLabel,
        saveLabel,
        newMailingChangesSavedLabel,
		newMailingChangesNotSavedLabel,
		generalLabelSuccess,
		generalLabelError,
		generalLabelSend,
		task,
		newMailingOpportunityLabel,
		toInvestorSelectionLabel,
		toInvestorsOverviewLabel,
		campaignIsActiveLabel,
		copyLabel,
		newMailingSelectedTemplateNotValidLabel,
		reportLabel,
		reportNotFoundLabel
	}

	@track
	isLoading = false;

    @api
    investorsIds;

    @track
    deTemplates;
    @track
    enTemplates;

	@track
	selectedDeTemplateId;
	@track
	selectedDeTemplateIdIsValid = true;

	@track
	selectedEnTemplateId;
	@track
	selectedEnTemplateIdIsValid = true;

    @wire(CurrentPageReference)
	currentPageReference;

	@api
	get isStoreButtonDisabled()
	{
	    return !this.selectedDeTemplateIdIsValid || !this.selectedEnTemplateIdIsValid || !this.correspondenceName || !this.sender || !this.isCampaignActive;
 	}
	//criteria
	@api
	correspondenceName;
	@api
	bcc;
	@api
	investorStatus;
	@api
	sender;

	@track
	investorsList;
	@track
	opportunityId;
	@track
	campaignId;
	@track
	copyCampaignId;
	@track
	campaignInfo;

	@track
	dataLoaded = false;

	@track
	hasErrorDetails = false;

	@track
	errorDetails;
	@track
	disableFormEditing = false;

	@track
	showModalCreateTask;// Defines if modal window to create a task is shown (true) or (not).

	@api
	get isCampaignActive()
	{
	    return this.campaignInfo !== undefined ? this.campaignInfo.campaignIsActive : true;
	}

	@api
	get isCampaignNotSaved()
	{
		return this.campaignId === undefined;
	}

    connectedCallback()
    {
        this.investorsIds = this.currentPageReference.state.c__investors;
        this.opportunityId = this.currentPageReference.state.c__opportunityId;
        this.campaignId = this.currentPageReference.state.c__campaignId;
        this.copyCampaignId = this.currentPageReference.state.c__copyCampaignId;
        let self = this;
		this.init();
		window.addEventListener("beforeunload", this.beforeUnloadHandler);

	}
    disconnectedCallback() {
        window.removeEventListener("beforeunload", this.beforeUnloadHandler);
    }

	@wire(getRecord, {
		recordId: '$opportunityId',
		fields: [OPPORTUNITY_NAME_FIELD, OPPORTUNITY_OWNER_ID_FIELD]
	})
	opportunity;

	reportId;

	@wire(loadReportId, {})
	async loadReportId(result) {
		if (result.data) {
			this.reportId = result.data;
		}
	}


    // opportunity Name for Header
    get opportunityName() {
        let returnValue;
        if (this.opportunity && this.opportunity.data) {
            returnValue = this.opportunity.data.fields.Name.value;
        } else {
            returnValue = null;
        }
        return returnValue;
	}
	
	get isSendButtonDisabled()
	{
		let hasCampaign = this.campaignId ? false : true;
		let hasCampaignMember = true;
		if (this.investorsList)
		{
			this.investorsList.forEach(investor => {
				if (investor.investorContactsList)
				{
					investor.investorContactsList.forEach(investorContact=> {
						if (investor.investorContactsList.campaignMemberId)
						{
							hasCampaignMember = false;
						}
					});
				}
			});
		}

		if (this.campaignInfo && !this.campaignInfo.campaignIsActive)
		{
			hasCampaign = true;
		}
		
		return (hasCampaign && hasCampaignMember) || this.isStoreButtonDisabled;
	}

    init()
    {
		selectEmailTemplates()
			.then(result => {
				let allTemplates = result;
				self.deTemplates = [];
				self.enTemplates = [];
				allTemplates.forEach(function(template){
					if (template.Name.endsWith('_DE'))
					{
						self.deTemplates.push({label: template.Name, value: template.Id});
					}
					else if (template.Name.endsWith('_EN'))
					{
						self.enTemplates.push({label: template.Name, value: template.Id});
					}
				});
			})
			.catch(error => {
				console.log('...error: ' + error);
			});
        this.dataLoaded = !this.campaignId;
        let self = this;
        let isCopyProcess = this.copyCampaignId !== undefined;
        let campaignIdToLoad = isCopyProcess ? this.copyCampaignId : this.campaignId
        if (campaignIdToLoad)
        {
            this.isLoading = true;
            loadCampaignById({campaignId: campaignIdToLoad})
				.then(resultJson => {
					let result = this.response = JSON.parse(resultJson);
					self.correspondenceName = isCopyProcess ? result.mailInfo.correspondenceName + ' ' + self.label.copyLabel : result.mailInfo.correspondenceName;
					self.bcc = result.mailInfo.bcc;
					self.sender = result.mailInfo.sender;
					self.investorStatus = result.mailInfo.investorStatus;
					self.selectedDeTemplateId = result.mailInfo.selectedDeTemplateId;
					self.selectedEnTemplateId = result.mailInfo.selectedEnTemplateId;
					self.investorsList = [];
					result.investorsList.forEach(function(record){
					    let copyRecord = Object.assign({}, record);
						if (isCopyProcess)
						{
							copyRecord.investorsId = undefined;
							copyRecord.linkToInvestor = undefined;
							copyRecord.mailingConfigId = undefined;
							copyRecord.investorContactsList.forEach(function(copyContactRecord){
								copyContactRecord.campaignMemberRec = undefined;
								copyContactRecord.mailingStatus = '';
								copyContactRecord.mailingStatusLabel = '';
								copyContactRecord.campaignMemberId = undefined;
								copyContactRecord.linkToCampaignMember = undefined;
       						});
						}
					    self.investorsList.push(copyRecord);
    				 });
					self.opportunityId = result.mailInfo.opportunityId;
					self.campaignInfo = result.campaignInfo;
					self.disableFormEditing = !result.campaignInfo.campaignIsActive;

					self.dataLoaded = true;
					if (isCopyProcess)
					{
						self.campaignInfo.campaignId = undefined;
						self.campaignInfo.campaignRec = undefined;
						self.campaignInfo.campaignIsActive = true;
						self.disableFormEditing = false;
					}
					self.isLoading = false;

				})
				.catch(error => {
					console.log('...error: ' + JSON.stringify(error));
					self.isLoading = false;
				});
        }
		if (this.investorsIds && this.investorsList === undefined)
		{
		    this.investorsList = [];
		}
    }

    handleEmailTemplateDEChange(event)
    {
        this.selectedDeTemplateId = event.detail.value;
    }

	handleEmailTemplateENChange(event)
	{
		this.selectedEnTemplateId = event.detail.value;
	}

	handleStore()
	{
		this.store(undefined);
	}

	store(navigateToAfterStore)
	{
		let mailingConfiguration = {};
		mailingConfiguration.opportunityId = this.opportunityId;
		mailingConfiguration.campaignInfo = {
			campaignId: this.campaignId
		}
		mailingConfiguration.investorsList = this.investorsList;
		mailingConfiguration.mailInfo = {
			correspondenceName: this.correspondenceName,
			investorStatus: this.investorStatus,
			sender: this.sender,
			bcc: this.bcc,
			selectedDeTemplateId: this.selectedDeTemplateId,
			selectedEnTemplateId: this.selectedEnTemplateId,
			opportunityId: this.opportunityId
		};

		let self = this;

		console.log('...sending:' + JSON.stringify(mailingConfiguration));
		saveMailingTemplate({jsonString: JSON.stringify(mailingConfiguration)})
			.then(resultJson => {
					let result = JSON.parse(resultJson);
					console.log('...save:' + resultJson);
					if(result.isSuccess) {
						this.dispatchToast(this.label.generalLabelSuccess, self.label.newMailingChangesSavedLabel, 'success');
						this.hasErrorDetails = false;
						this.campaignId = result.campaignInfo.campaignId;
						this.investorsList = result.investorsList;
						this.campaignInfo = result.campaignInfo;
						this.disableFormEditing = result.campaignInfo !== undefined ?
											!result.campaignInfo.campaignIsActive : false;
						if (navigateToAfterStore !== undefined)
						{
							this.navigateTo(navigateToAfterStore);
						}
						else
						{
						    let url = window.location.href;
						    console.log('...url: ' + url);
                            if (url.indexOf('?') > -1 && !url.includes('c__campaignId')){
                               console.log('...url change start  ' + url);
                               url += '&c__campaignId=' + this.campaignId;
                               window.location.href = url;
                               console.log('...url change: ' + url);
                            }
						}
					}
					else {
						if (result.hasErrorDetails)
						{
							this.hasErrorDetails = true;
							this.errorDetails = result.additionalErrors;
						}
						else
						{
							this.dispatchToast(this.label.generalLabelError, result.message, 'error');
							this.hasErrorDetails = false;
						}
					}
			})
			.catch(error => {
				console.log('...error: ' + JSON.stringify(error));
			});
 	}

	handleCriteriaChange(event)
	{
		let criteriaData = event.detail;
		this.correspondenceName = event.detail.correspondenceName;
		this.investorStatus = event.detail.investorStatus;
		this.sender = event.detail.sender;
		this.bcc = event.detail.bcc;
	}

	dispatchToast(title, message, variant) {
		this.dispatchEvent(new ShowToastEvent({title: title,message: message,variant: variant}));
	}

	handleInvestorsListUpdate(event)
	{
	    this.investorsList = event.detail.investorsList;
	}

	handleSend(navigateToAfterStore)
	{
		const child = this.template.querySelector('c-new-mailing-send-modal');

		let mailingConfiguration = {};
		mailingConfiguration.opportunityId = this.opportunityId;
		mailingConfiguration.campaignInfo = {
			campaignId: this.campaignId
		}
		mailingConfiguration.investorsList = this.investorsList;
		mailingConfiguration.mailInfo = {
			correspondenceName: this.correspondenceName,
			investorStatus: this.investorStatus,
			sender: this.sender,
			bcc: this.bcc,
			selectedDeTemplateId: this.selectedDeTemplateId,
			selectedEnTemplateId: this.selectedEnTemplateId,
			opportunityId: this.opportunityId
  		};

//  		console.log('...sending: ' + JSON.stringify(mailingConfiguration));

		child.openModal(JSON.stringify(mailingConfiguration));
	}

	handleSendResponse(event)
	{
		let self = this;

		let result = JSON.parse(event.detail);
		if (result.isSuccess)
		{
			this.dispatchToast(this.label.generalLabelSuccess, self.label.newMailingChangesSavedLabel, 'success');
			this.hasErrorDetails = false;
			this.campaignId = result.campaignInfo.campaignId;
			this.investorsList = result.investorsList;
			this.campaignInfo = result.campaignInfo;
			this.disableFormEditing = result.campaignInfo !== undefined ?
								!result.campaignInfo.campaignIsActive : false;
			const child = this.template.querySelector('c-new-mailing-correspondence-mask');
			child.isFormEditingDisabled = this.disableFormEditing;
			child.handleFormDeactivation();
		}
		else
		{
			if (result.hasErrorDetails)
			{
				this.hasErrorDetails = true;
				this.errorDetails = result.additionalErrors;
			}
			else
			{
				this.hasErrorDetails = false;
			}
		}
	}

    handleOpenModalCreateTask(event)
    {
        this.showModalCreateTask = true;
    }

    handleCloseModalCreateTask(event)
    {
        this.showModalCreateTask = false;
    }

    handleToInvestorsOverviewClick()
    {
		this[NavigationMixin.Navigate]({
			type: 'standard__navItemPage',
			attributes: {
				apiName: 'NewMailing'
			},
			state: {

			}
		});
    }

    handleToInvestorsSelectionClick() {}

    handleNavigationButtonClick(event)
    {
        let navigateTo = event.target.dataset.targetNavigation;
        this.openUnsavedChangesDialog(navigateTo);
    }

    openUnsavedChangesDialog(navigateTo)
    {
        if (!this.disableFormEditing)
        {
			const child = this.template.querySelector('c-new-mailing-unsaved-changes-modal');
			child.navigateTo = navigateTo;
			child.show();
		}
		else
		{
		    this.navigateTo(navigateTo);
  		}
    }

    handleSaveChanges(event)
    {
        let target = event.detail.navigateTo;
        this.store(target);
    }

	handleDiscardChanges(event)
	{
		let target = event.detail.navigateTo;
		if (target !== undefined)
		{
			this.navigateTo(target);
		}
	}

    navigateTo(navigateTo)
    {
        if (navigateTo.toString() === 'opportunity')
        {
			this[NavigationMixin.Navigate]({
				type: 'standard__recordPage',
				attributes: {
					"recordId": this.opportunityId,
					"objectApiName": "Opportunity",
					"actionName": "view"
				},
			});
        }
        else if(navigateTo.toString() === 'investorsOverview')
        {
			this[NavigationMixin.Navigate]({
				type: 'standard__navItemPage',
				attributes: {
					apiName: 'InvestorsList'
				},
				state : {
				    c__recordId: this.opportunityId
    			}
			});
        }
        else if(navigateTo.toString() === 'investorSelection') {
			this[NavigationMixin.Navigate]({
				type: 'standard__navItemPage',
				attributes: {
					apiName: 'InvestorSelection'
				},
				state : {
					c__recordId: this.opportunityId
				}
			});
        }
		else if(navigateTo.toString() === 'report' ) {
		    if (this.reportId != null)
		    {
				this[NavigationMixin.Navigate]({
					type: 'standard__recordPage',
					attributes: {
						recordId: this.reportId,
						objectApiName: 'Report',
						actionName: 'view'
					},
					state : {
						fv0: this.campaignId
					}
				});
			}
			else
			{
				this.notifyUser('', this.label.reportNotFoundLabel, 'error');
   			}
		}

    }

	handleTemplateSearch(event) {
	    let targetId = event.target.dataset.id;
		var searchParam = {
			referenceObjectTypes: ['EmailTemplate'],
			searchTerm: event.detail.searchTerm,

		};

		lookupSearch(searchParam)
			.then(results => {
				this.template
					.querySelector('[data-id="' + targetId + '"]')
					.setSearchResults(results);
			})
			.catch(error => {
				this.notifyUser(
					'Lookup Error',
					'An error occurred while searching with the lookup field.\n' +
						reduceErrors(error).join(', '),
					'error'
				);
			});
	}

	/**
	 * notify user
	 * @param {string} title
	 * @param {string} message
	 * @param {string} variant
	 */
	notifyUser(title, message, variant) {
		const toastEvent = new ShowToastEvent({
			title,
			message,
			variant
		});
		this.dispatchEvent(toastEvent);
	}

	handleTemplateLookupChange(event) {
		let selection = event.target.selection;
		let templateField = event.target.dataset.id;
		let validityField = templateField + 'IsValid';
		let chosenTemplate = selection.length > 0 ? selection[0] : undefined;

		let newValueIsValid = chosenTemplate === undefined;
		if (chosenTemplate !== undefined)
		{
		    let templateAvailableOptions = templateField.toString() === 'selectedEnTemplateId' ? this.enTemplates : this.deTemplates;
			templateAvailableOptions.forEach(function(option) {
				if (option.value.toString() === chosenTemplate.id.toString())
				{
				    newValueIsValid = true;
    			}
			});
		}
		this[validityField] = newValueIsValid;
		this[templateField] = chosenTemplate !== undefined ? chosenTemplate.id : undefined;
	}

	// following code is a hack to make combobox overlay other elements
	_isInitialized = false;

	renderedCallback() {
	   if (!this._isInitialized) {
		   const style = document.createElement('style');
		   style.innerText = '.slds-table_header-fixed_container, .slds-table--header-fixed_container, .slds-scrollable_x, .slds-scrollable--x, .slds-scrollable_y, .slds-scrollable--y {overflow:visible !important; overflow-y: visible !important; overflow-x: visible !important;}';
		   let input = this.template.querySelector('.correspondence-mask');
		   console.log('...correspondence-mask: ' + input);
		   if (input) {
		       this._isInitialized = true;
			   input.appendChild(style);
		   }
	   }
   }

}