/**
 * Service component with utility functions
 */

const SORTING_DIRECTION = { ASC: 'ASC', DESC: 'DESC' };

/**
 * Sorts a given 'data' array of objects by 'sortByField' field and 'direction' direction
 */
const sort = (data, sortByField, direction) => {
    // serialize the data before calling sort function
    let dataCopy = JSON.parse(JSON.stringify(data));

    // Return the value stored in the field
    let keyValue = (a) => {
        return a[sortByField];
    };

    // checks whether the direction is reverse
    let isReverse = direction.toUpperCase() === SORTING_DIRECTION.ASC ? 1 : -1;

    // sorting data
    dataCopy.sort((x, y) => {
        x = keyValue(x) ? keyValue(x) : ''; // handling null values
        y = keyValue(y) ? keyValue(y) : '';

        x = typeof x === 'string' ? x.toLowerCase() : x;
        y = typeof y === 'string' ? y.toLowerCase() : y;

        // sorting values based on direction
        return isReverse * ((x > y) - (y > x));
    });

    return dataCopy;
};

export {
    SORTING_DIRECTION,
    sort
};