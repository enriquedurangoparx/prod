import { LightningElement, api, track, wire  } from 'lwc';
import saveInvestors from '@salesforce/apex/InvestorSelectionHelper.saveInvestors';
import { CurrentPageReference } from 'lightning/navigation';
import { fireEvent } from 'c/pubsub';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';

// import custom labels
import close from '@salesforce/label/c.InvestorsListCloseLabel';
import cancel from '@salesforce/label/c.investorsListCancelLabel';
import createInvestors from '@salesforce/label/c.InvestorsSelectionCreateInvestors';
import createInvestorToContacts from '@salesforce/label/c.InvestorsSelectionCreateInvestorToContacts';
import addSelection from '@salesforce/label/c.AddSelectionLabel';
import existingInvestorContact from '@salesforce/label/c.InvestorsSelectionExistingInvestorContact';
import newInvestorContact from '@salesforce/label/c.InvestorsSelectionNewInvestorContact';
import newInvestor from '@salesforce/label/c.InvestorsSelectionNewInvestor';
import existingInvestor from '@salesforce/label/c.InvestorsSelectionExistingInvestor';
import investorContactDescription from '@salesforce/label/c.InvestorsSelectionInvestorContactDescription';
import investorDescription from '@salesforce/label/c.InvestorsSelecetionInvestorDescription';


export default class InvestorSelectionAddRecordsModal extends LightningElement {
    @wire(CurrentPageReference) pageRef;

    @track showModal = false;
    @track investorRecordsWrap;
    @track objectApiName;
    @track isContact;
    @track opportunityId;
    @track selectedRecords;
    @track selectedTab;

    label = {
        close,
        createInvestors,
        createInvestorToContacts,
        addSelection,
        cancel,
        existingInvestorContact,
        newInvestorContact,
        newInvestor,
        existingInvestor,
        investorContactDescription,
        investorDescription
    }

    @api
    openModal(selectedRecords, objectApiName, opportunityId, selectedTab) {
        this.objectApiName = objectApiName;
        this.isContact = objectApiName === 'Contact' ? true : false;
        this.opportunityId = opportunityId;
        this.showModal = true;
        this.selectedRecords = selectedRecords;
        this.selectedTab = selectedTab;
    }

    /**
     * close objective selection modal
     */
    closeModal() {
        this.showModal = false;
    }

    addSelection(){
        this.closeModal();
        fireEvent(this.pageRef, 'InvestorSelectionDatatable__isLoading', {isLoading : true});
        saveInvestors({objectApiName: this.objectApiName, jsonRecords: JSON.stringify(this.selectedRecords), opportunityId : this.opportunityId, source : this.selectedTab})
        .then(result => {
            this.handleAddSelection();
        })
        .catch(error => {
            console.error('InvestorSelectionAddRecordsModal:error', JSON.parse(JSON.stringify(error)));
            dispatchEvent(
                new ShowToastEvent({
                    title: error['status'] ? error['status'] : error['body']['pageErrors'][0]['statusCode'],
                    message: error['body']['message'] ? error['body']['message'] : error['body']['pageErrors'][0]['message'],
                    variant: 'error',
                }),
            );
            fireEvent(this.pageRef, 'InvestorSelectionDatatable__isLoading', {isLoading : false});
        });
    }

    handleAddSelection()
    {
        fireEvent(this.pageRef, 'investorSelectionInvestorList-getInvestors', {});
        fireEvent(this.pageRef, 'InvestorSelectionTabBidder__bidderTabInitData', {});
        this.dispatchEvent(new Event('recordadded'));
    }
}