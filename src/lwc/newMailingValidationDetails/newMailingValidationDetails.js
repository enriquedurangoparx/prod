import { LightningElement, api, track } from 'lwc';

import generalLabelErrorsHaveOccured from '@salesforce/label/c.GeneralLabelErrorsHaveOccured';
import investorsListTableRowContact from '@salesforce/label/c.InvestorsListTableRowContact';
import investorsListTableRowAccount from '@salesforce/label/c.InvestorsListTableRowAccount';

export default class NewMailingValidationDetails extends LightningElement
{
    @api 
    hasErrorDetails = false;

    @api
    errorDetails;

    label = {
        generalLabelErrorsHaveOccured,
        investorsListTableRowContact,
        investorsListTableRowAccount
    }

    get showComponent()
    {
        return this.hasErrorDetails && this.errorDetails;
    }

	get basicError()
	{
	    return this.hasErrorDetails && this.errorDetails.basicError;
	}


    get showInactiveRecordsBlock()
    {
        return this.errorDetails.inactiveInvestorsList || this.errorDetails.inactiveInvestorContactsList ? true : false;
    }

    get inactiveInvestorsMessage()
    {
        return this.errorDetails.inactiveInvestorsMessage ? this.errorDetails.inactiveInvestorsMessage : '';
    }

    get inactiveInvestorsList()
    {
        return this.errorDetails.inactiveInvestorsList ? this.errorDetails.inactiveInvestorsList : [];
    }

    get inactiveInvestorContactsList()
    {
        return this.errorDetails.inactiveInvestorContactsList ? this.errorDetails.inactiveInvestorContactsList : [];
    }



    get showContactLabel()
    {
        return this.errorDetails.inactiveInvestorContactsList && this.errorDetails.inactiveInvestorContactsList.length > 0 ? true : false;
    }

    get contactLabel()
    {
        return this.errorDetails.inactiveInvestorContactsList ? this.label.investorsListTableRowContact : '';
    }

    get showAccountLabel()
    {
        return this.errorDetails.inactiveInvestorsList && this.errorDetails.inactiveInvestorsList > 0 ? true : false;
    }

    get accountLabel()
    {
        return this.errorDetails.inactiveInvestorsList ? this.label.investorsListTableRowAccount : '';
    }

   



    get showInvalidContactsEmailBlock()
    {
        return this.errorDetails.investorContactsWithInvalidEmail && this.errorDetails.investorContactsWithInvalidEmail.length > 0 ? true : false;
    }

    get investorContactsInvalidEmailMessage()
    {
        return this.errorDetails.investorContactsInvalidEmailMessage ? this.errorDetails.investorContactsInvalidEmailMessage : '';
    }

    get investorContactsWithInvalidEmail()
    {
        return this.errorDetails.investorContactsWithInvalidEmail ? this.errorDetails.investorContactsWithInvalidEmail : [];
    }



    get showInvalidBccEmailsBlock()
    {
        return this.errorDetails.bccInvalidEmails && this.errorDetails.bccInvalidEmails.length > 0 ? true : false;
    }

    get bccInvalidEmailMessage()
    {
        return this.errorDetails.bccInvalidEmailMessage ? this.errorDetails.bccInvalidEmailMessage : '';
    }

    get bccInvalidEmailsList()
    {
        return this.errorDetails.bccInvalidEmails ? this.errorDetails.bccInvalidEmails : [];
    }



    get showMissingLanguageWithSendingTypeBlock()
    {
        return this.errorDetails.contactsHasTypeButNoLanguageList && this.errorDetails.contactsHasTypeButNoLanguageList.length > 0 ? true : false;
    }

    get missingLanguageWithSendingTypeMessage()
    {
        return this.errorDetails.contactHasTypeButNoLanguageMessage ? this.errorDetails.contactHasTypeButNoLanguageMessage : '';
    }

    get bmissingLanguageWithSendingTypeList()
    {
        return this.errorDetails.contactsHasTypeButNoLanguageList ? this.errorDetails.contactsHasTypeButNoLanguageList : [];
    }

    

    get showContactHasCcWithoutToBlock()
    {
        return this.errorDetails.contactHasCcWithoutToList && this.errorDetails.contactHasCcWithoutToList.length > 0 ? true : false;
    }

    get contactHasCcWithoutToMessage()
    {
        return this.errorDetails.contactHasCcWithoutToMessage ? this.errorDetails.contactHasCcWithoutToMessage : '';
    }

    get contactHasCcWithoutToList()
    {
        return this.errorDetails.contactHasCcWithoutToList ? this.errorDetails.contactHasCcWithoutToList : [];
    }
}