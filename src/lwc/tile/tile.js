/**
 * Represents a tile.
 * @class
 * @param {object} tile
 */
import { LightningElement, api/*, wire*/ } from 'lwc';
//import { CurrentPageReference } from 'lightning/navigation';
//import { fireEvent } from 'c/pubsub';

export default class Tile extends LightningElement {
    @api tile;

    //@wire(CurrentPageReference) pageRef;

    handleTileSelected() {
        console.log('>>>Tile -- handleTileSelected');
        console.log('>>>this.tile: ' + JSON.stringify(this.tile, null, '\t'));

        const selectedEvent = new CustomEvent('selected', {
            detail: this.tile.id
        });

        /*fireEvent(this.pageRef, 'Tile__tileSelected', {
            detail: this.tile.id
        });*/
        this.dispatchEvent(selectedEvent);
        //fireEvent(this.pageRef, 'TileList__tileSelected', this.tile.id);
    }

    get backgroundImageStyle() {
        return `background-image:url(${this.tile.backgroundImageUrl})`;
    }

}