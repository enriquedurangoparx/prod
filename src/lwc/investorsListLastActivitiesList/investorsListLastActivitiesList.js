import { LightningElement, api } from 'lwc';

export default class InvestorsListLastActivitiesList extends LightningElement
{
    @api rowId;
    _listOfActivities;

    set listOfActivities(value)
    {
        let tempList = value ? value.split(";") : [];
        let templistOfActivities = [];
        if (tempList.length > 0)
        {
            tempList.forEach(contactRow => {
                templistOfActivities.push(contactRow);
            });
        }
        this._listOfActivities = templistOfActivities;
    }

    @api
    get listOfActivities()
    {
        return this._listOfActivities;
    }
}