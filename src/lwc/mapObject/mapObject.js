export function createMarker(id, Latitude, Longitude, title, description, Street, PostalCode, City, Country, icon='utility:salesforce1') {
    if(Latitude && Longitude){
        return {id, location:{Latitude, Longitude}, title, description, icon};
    }
    
    return {id, location:{City, Country, PostalCode, Street, Latitude, Longitude}, title, description, icon};
}