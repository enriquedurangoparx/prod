import { LightningElement, api, track } from 'lwc';
import { refreshApex } from '@salesforce/apex';

import getRequestById from '@salesforce/apex/InvestorsListController.getBidsByRecordId';

export default class InvestorsListPopover extends LightningElement 
{
    @api recordId;
    @api top;
    @api left;

    @track request;
    @track isLoading = true;
    connectedCallback()
    {
        getRequestById({recordId: this.recordId})
        .then(result => {
            this.request = result;
            this.isLoading = false;
        });
    }

    handleRefreshBidActivity()
    {
        return refreshApex(this.wiredResult);
    }

    get positionStyle() {
        return `top: ${this.top}px; left:${this.left}px`;
    }
}