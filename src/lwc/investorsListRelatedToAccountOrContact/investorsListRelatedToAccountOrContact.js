import { LightningElement, api, wire, track } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import { NavigationMixin } from 'lightning/navigation';

import getRelatedInvestorsList from '@salesforce/apex/MyListController.getRelatedInvestorsList';

import errorLabel from '@salesforce/label/c.GeneralLabelError'; 
import viewAllLabel from '@salesforce/label/c.GeneralLabelViewAll';
import collapseLabel from '@salesforce/label/c.GeneralLabelCollapse';
import noRecordsFoundLabel from '@salesforce/label/c.GeneralLabelNoRecordsFound';
import listNameLabel from '@salesforce/label/c.MyListOverviewListName'; 
import listNumberLabel from '@salesforce/label/c.InvestorsListNumber';
import listOwnerLabel from '@salesforce/label/c.MyListOverviewOwner'; 
import lastModifiedByLabel from '@salesforce/label/c.GeneralLabelLastModifiedBy';
import lastModifiedDateLabel from '@salesforce/label/c.GeneralLabelLastModifiedDate'; 
import createdDateLabel from '@salesforce/label/c.GeneralLabelCreatedDate';

export default class InvestorsListRelatedToAccountOrContact extends NavigationMixin(LightningElement)
{
    @api objectApiName;
    @api recordId;

    @track isLoading = true;
    @track isViewAll = false;
    @track hasError = false;
    @track errorMessage;
    
    @track data = []; // keeps data fore the table

    relatedListMaxSize = 5;
    _dataTruncated = []; // keeps truncated data with size of relatedListMaxSize
    _data = []; // keeps the whole data from response

    label = {
        errorLabel,
        viewAllLabel,
        collapseLabel,
        noRecordsFoundLabel
    }

    columns = [
        { label: listNameLabel, fieldName: 'listNameLink',  type: 'url',  typeAttributes: { label: {fieldName: 'name'}, target:'_blank'}},
        { label: listNumberLabel, fieldName: 'numberOfContacts', type: 'number', cellAttributes: { alignment: 'left' } },
        { label: listOwnerLabel, fieldName: 'listOwnerLink',  type: 'url',  typeAttributes: { label: {fieldName: 'ownerName'}, target:'_blank'}},
        { label: lastModifiedByLabel, fieldName: 'listModifiedLink',  type: 'url',  typeAttributes: { label: {fieldName: 'lastModifiedByName'}, target:'_blank'}},
        { label: lastModifiedDateLabel, fieldName: 'lastModifiedDate', type: 'date', },
        { label: createdDateLabel, fieldName: 'createdDate', type: 'date', },
    ];

    wiredRecords
    @wire(getRelatedInvestorsList, { obj: '$objectApiName', recordId: '$recordId' })
    wiredRelatedInvestorsList(response) {
        // Hold on to the provisioned response so we can refresh it later.
        this.wiredRecords = response; // track the provisioned response
        const { data, error } = response; // destructure the provisioned response
        if (data) 
        { 
            let resp = JSON.parse(response.data);

            if (resp.isSuccess)
            {
               
                this._data = this.prepareData(resp.relatedInvestorsList);
                let truncatedData = [];

                for (let i = 0; i < resp.relatedInvestorsList.length; i++)
                {
                    if (i < this.relatedListMaxSize)
                    {
                        truncatedData.push(this._data[i]);
                    }
                    else
                    {
                        break;
                    }
                }
                this._dataTruncated = truncatedData;
                this.setTableData();
                
                this.setComponentView(false);
            }
        }
        else if (error) 
        { 
            this.setComponentView(true, error, error);
            this.isLoading = false;
        }
    }

    prepareData(result){
        let rec = [];

        result.forEach(element => {
            element.listNameLink = '/' + element.id;
            element.listOwnerLink = '/' + element.ownerId;
            element.listModifiedLink = '/' + element.lastModifiedById;

            rec.push(element);
        });

       return rec;
    }

    get showViewLabel()
    {
        return this.isViewAll ? this.label.collapseLabel : this.label.viewAllLabel;
    }

    handleViewAllClick(event){
        this.isViewAll = !this.isViewAll;
        this.setTableData();
    }

    setTableData()
    {
        if (this.isViewAll)
        {
            this.data = this._data;
        }
        else 
        {
            this.data = this._dataTruncated;
        }
    }

    setComponentView(isError, message, error)
    {
        this.isLoading = false;

        if (isError)
        {
            this.hasError = isError;
            this.errorMessage = message;
            this.ShowToastEvent(this.label.errorLabel, error, 'error');
        }
        else
        {
            if (this.data && this.data.length > 0)
            {
                this.hasError = isError;
            }
            else
            {
                this.hasError = true;
                this.errorMessage = this.label.noRecordsFoundLabel;
            }
        }
    }

    /**
     * notify user
     * @param {string} title
     * @param {string} message
     * @param {string} variant
     */
    notifyUser(title, message, variant) {
        const toastEvent = new ShowToastEvent({
            title,
            message,
            variant
        });
        this.dispatchEvent(toastEvent);
    }
}