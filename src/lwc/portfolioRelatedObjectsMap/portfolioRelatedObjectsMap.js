import { LightningElement, api, track, wire } from 'lwc';
import { createMarker } from 'c/mapObject';

import { getObjectInfo } from 'lightning/uiObjectInfoApi';
import { CurrentPageReference, NavigationMixin } from 'lightning/navigation';
import { registerListener } from 'c/pubsub';

import OBJECT from '@salesforce/schema/PropertyObject__c';

import NAME_FIELD from '@salesforce/schema/PropertyObject__c.Name';
import MAIN_THUMBNAIL from '@salesforce/schema/PropertyObject__c.MainThumbnail__c';
import SQM_IN_TOTAL_FIELD from '@salesforce/schema/PropertyObject__c.SqmInTotal__c';
import VACANCY_FIELD from '@salesforce/schema/PropertyObject__c.VacancySqm__c';
import PROPERTY_OWNER_FIELD from '@salesforce/schema/PropertyObject__c.PropertyOwner__c';

import searchRelatedObjectsBySoql from '@salesforce/apex/LWCObjectHelper.searchRelatedObjectsBySoql';

// import custom labels:
import noRecordsFoundLabel from '@salesforce/label/c.GeneralLabelNoRecordsFound';


export default class PortfolioRelatedObjectsMap extends NavigationMixin(LightningElement) {
    @wire(CurrentPageReference) pageRef;
    @api recordId;
    @api objectApiName;

    @track objectObjectInfo;
    @track error;   //this holds errors
    @track center;
    @track mapMarkers;
    @track zoomLevel = 7;
    @track isLoading = true;
    
    label = {
        noRecordsFoundLabel
    }

    @wire(getObjectInfo, { objectApiName: OBJECT })
    wiredObjectInfo({ error, data }) {
        if (data) {
            // eslint-disable-next-line @lwc/lwc/no-async-operation
            setTimeout(() => {
                this.objectObjectInfo = data;
                this.handleLoad();
            }, 1000);
        } else if (error) {
            this.error = JSON.stringify(error);
            this.isLoading = false;
        }
    }

    get isObjectObjectInfo()
    {
        return this.objectObjectInfo && this.objectObjectInfo.fields ? true : false;
    }

    connectedCallback(){
        registerListener('mapcomponent_refreshInitialData', this.refreshData, this);
    }

    handleLoad(){
        searchRelatedObjectsBySoql({recordId: this.recordId, objApiName: this.objectApiName})
        .then(result => {
            if (result.length === 0) {
                this.isLoading = false;
                this.mapMarkers = undefined;
                this.error = this.label.noRecordsFoundLabel;
            }
            else if (result.length > 0)
            {
                this.isLoading = false;
                this.error = undefined;
                this.mapMarkers = this.createMarkers(result);
            }       
        })
        .catch(error => {
            this.isLoading = false;
            this.error = JSON.stringify(error);
            this.mapMarkers = undefined;
        });
    }

    createMarkers(records){
        let tempMarkers = [];
        // this.center = this.calculateCenter(records);
        if (this.objectObjectInfo && this.objectObjectInfo.fields)
        {
            records.forEach(record => {
                const latitudeValue = record.Geolocation__Latitude__s ? record.Geolocation__Latitude__s : '';
                const longitudeValue = record.Geolocation__Longitude__s ? record.Geolocation__Longitude__s : '';
                const nameValue = record[NAME_FIELD.fieldApiName];
                const sqmInTotalFieldName = (this.objectObjectInfo.fields[SQM_IN_TOTAL_FIELD.fieldApiName] ? this.objectObjectInfo.fields[SQM_IN_TOTAL_FIELD.fieldApiName].label : '');
                const sqmInTotalValue = (record[SQM_IN_TOTAL_FIELD.fieldApiName] ? this._replacePointWithComma(record[SQM_IN_TOTAL_FIELD.fieldApiName]) + ' m²' : '');
                const vacancyFieldName = (this.objectObjectInfo.fields[VACANCY_FIELD.fieldApiName] ? this.objectObjectInfo.fields[VACANCY_FIELD.fieldApiName].label : '');
                const vacancyValue = (record[VACANCY_FIELD.fieldApiName] ? this._replacePointWithComma(record[VACANCY_FIELD.fieldApiName]) + ' m²' : '');
                const propertyOwnerFieldName = (this.objectObjectInfo.fields[PROPERTY_OWNER_FIELD.fieldApiName] ? this.objectObjectInfo.fields[PROPERTY_OWNER_FIELD.fieldApiName].label : '');
                const propertyOwnerValue = (record.PropertyOwner__r ? record.PropertyOwner__r.Name : '');
                const propertyOwnerId = ((record.PropertyOwner__r) ? record.PropertyOwner__r.Id : '');
                const urlToPropertyOwner = window.location.origin + '/lightning/r/Account/' + propertyOwnerId + '/view';
                const mainThumbnail = (record[MAIN_THUMBNAIL.fieldApiName] ? record[MAIN_THUMBNAIL.fieldApiName] : '');
                const street = record.Street__c ? record.Street__c : ''; 
                const postalCode = record.PostalCode__c ? record.PostalCode__c : ''; 
                const country = record.Country__c ? record.Country__c : '';
    
                let marker = createMarker(
                    '',
                    latitudeValue,
                    longitudeValue,
                    nameValue,
                    `<div style="display: table; width: 100%">
                        <div style="display: table-row-group;">
                            <div style="display: table-row;">
                                ${mainThumbnail ? '<div style="display: table-cell; padding: 3px 10px; vertical-align: top;">'+mainThumbnail+'</div>' : ''}
                                <div style="display: table-cell; padding: 3px 10px; vertical-align: top;">
                                    ${sqmInTotalFieldName}: ${sqmInTotalValue}<br />
                                    ${vacancyFieldName}: ${vacancyValue}<br />
                                    ${propertyOwnerFieldName}: <a target="_blank" href="${urlToPropertyOwner}">${propertyOwnerValue}</a><br />
                                </div>
                            </div>
                        </div>
                    </div>`,
                    street,
                    postalCode, 
                    country, 
                    ''
                );
                tempMarkers.push(marker);
            });
        }

        return tempMarkers;
    }

    /**
     * Calculate for all object the center location for the map.
     * https://stackoverflow.com/questions/6671183/calculate-the-center-point-of-multiple-latitude-longitude-coordinate-pairs
     * @param {Array} records
     */
    calculateCenter(records){
        let sumX = 0;
        let sumY = 0;
        let sumZ = 0;
        const length = records.length;

        records.forEach(record => {
            let lat = this.degr2rad(record.Geolocation__Latitude__s);
            let lng = this.degr2rad(record.Geolocation__Longitude__s);
            // sum of cartesian coordinates
            sumX += Math.cos(lat) * Math.cos(lng);
            sumY += Math.cos(lat) * Math.sin(lng);
            sumZ += Math.sin(lat);
        });
        
        let avgX = sumX / length;
        let avgY = sumY / length;
        let avgZ = sumZ / length;

        // convert average x, y, z coordinate to latitude and longtitude
        let lng = Math.atan2(avgY, avgX);
        let hyp = Math.sqrt(avgX * avgX + avgY * avgY);
        let lat = Math.atan2(avgZ, hyp);

        return {
            location: {
                Latitude: this.rad2degr(lat),
                Longitude: this.rad2degr(lng)
            }
        };
    }

    rad2degr(radiant) { 
        return radiant * 180 / Math.PI;
    }

    degr2rad(degrees) {
        return degrees * Math.PI / 180;
    }

    _replacePointWithComma(number){
        let returnValue = '';

        if(typeof number !== 'undefined'){
            returnValue = number.toString().replace('.', ',');
        }

        return returnValue;
    }

    get showMap(){
        let returnValue = (this.mapMarkers ? true : false);
        
        return returnValue;
    }

    refreshData(event)
    {
        this.handleLoad();
    }
}