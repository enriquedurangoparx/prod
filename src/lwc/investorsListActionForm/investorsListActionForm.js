import { LightningElement, api, track } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import { reduceErrors } from 'c/lwcUtil';
import lookupSearch from '@salesforce/apex/LookupController.search';
import Id from '@salesforce/user/Id';

import createTask from '@salesforce/apex/InvestorsListController.createTask';
import createEvent from '@salesforce/apex/InvestorsListController.createEvent';

export default class InvestorsListActionForm extends LightningElement {

    @api investorId;

    // setter for activityType, sets track variables for managing which fields will be displayed and stored
    _activityType;
    @api set activityType(value) {
        this._activityType = value;
        if(value === "call"){
            this.typeIsCall=true;
            this.typeIsTask = this.typeIsBid = this.typeIsEvent = false;
            this.callSubject = this.label.callSubject;
        } else if(value === "task"){
            this.typeIsTask = true;
            this.typeIsCall = this.typeIsBid = this.typeIsEvent = false;
        } else if(value === "event"){
            this.typeIsEvent = true;
            this.typeIsCall = this.typeIsBid = this.typeIsTask = false;
        } else if(value === "bid"){
            this.typeIsBid = true;
            this.typeIsCall = this.typeIsEvent = this.typeIsTask = false;
        }
    }
    get activityType(){
        return this._activityType;
    }
    // track variable for the users id
    @track userId = Id;
    // value for ActivityDate field, will be pre-populated with the current date
    @track dateValue;
    @track callSubject;

    // values to show/hide fields depending on activityType
    @track typeIsEvent=false;
    @track typeIsTask=false;
    @track typeIsBid=false;
    @track typeIsCall=false;

    // UI Labels
    label = {
        date: "Datum",
        topic: "Thema",
        comment: "Kommentar",
        dueDate: "Fälligkeitsdatum",
        contacts: "Kontakte",
        enquiry: "Ankaufprofil",
        assignedTo: "Zugeordnet zu",
        contactsErrorMessage: "Bitte wählen Sie einen oder mehrere Kontakte aus.",
        toastTitle: "Datensatz erfolgreich erzeugt.",
        toastMessage: "",
        startDate: "Start Datum",
        endDate: "Ende Datum",
        amount: "Betrag",
        activityBidSubjectLabel: "Gebot",
        callSubject: "Anruf"
    }

    // configuration-objects for lookup components
    contactField = {objectApiName: "Task", apiName: "WhoId", isMultiEntry: true};
    userField = {objectApiName: "Task", apiName: "OwnerId", isMultiEntry: false};
    enquiryField = {objectApiName: "Task", apiName: "AcquisitionProfile__c", isMultiEntry: false};

    // fields to hold values from the lookup fields
    selectedContactIds;
    selectedOwnerId;
    acqouisitionProfileId;

    renderedCallback(){
        // pre-populating the date field with the current date
        var date = new Date();
        this.dateValue = date.toISOString().substring(0,10);
    }

	handleContactsLookupSearch(event) {
        var searchParam = {
            parentObjectApiName: this.contactField.objectApiName,
            lookupApiName: this.contactField.apiName,
            searchTerm: event.detail.searchTerm
        };

        lookupSearch(searchParam)
            .then(results => {
                this.template
                    .querySelector('[data-id="nameSearchInput"]')
                    .setSearchResults(results);
            })
            .catch(error => {
                this.notifyUser(
                    'Lookup Error',
                    'An error occured while searching with the lookup field.\n' +
                        reduceErrors(error).join(', '),
                    'error'
                );
            });
    }


    handleContactsChange(event) {
        if(event.target.selection && event.target.selection.length > 0) {
            this.selectedContactId = event.target.selection[0].id;
        } else {
            this.selectedContactId = undefined;
        }
    }

    handleContactSelectionChange(event){
        if(event.target.selection && event.target.selection.length > 0) {
            this.selectedContactIds = [];
            for(let i=0; i<event.target.selection.length; i++) {
                this.selectedContactIds.push(event.target.selection[i].id);
            }
            
        } else {
            this.selectedContactId = undefined;
        }
    }

    handleOwnerLookupSearch(event) {
        var searchParam = {
            parentObjectApiName: this.userField.objectApiName,
            lookupApiName: this.userField.apiName,
            searchTerm: event.detail.searchTerm
        };

        lookupSearch(searchParam)
            .then(results => {
                this.template
                    .querySelector('[data-id="ownerSearchInput"]')
                    .setSearchResults(results);
            })
            .catch(error => {
                this.notifyUser(
                    'Lookup Error',
                    'An error occured while searching with the lookup field.\n' +
                        reduceErrors(error).join(', '),
                    'error'
                );
            });
    }


    handleOwnerChange(event) {
        if(event.target.selection && event.target.selection.length > 0) {
            this.acqouisitionProfileId = event.target.selection[0].id;
        } else {
            this.acqouisitionProfileId = undefined;
        }
    }

    handleOwnerSelectionChange(event){
         
    }

    handleEnquiryLookupSearch(event) {
        var searchParam = {
            parentObjectApiName: this.enquiryField.objectApiName,
            lookupApiName: this.enquiryField.apiName,
            searchTerm: event.detail.searchTerm
        };

        lookupSearch(searchParam)
            .then(results => {
                this.template
                    .querySelector('[data-id="enqirySearchInput"]')
                    .setSearchResults(results);
            })
            .catch(error => {
                this.notifyUser(
                    'Lookup Error',
                    'An error occured while searching with the lookup field.\n' +
                        reduceErrors(error).join(', '),
                    'error'
                );
            });
    }


    handleEnquiryChange(event) {
        if(event.target.selection && event.target.selection.length > 0) {
            this.selectedOwnerId = event.target.selection[0].id;
        } else {
            this.selectedOwnerId = undefined;
        }
    }

    handleEnquirySelectionChange(event){
         
    }

    @api 
    async submit(){
        // validate form
        let allValid = this.validateForm();
        if(!allValid)
            return;

        // create Data Object for storing
        let description = this.template.querySelector('[data-id="commentInput"]').value;
        let activity = {ActivityDate: this.dateValue,
                    WhatId: this.investorId, 
                    TaskWhoIds: this.selectedContactIds, 
                    Description: description,
                    AcquisitionProfile__c: this.acqouisitionProfileId,
                    OwnerId: this.selectedOwnerId};

        switch(this.activityType){
            case "call": 
                activity.Subject = this.template.querySelector('[data-id="topicInput"]').value;
                break;
            case "task": 
                activity.Subject = this.template.querySelector('[data-id="topicInput"]').value;
                activity.ActivityDate = this.template.querySelector('[data-id="dueDateInput"]').value;
                break;
            case "event": 
                activity.Subject = this.template.querySelector('[data-id="topicInput"]').value;
                activity.StartDate = this.template.querySelector('[data-id="startDateInput"]').value;
                activity.EndDate = this.template.querySelector('[data-id="endDateInput"]').value;
                break;
            case "bid": 
                activity.Subject = this.label.activityBidSubjectLabel;
                activity.Amount__c = this.template.querySelector('[data-id="amountInput"]').value;
                break;
            default: 
        }
        
        let createdObject;
        
        if(this.activityType === "event"){
            createdObject = await createTask({
                data: activity  
            });
        }
        else {
            createdObject = await createEvent({
                data: activity  
            });
        }
        
        window.console.log("createdObject:", createdObject);

        const event = CustomEvent('activitycreated', {
            composed: true,
            bubbles: true,
            cancelable: true,
            detail: {
                toastTitle: this.label.toastTitle,
                toastMessage: this.label.toastMessage,
                toastVariant: 'success'
            },
        });
        this.dispatchEvent(event);

    }

    validateForm(){
        const allValidInput = [...this.template.querySelectorAll('lightning-input')]
            .reduce((validSoFar, inputCmp) => {
                        inputCmp.reportValidity();
                        return validSoFar && inputCmp.checkValidity();
            }, true);
            const allValidTextArea = [...this.template.querySelectorAll('lightning-textarea')]
            .reduce((validSoFar, inputCmp) => {
                        inputCmp.reportValidity();
                        return validSoFar && inputCmp.checkValidity();
            }, true);
            const allValidLookup = [...this.template.querySelectorAll('c-lookup')]
            .reduce((validSoFar, inputCmp) => {
                        inputCmp.reportValidity();
                        return validSoFar && inputCmp.checkValidity();
            }, true);
        let allValid = (allValidInput && allValidTextArea && allValidLookup);

        return allValid;

    }


    /**
     * notify user
     * @param {string} title
     * @param {string} message
     * @param {string} variant
     */
    notifyUser(title, message, variant) {
        const toastEvent = new ShowToastEvent({
            title,
            message,
            variant
        });
        this.dispatchEvent(toastEvent);
    }

}