import { LightningElement, api, track } from 'lwc';

export default class MobilePropertyExplorerMap extends LightningElement {

    @track properties;
    @api urlParams;

    @api setProperties(properties){
        this.properties = properties;
        this.numberOfResults = this.properties.length;
    }

    // @api setUrlParams(params)
    // {
    //     this.urlParams = params;
    // }

    // get showMap()
    // {
    //     return this.urlParams ? true : false;
    // }

    get visualforcePageUrl()
    {
      return `/apex/mobilePropertyExplorerMap?latitude=${this.urlParams.latitude}&longitude=${this.urlParams.longitude}&distanceMin=${this.urlParams.distanceMin}&distanceMax=${this.urlParams.distanceMax}&rentedMin=${this.urlParams.rentedMin}&rentedMax=${this.urlParams.rentedMax}&typeOfUse=${this.urlParams.typeOfUse}`;
    }

}