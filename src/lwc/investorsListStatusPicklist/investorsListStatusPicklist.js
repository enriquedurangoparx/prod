import { LightningElement, track, api, wire } from 'lwc';
import { fireEvent, registerListener } from 'c/pubsub';
import STATUS_FIELD from '@salesforce/schema/AccountToOpportunity__c.Status__c';
import { getPicklistValues } from 'lightning/uiObjectInfoApi';
import { CurrentPageReference } from 'lightning/navigation';

export default class InvestorsListStatusPicklist extends LightningElement {
    @wire(CurrentPageReference) pageRef;
    @api rowId;
    @api status;
    @track options;
    @track statusField;
    rendered = true;
    allOptions = []; //used to hold all option values

    connectedCallback(){
        registerListener('investorsListStatusPicklist_refresh', this.refreshSelectedValue, this);
        registerListener('investorsListUnderbiddersModal_updateHasBeenCancelled', this.closingHasBeenCancelled, this);
    }

    renderedCallback(){
        this.statusField = STATUS_FIELD;
    }

    @wire(getPicklistValues, { recordTypeId: '012000000000000AAA', fieldApiName: '$statusField' })
    setPicklistOptions({error, data}) {
        if (data) {
            this.options = [];
            for(let i = 0; i < data.values.length; i++ ){
                let option = {};
                option.label = data.values[i].label;
                option.value = data.values[i].value;
                if(option.value == this.status){
                    option.selected = 'selected';
                }
                if(option.selected == 'selected' || (option.value != 'Offer' && option.value != 'Declination')){
                    this.options.push(option);
                }

                // We store all options in this array to have also the disabled options available on refresh
                this.allOptions.push(option);
            }
        } else if(error){
            console.log('error: ' + JSON.stringify(error, null, '\t'));
        }
    }

    handleStatusChange(event){
        let data = {}
        data.id = this.rowId;
        data.status = event.target.value;
        fireEvent(this.pageRef, 'investorsListStatusPicklist_statusChange', data);
    }

    refreshSelectedValue(data){
        // First update picklist (add update or declination value)
        for(var i = 0; i < data.length; i++){
            if(this.rowId === data[i].Id){
                if(data[i].Status__c == 'Offer' || data[i].Status__c == 'Declination'){
                    let statusAlreadyInPicklist = false;
                    for(var l = 0; l < this.options.length; l++){
                        if(this.options[l].value == data[i].Status__c){
                            statusAlreadyInPicklist = true;
                        }
                    }
                    if(!statusAlreadyInPicklist){
                        for(let j = 0; j < this.allOptions.length; j++ ){
                            if(this.allOptions[j].value == data[i].Status__c){
                                this.options.push(this.allOptions[j]);
                            }
                        }
                    }
                }
            }
        }

        // Set selected value
        for(var i = 0; i < data.length; i++){
            if(this.rowId === data[i].Id){
                for(var j = 0; j < this.options.length; j++){
                    // First reset
                    this.options[j].selected = '';

                    // Set the selected value
                    if(this.options[j].value == data[i].Status__c){
                        this.options[j].selected = 'selected';
                    }
                }
            }
        }
    }

    closingHasBeenCancelled(data) {
        let investorWhichIsGettingClosed = data.investorWhichIsGettingClosed;
        let investorPreviousStatus = data.investorPreviousStatus + '';
        console.log('...investorPreviousStatus: ' + investorPreviousStatus);
        if(this.rowId === investorWhichIsGettingClosed.Id){
            this.rendered = false;
            for(let j = 0; j < this.options.length; j++){
				this.options[j].selected = (this.options[j].value.toString() === investorPreviousStatus) ? 'selected' : '';
            }
            setTimeout(() => {
                this.rendered = true;
            }, 50);
        }
    }
}