import { LightningElement, api } from 'lwc';
// For the render() method
import template from './relatedObjectsTableCheckbox.html';

export default class relatedObjectsTableCheckbox extends LightningElement
{
    @api rowId;
    @api disabledandchecked;
    
    // Required for mixins
    render() {
        return template;
    }

    handleChange(event){
        // console.log('rowId: ', this.rowId);
        // console.log('event.target.checked: ', event.target.checked);
        // console.log('disabledandchecked: ', this.disabledandchecked);
        
        const evnt = CustomEvent('recordselect', {
            composed: true,
            bubbles: true,
            cancelable: true,
            detail: {
                rowId: this.rowId,
                checked: event.target.checked
            },
        });
        // console.log('dispatchEvent before: ');
        this.dispatchEvent(evnt);
        // console.log('dispatchEvent after: ');
    }
}