import { LightningElement, api, track } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';

import deleteInvestorsCall from '@salesforce/apex/InvestorsListController.deleteInvestors';

// import custom labels
import close from '@salesforce/label/c.InvestorsListCloseLabel';
import cancel from '@salesforce/label/c.investorsListCancelLabel';
import header from '@salesforce/label/c.InvestorsListStatusModalTitle';
import remove from '@salesforce/label/c.InvestorsListDeleteModalRemove';
import text from '@salesforce/label/c.InvestorsListDeleteModalText';
import toastTitle from '@salesforce/label/c.InvestorsListDeleteModalToastTitle';
import generalLabelError from '@salesforce/label/c.GeneralLabelError';
import generalLabelUnexpectedError from '@salesforce/label/c.GeneralLabelUnknownError';
import generalLabelCheck from '@salesforce/label/c.GeneralLabelCheck';


export default class InvestorsListDeleteModal extends LightningElement {
    @track showModal = false;
    @api recordsToDelete;

    label = {
        close,
        cancel,
        header,
        remove,
        text,
        toastTitle,
        toastMessage: ""
    }

    /********************
     * EXPOSED FUNCTIONS
     ********************/
    /**
     * open objective selection modal
     */
    @api
    openModal(recordsToDelete) {
        this.recordsToDelete = recordsToDelete;
        this.showModal = true;
    }

    /**
     * close objective selection modal
     */
    closeModal() {
        this.showModal = false;
    }

    /**
     * deletes Investors by fetching Id's from Objects in recordsToDelete Array
     */
    async deleteInvestors(){
        let investorIds = [];
        for(let i=0; i<this.recordsToDelete.length; i++){
            investorIds.push(this.recordsToDelete[i].Id);
        }
        const deleteInvestorsResult = JSON.parse(await deleteInvestorsCall({
            investorIds : investorIds
        }));
        console.log('deleteInvestorsResult ', JSON.stringify(deleteInvestorsResult));

        if (deleteInvestorsResult && deleteInvestorsResult.isSuccess)
        {
            this.invokeCustomEvent(deleteInvestorsResult);
        }
        else if (deleteInvestorsResult && !deleteInvestorsResult.isSuccess)
        {
            this.invokeCustomEvent(deleteInvestorsResult);
        }
        else
        {
            this.invokeCustomEvent(undefined);
        }
    }

    invokeCustomEvent(data)
    {
        if (data && data.isSuccess)
        {
            const event = CustomEvent('investorsdeleted', {
                composed: true,
                bubbles: true,
                cancelable: true,
                detail: {
                    toastTitle: data.title,
                    toastMessage: data.message,
                    toastVariant: 'success'
                },
            });
            this.dispatchEvent(event);
            this.closeModal();
        } 
        else if (!data.isSuccess)
        {
            this.notifyUserWithUrl(data.title, data.message, data.recordId, 'error');
        }
        else 
        {
            this.notifyUser(generalLabelError, generalLabelUnexpectedError, 'error');
        }        
    }

    /**
     * notify user
     * @param {string} title
     * @param {string} message
     * @param {string} variant
     */
    notifyUser(title, message, variant) {
        const toastEvent = new ShowToastEvent({
            title,
            message,
            variant
        });
        this.dispatchEvent(toastEvent);
    }

    notifyUserWithUrl(title, message, recId, variant) {
        const event = new ShowToastEvent({
            "title": title,
            "message": message,
            "variant": variant,
            "messageData": [
                {
                    url: '/' + recId,
                    label: generalLabelCheck
                }
            ]
        });
        this.dispatchEvent(event);
    }
}