import { LightningElement, api, track, wire } from 'lwc';
import { registerListener, unregisterAllListeners } from 'c/pubsub';
import { CurrentPageReference } from 'lightning/navigation';
import { fireEvent } from 'c/pubsub';

// apex controller methods
import findInvestorContacts from '@salesforce/apex/InvestorSelectionHelper.findInvestorContacts';
import loadInvestorsListItems from '@salesforce/apex/InvestorSelectionHelper.loadInvestorsListItems';
import loadMoreSelectedInvestors from '@salesforce/apex/InvestorSelectionHelper.loadMoreSelectedInvestors';
import loadMoreSelectedInvestorContacts from '@salesforce/apex/InvestorSelectionHelper.loadMoreSelectedInvestorContacts';
import loadMoreInvestorListItems from '@salesforce/apex/InvestorSelectionHelper.loadMoreInvestorListItems';
import loadMoreInvestorListItemContacts from '@salesforce/apex/InvestorSelectionHelper.loadMoreInvestorListItemContacts';

import searchRecords from '@salesforce/apex/InvestorSelectionHelper.search';
import searchByObjectIds from '@salesforce/apex/InvestorSelectionHelper.searchByObjectIds';
import searchMatchingContacts from '@salesforce/apex/LWCObjectMatchingController.getMatchingContacts';

// import custom labels:
import addSelection from '@salesforce/label/c.AddSelectionLabel';
import showInvestorContact from '@salesforce/label/c.InvestorsSelectionShowInvestorContact';
import hideInvestorContact from '@salesforce/label/c.InvestorsSelectionHideInvestorContact';
import recordSearchPlaceholder from '@salesforce/label/c.InvestorsSelectionRecordSearchPlaceholder';
import brandLabel from '@salesforce/label/c.InvestorsSelectionBrand';
import representativeLabel from '@salesforce/label/c.InvestorsSelectionRepresentative';
import streetLabel from '@salesforce/label/c.InvestorsSelectionStreet';
import cityLabel from '@salesforce/label/c.InvestorsSelectionCity';
import accountNameLabel from '@salesforce/label/c.InvestorsSelectionAccountName';
import firstnameLabel from '@salesforce/label/c.InvestorsSelectionFirstname';
import lastnameLabel from '@salesforce/label/c.InvestorsSelectionLastname';
import emailLabel from '@salesforce/label/c.InvestorsSelectionEmail';
import selectedAccountsHeading from '@salesforce/label/c.InvestorsSelectionHeadingSelectedAccounts';
import selectedContactsHeading from '@salesforce/label/c.InvestorsSelectionHeadingSelectedContacts';
import searchResultsHeading from '@salesforce/label/c.InvestorsSelectionHeadingSearchResults';
import selectAll from '@salesforce/label/c.investorSelectionDatatableSelectAll';
import unselectAll from '@salesforce/label/c.investorSelectionDatatableUnselectAll';
import generalLabelNoRecordsFound from '@salesforce/label/c.GeneralLabelNoRecordsFound';
import investorsSelectionInvestorsLabel from '@salesforce/label/c.InvestorsSelectionInvestorsLabel';
import investorsListSearchAccountContacts from '@salesforce/label/c.InvestorsListSearchAccountContacts';
import contractEntries from '@salesforce/label/c.MyListDetailDataComponentContactEntries';
import accountEntries from '@salesforce/label/c.MyListDetailDataComponentAccountEntries';
import investorsSelectionShowed from '@salesforce/label/c.InvestorsSelectionShowed';
import investorsSelectionHidden from '@salesforce/label/c.InvestorsSelectionHidden';
import investorsSelectionAlreadyInInvestorList from '@salesforce/label/c.InvestorsSelectionAlreadyInInvestorList';
import investorsSelectionAdjustSearchParameter from '@salesforce/label/c.InvestorsSelectionAdjustSearchParameter';


// The delay used when debouncing event handlers before invoking Apex.
const DELAY = 300;

// constants for sorting by default
const DEFAULT_SORT_FIELD = 'Name';
const DEFAULT_SORT_DIRECTION = 'asc';
const ACCOUNT_FORM_FIELDS = ['Name', 'Brand__c', 'BillingStreet', 'BillingCity'];
const CONTACT_FORM_FIELDS = ['Title', 'FirstName', 'LastName', 'Position__c', 'Email'];

const columns = [
    {label: '', type: 'deleteButtonRow', fieldName: 'investorContactID', fixedWidth: 36},
    { label: accountNameLabel, fieldName: '_InvestorId',  type: 'url',typeAttributes: { label: {fieldName: 'InvestorName'}, target:'_blank'}, sortable: true },
    { label: firstnameLabel, fieldName: '_ContactIdForName',  type: 'url',typeAttributes: { label: {fieldName: 'FirstName'}, target:'_blank'}, sortable: true },
    { label: lastnameLabel, fieldName: '_ContactIdForSurname',  type: 'url', typeAttributes: { label: {fieldName: 'LastName'}, target:'_blank'}, sortable: true },
    { label: representativeLabel, fieldName: 'isRepresentative',sortable: true },
    { label: streetLabel, fieldName: 'InvestorBillingStreet',sortable: true },
    { label: cityLabel, fieldName: 'InvestorBillingCity',sortable: true },
    { label: emailLabel, fieldName: 'Email',sortable: true },
    { label: brandLabel, fieldName: '_InvestorBrandId',  type: 'url', typeAttributes: { label: {fieldName: 'InvestorBrandName'}, target:'_blank'}, sortable: true },
    { label: '', type: 'actionButtonRow', fieldName: 'Id', fixedWidth: 46, typeAttributes: {rowStatus: { fieldName: 'Stage'}}, cellAttributes: { class: { fieldName: 'bgClass' }}}
];

const investorColumns = [
    {label: '', type: 'deleteButtonRow', fieldName: 'investorId', fixedWidth: 36},
    { label: accountNameLabel, fieldName: '_InvestorId',  type: 'url',typeAttributes: { label: {fieldName: 'InvestorName'}, target:'_blank'}, sortable: true },
    { label: streetLabel, fieldName: 'InvestorBillingStreet',sortable: true },
    { label: cityLabel, fieldName: 'InvestorBillingCity',sortable: true },
    { label: brandLabel, fieldName: '_InvestorBrandId',  type: 'url', typeAttributes: { label: {fieldName: 'InvestorBrandName'}, target:'_blank'}, sortable: true },
    { label: '', type: 'actionButtonRow', fieldName: 'Id', fixedWidth: 46, typeAttributes: {rowStatus: { fieldName: 'Stage'}}, cellAttributes: { class: { fieldName: 'bgClass' }}}
];

const MODES = { Contact: 'CONTACT_SEARCH', Account: 'ACCOUNT_SEARCH', Opportunity: 'OPPORTUNITY_SEARCH' };

export default class InvestorSelectionDatatable extends LightningElement {
    @wire(CurrentPageReference) pageRef;
    @api opportunityId;
    @api selectedTab;
    MATCHING_TAB = 'Matching';
    LIST_TAB = 'List';
    SEARCH_TAB = 'Detailed Search';
    UNDERBIDDER_TAB = 'bidder';

    @track objectApiName;
    @track isLoading = false;
    @track investorListName = '';
    @track selectAllButtonVisible = true;

    @track error;
    @track showSearchResults = true;


    // Selected Investors without Contacts / Investors with Contacts table variables starts
    @track investorColumns = investorColumns;
    @track selectedInvestorWithoutContacts = [];
    @track selectedInvestorContact = [];
    @track displaySelectedItemsTable = false;

    get showSelectedInvestorWithoutContacts()
    {
        return this.selectedInvestorWithoutContacts && this.selectedInvestorWithoutContacts.length > 0 ? true : false;
    }

    get showSelectedInvestorWithContacts()
    {
        return this.selectedTab != this.UNDERBIDDER_TAB && this.selectedInvestorContact && this.selectedInvestorContact.length > 0 ? true : false;
    }
    // Selected Investors without Contacts / Investors with Contacts table variables ends

    // Table
    _data = [];// contains raw records, without filtering
    @track data = [];// Contains ONLY filtered records.
    @track columns = columns;
    @track selectedRecords = [];

    // Investor List Table
    @track showInvestorListItemsWithoutContactsTable;
    @track showInvestorListItemsWithContactsTable;
    @track showInvestorListTable;
    _listItemsWithoutContacts;
    @track listItemsWithoutContacts;
    _listItemsWithContacts;
    @track listItemsWithContacts;
    @track columnsListItemsWithoutContacts;
    @track columnsListItemsWithContacts;

    // Table sorting
    @track sortedBy = DEFAULT_SORT_FIELD;
    @track sortedBySelectedDataInvestor = DEFAULT_SORT_FIELD;
    @track sortedBySelectedDataInvestorContact = DEFAULT_SORT_FIELD;
    @track sortedByListInvestorWithoutContacts = DEFAULT_SORT_FIELD;
    @track sortedByListInvestorWithContacts = DEFAULT_SORT_FIELD;
    @track sortedDirection = DEFAULT_SORT_DIRECTION;

    // recordForm.js
    @track accountFormFields;
    @track contactFormFields;
    @track formColumns; // Number of columns in record-form
    @track buttonAddSelectionIsDisabled = true;

    label = {
        addSelection,
        showInvestorContact,
        hideInvestorContact,
        recordSearchPlaceholder,
        selectedAccountsHeading,
        selectedContactsHeading,
        searchResultsHeading,
        selectAll,
        unselectAll,
        generalLabelNoRecordsFound,
        investorsSelectionInvestorsLabel,
        investorsListSearchAccountContacts,
        contractEntries,
        accountEntries,
        investorsSelectionShowed,
        investorsSelectionHidden,
        investorsSelectionAlreadyInInvestorList,
        investorsSelectionAdjustSearchParameter
    };

    currentMode;

    // @track
    // displayHelpMessage;

    @track
    helpMessage;
    
	connectedCallback() {
        registerListener('InvestorSelection__resetPage', this.resetPageState, this);
        registerListener('InvestorSelectionDatatable__isLoading', this.indicateLoading, this);
        registerListener('InvestorSelectionTab__searchRecords', this.renderData, this);
        registerListener('InvestorSelectionTab__searchRecordsbylistview', this.renderDataByListview, this);
        registerListener('InvestorSelection__addSelection', this.addSelection, this);
        registerListener('InvestorSelectionTab__checkboxChecked', this.checkboxChecked, this);
        // registerListener('InvestorSelection__resetPage', this.fetchInvestorContactRecords, this);

        // added 12.12.2019 : egmark : Matching US
        registerListener('InvestorSelection__matchingRecords', this.refreshMatchingRecords, this);
        registerListener('InvestorSelection__showInvestorListItem', this.showInvestorListItem, this);

        // ema : US693
        registerListener('InvestorSelection__bidderRecords', this.showBidderRecords, this);
        registerListener('InvestorSelection__bidderStopLoading', this.stopLoadingBidderRecords, this);

        // ema : US 2591
        registerListener('investor_selection_datatable__toggle_checkbox', this.toggleCheckbox, this);
        // this.fetchInvestorContactRecords();
    }

    showInvestorWithoutContactsRowData(event) {
        const rowId = event.detail.rowId;
        const selectedRow = this.selectedInvestorWithoutContacts.find((row) => {
            return row.Id === rowId;
        });
        this.dispatchRowSelectedEvent(selectedRow['Id'], null);
    }


    showInvestorContactRowData(event) {
        const rowId = event.detail.rowId;
        const selectedRow = this.selectedInvestorContact.find((row) => {
            return row.Id === rowId;
        });
        this.dispatchRowSelectedEvent(selectedRow['AccountId'], selectedRow['Id']);
    }


    showUnderbidderRowData(rowId) {
        const selectedRow = this.data.find((row) => {
            return row.accountId === rowId;
        });
        this.dispatchRowSelectedEvent(selectedRow['accountId'], null);
    }

    showListInvestorRow(event) {
        let rowId = event.detail.rowId;
        const selectedRow = this.listItemsWithoutContacts.find((row) => {
            return row.accountId === rowId;
        });
        this.dispatchRowSelectedEvent(selectedRow['accountId'], null);
    }

    showListInvestorContactRow(event) {
        let rowId = event.detail.rowId;
        const selectedRow = this.listItemsWithContacts.find((row) => {
            return row.Id === rowId;
        });
        this.dispatchRowSelectedEvent(selectedRow['AccountId'], selectedRow['Id']);
    }

    showRowData(event) {
        let rowId = event.detail.rowId;
        let dataCopy = JSON.parse(JSON.stringify(this.data));
        let selectedRow = "undefined";

        if (this.selectedTab === this.UNDERBIDDER_TAB) {
            this.showUnderbidderRowData(rowId);
            return;
        }

        for (var i = 0; i < dataCopy.length; i++) {
            if(dataCopy[i].Id === rowId){
                selectedRow = dataCopy[i];
            }
        }

        if(selectedRow !== 'undefined'){
            this.dispatchRowSelectedEvent(
                this.currentMode === MODES.Account ? selectedRow.record.Id : selectedRow.AccountId,
                this.currentMode === MODES.Account ? null : selectedRow.Id
            );
        }
    }

    dispatchRowSelectedEvent(accountId, contactId) {
        let detail = {};
        detail.show = {
            account: true,
            contact: true
        };

        detail.account = {
            detail: {
                recordId: accountId,
                objectApiName: 'Account',
                fields: this.accountFormFields,
                mode: 'view',
                columns: this.formColumns
            }
        };

        detail.contact = {
            detail: {
                recordId: contactId,
                objectApiName: 'Contact',
                fields: this.contactFormFields,
                mode: 'view',
                columns: this.formColumns
            }
        };

        const selectedRowEvent = new CustomEvent(
            'onerecordselected',
            {
                detail
            }
        );
        this.dispatchEvent(selectedRowEvent);
    }

    get showDataTabe()
    {
        return this.data && this.data.length > 0 ? true : false;
    }

    // get isListTab()
    // {
    //     return this.selectedTab === 'List' ? true : false;
    // }

    showInvestorListItem(data) {
        if (data.isLoading)
        {
            this.initLazyLoadState();
            this.investorListName = ' - ' + data['investorListName'];
            this.isLoading = true;
            this.data = [];
            this._data = [];
            this.listItemsWithContacts = [];
            this.listItemsWithoutContacts = [];
            this._listItemsWithContacts = [];
            this._listItemsWithoutContacts = [];
            this.tableColumns = data.tableMetadata.columnsInvestorListItems;
            this.accountFormFields = data.formMetadata.fields.account;
            this.contactFormFields = data.formMetadata.fields.contact;
            this.formColumns = data.formMetadata.columnsInvestorListItems;
            this.columnsListItemsWithContacts = data.tableMetadata.columnsInvestorListItems;
            this.columnsListItemsWithoutContacts = data.tableMetadata.columnsInvestorListItemsWithout;
            this.investorListId = data.investorListId;
            this.selectedInvestorItemContacts = new Map();
            this.selectedInvestorItems = new Map();
        }
        else
        {
            // SECTION FOR ITEMS WITHOUT CONTACTS STARTS
            this._listItemsWithoutContacts = data.investorListItemsWithoutContacts ? JSON.parse(data.investorListItemsWithoutContacts) : [];
            this.listItemsWithoutContacts = this._listItemsWithoutContacts;
            this.showInvestorlistItemsWithoutContactsTable = this.listItemsWithoutContacts && this.listItemsWithoutContacts.length > 0 ? true : false;
            this.listItemLoadedSize = this.listItemsWithoutContacts.length;
            // SECTION FOR ITEMS WITHOUT CONTACTS ENDS

            // SECTION FOR ITEMS WITH CONTACTS STARTS
            this._listItemsWithContacts = data.investorListItems ? JSON.parse(data.investorListItems) : [];

            this.listItemsWithContacts = this._listItemsWithContacts;
            this.showInvestorListItemsWithContactsTable = this.listItemsWithContacts && this.listItemsWithContacts.length > 0 ? true : false;
            this.listItemContactLoadedSize = this.listItemsWithContacts.length;
            // SECTION FOR ITEMS WITH CONTACTS ENDS
            
            if (this.showInvestorlistItemsWithoutContactsTable || this.showInvestorListItemsWithContactsTable)
            {
                this.showInvestorListTable = true;
            }
            else
            {
                this.showInvestorListTable = false;
            }

            this.showSearchResults = true;
            // this.displaySelectedItemsTable = false;
            this.isLoading = false;
        }
    }

    get showItemsWithoutContactTable()
    {
        return this.data && this.data.length > 0 ? true : false;
    }

    get showItemsContactTabe()
    {
        return this.data && this.data.length > 0 ? true : false;
    }

    showBidderRecords(data)
    {
        this.isLoading = true;
        this.tableColumns = data.tableMetadata.columns;
        this.accountFormFields = data.formMetadata.fields.account;
        this.contactFormFields = data.formMetadata.fields.contact;
        this.formColumns = data.formMetadata.columns;

        this.tableColumns = data.tableMetadata.columns;

        this._data = JSON.parse(data.bidderRecords);
        this._data = this._data ? this._data : [];

        this.data = JSON.parse(JSON.stringify(this._data));

        this.isLoading = false;
        this.searchDataTableIsLoading = false;
        // if true lazy load turns off, the variable indicates that we have no records anymore
        this.dataLoadingFinished = false;
    }

    selectedRecords = [];
    checkboxChecked(rowInformation) {
        console.log('....checkboxChecked: ' + rowInformation);
        let value = rowInformation.split('-')[0];
        console.log('...value: ' + JSON.stringify(value));
        let rowId = rowInformation.split('-')[1];
        let dataCopy = JSON.parse(JSON.stringify(this.data));
        let selectedRow = "undefined";
        console.log('...dataCopy: ' + JSON.stringify(dataCopy));
        console.log('...dataCopy.length: ' + dataCopy.length);
        for (var i = 0; i < dataCopy.length; i++) {
            if (dataCopy[i].Id === rowId || dataCopy[i].accountId === rowId)
            {
                if (value === 'true')
                {
                        selectedRow = this.currentMode === MODES.Account ? dataCopy[i]['record'] : dataCopy[i];
                        this.selectedRecords.push(selectedRow);
                }
                else
                {
                    for ( var j = 0; j < this.selectedRecords.length; j++)
                    {
                        if ( this.selectedRecords[j].Id === rowId || this.selectedRecords[j].accountId)
                        {
                            this.selectedRecords.splice(j, 1);
                        }
                    }
                }
            }
        }

        let numberOfSelectedRows = this.selectedRecords.length;
        this.toggleAddSelectionButtonAvailability(numberOfSelectedRows === 0 || numberOfSelectedRows === "undefined");
        console.log('...numberOfSelectedRows: ' + numberOfSelectedRows);
        //Notify investorSelection.js to reset both record-form.
        if(numberOfSelectedRows === 0){
            const noRowSelectedEvent = new CustomEvent('norowisselected');
            this.dispatchEvent(noRowSelectedEvent);
        }
    }

    selectedInvestorItems = new Map();
    selectedInvestorItemContacts = new Map();
    @track
    addButtonForInvestorIsDisabled = true;
    @track
    addButtonForInvestorContactIsDisabled = true;
    toggleCheckbox(obj)
    {
        let value = obj.isChecked;
        let rowId = obj.recordId;
        let dataCopy = [];

        switch(obj.tableName) 
        {
            case 'listTabInvestor':
                dataCopy = this.listItemsWithoutContacts ? this.listItemsWithoutContacts : [];
                break;
            case 'listTabInvestorContact':
                dataCopy = this.listItemsWithContacts ? this.listItemsWithContacts : [];
                break;
            default:
        }
        
        let selectedRow = "undefined";
        for (let i = 0; i < dataCopy.length; i++) {
            if (dataCopy[i].Id === rowId || dataCopy[i].accountId === rowId)
            {
                selectedRow = this.currentMode === MODES.Account ? dataCopy[i]['record'] : dataCopy[i];
                if (value)
                {
                    switch(obj.tableName) 
                    {
                        case 'listTabInvestor':

                            this.selectedInvestorItems.set(selectedRow.itemId, selectedRow);
                            break;
                        case 'listTabInvestorContact':
                            console.log('02');
                            this.selectedInvestorItemContacts.set(selectedRow.itemId, selectedRow);
                            break;
                        default:
                    }
                }
                else
                {
                    switch(obj.tableName) 
                    {
                        case 'listTabInvestor':
                            if (this.selectedInvestorItems.has(selectedRow.itemId))
                            {
                                this.selectedInvestorItems.delete(selectedRow.itemId);
                            }
                            break;
                        case 'listTabInvestorContact':
                            if (this.selectedInvestorItemContacts.has(selectedRow.itemId))
                            {
                                this.selectedInvestorItemContacts.delete(selectedRow.itemId);
                            }
                            break;
                        default:
                    }
                }
            }
        }
        switch(obj.tableName) 
        {
            case 'listTabInvestor':
                this.addButtonForInvestorIsDisabled = this.selectedInvestorItems && this.selectedInvestorItems.size === 0 ? true : false;
                break;
            case 'listTabInvestorContact':
                this.addButtonForInvestorContactIsDisabled = this.selectedInvestorItemContacts && this.selectedInvestorItemContacts.size === 0 ? true : false;
                break;
            default:
        }

        console.log('=== after ===');
        console.log('111111111');
        console.log('this.selectedInvestorItems: ', JSON.stringify(Array.from(this.selectedInvestorItems.values())));
        console.log('=== after ===');

        // let numberOfSelectedRows = selectedInvestorItems.length;
        // //Notify investorSelection.js to reset both record-form.
        // if(numberOfSelectedRows === 0){
        //     const noRowSelectedEvent = new CustomEvent('norowisselected');
        //     this.dispatchEvent(noRowSelectedEvent);
        // }
    }
    
    fetchInvestorContactRecords()
    {
        this.isLoading = true;
        this.initLazyLoadState();
        findInvestorContacts({recId: this.opportunityId, limitSize: this.selectedRecordsLimitSize})
        .then(result => {
            if (this.selectedTab !== this.UNDERBIDDER_TAB)
            {
                this.notifyParentThatRecordsAvailableEvent(result);
            }
            const tempResult = JSON.parse(JSON.stringify(result));
            this._preprocessSelectedData(tempResult, false, undefined);
            // let hasSelectedRecords = this.selectedInvestorContact.length > 0 || this.selectedInvestorWithoutContacts.length  > 0 ? true : false;

            this.toggleControl(true, false, false)

            if (this.selectedTab === this.UNDERBIDDER_TAB)
            {
                // this.displayHelpMessage = this.selectedInvestorWithoutContacts.length > 0 ? false : true;
                // this.helpMessage = this.label.generalLabelNoRecordsFound;
            }
        });
    }

    loadMoreSelectedInvestorContacts()
    {
        // prevent lazy load if we have no records anymore
        if (this.selectedInvestorsContactsLoadingFinished)
        {
            this.selectedContactsLoading = false;
            return;
        }

        this.selectedContactsLoading = true;
        this.selectedInvestorsContactsLimitSize = this.selectedInvestorsContactsLimitSize + this.selectedInvestorsContactsLimitStep;
        loadMoreSelectedInvestorContacts({recId: this.opportunityId, limitSize: this.selectedInvestorsContactsLimitSize})
            .then((result) => {
                const tempResult = JSON.parse(JSON.stringify(result));
                this._preprocessSelectedData(tempResult, true, 'investorcontacts');
                this.selectedContactsLoading = false;
            });
    }

    loadMoreSelectedInvestors()
    {
        // prevent lazy load if we have no records anymore
        if (this.selectedInvestorsLoadingFinished)
        {
            this.selectedAccountsLoading = false;
            return;
        }

        this.selectedAccountsLoading = true;
        this.selectedInvestorsLimitSize = this.selectedInvestorsLimitSize + this.selectedInvestorsLimitStep;
        loadMoreSelectedInvestors({recId: this.opportunityId, limitSize: this.selectedInvestorsLimitSize})
            .then((result) => {
                const tempResult = JSON.parse(JSON.stringify(result));
                this._preprocessSelectedData(tempResult, true, 'investors');
                this.selectedAccountsLoading = false;
            });
    }

   
    loadMoreInvestorListItems()
    {
        // prevent lazy load if we try to filter table using searchInDatatableOnChangeHandler method
        if (this.filterValue)
        {
            return;
        }

        // prevent lazy load if we have no records anymore
        if (this.listItemLoadingFinished)
        {
            this.investorListItemsIsLoading = false;
            return;
        }

        this.investorListItemsIsLoading = true;
        this.listItemLimitSize = this.listItemLimitSize + this.listItemLimitSizeStep;
        loadMoreInvestorListItems({opportunityId: this.opportunityId, investorListId: this.investorListId, limitSize: this.listItemLimitSize})
            .then((result) => {
                this._listItemsWithoutContacts = JSON.parse(result).investorListItemsWithoutContacts;

                if (this.listItemLoadedSize < this._listItemsWithoutContacts.length)
                {
                    this.listItemsWithoutContacts = this._listItemsWithoutContacts;
                    this.listItemLoadedSize = this._listItemsWithoutContacts.length;
                }
                else
                {
                    this.listItemLoadingFinished = true;
                }

                this.investorListItemsIsLoading = false;
            });
    }
   
    loadMoreInvestorListItemContacts()
    {
        // prevent lazy load if we try to filter table using searchInDatatableOnChangeHandler method
        if (this.filterValue)
        {
            return;
        }

        // prevent lazy load if we have no records anymore
        if (this.listItemContactLoadingFinished)
        {
            this.investorListItemContactsIsLoading = false;
            return;
        }

        this.investorListItemContactsIsLoading = true;
        this.listItemContactLimitSize = this.listItemContactLimitSize + this.listItemLimitSizeStep;
        loadMoreInvestorListItemContacts({opportunityId: this.opportunityId, investorListId: this.investorListId, limitSize: this.listItemContactLimitSize})
            .then((result) => {
                this._listItemsWithContacts = JSON.parse(result).investorListItemsWithContacts;

                if (this.listItemContactLoadedSize < this._listItemsWithContacts.length)
                {
                    this.listItemsWithContacts = this._listItemsWithContacts;
                    this.listItemContactLoadedSize = this._listItemsWithContacts.length;
                }
                else
                {
                    this.listItemContactLoadingFinished = true;
                }
                
                this.investorListItemContactsIsLoading = false;
            });
    }

    handleRecordAdded()
    {
        this.isLoading = false;

        switch(this.selectedTab)
        {
            case this.UNDERBIDDER_TAB:
                fireEvent(this.pageRef, 'InvestorSelectionTabBidder__bidderTabInitData', {});
                break;
            case this.LIST_TAB:
                this.refreshInvestorListItems();
                break;
            case this.SEARCH_TAB:
                this.initLazyLoadState();
                this.searchTabGetRecords();
                break;
            case this.MATCHING_TAB:
                break;
            default:
        }
        // findInvestorContacts({recId: this.opportunityId, limitSize: this.selectedRecordsLimitSize})
        // .then(result => {
        //     const tempResult = JSON.parse(JSON.stringify(result));
        //     this._preprocessSelectedData(tempResult);

        //     // let hasSelectedRecords = this.selectedInvestorContact.length > 0 || this.selectedInvestorWithoutContacts.length  > 0 ? true : false;

        //     // this.toggleControl(false, false, false)

        //     this.toggleControlButtonsToInitialState();
        // });
        // ema : added to fix accumulating of this.selectedRecrods every time when records were added
    
        this.toggleControlButtonsToInitialState();

        fireEvent(this.pageRef, 'InvestorSelectionDatatable__recordadded', '');
       
    }

    refreshInvestorListItems()
    {
        loadInvestorsListItems({
            opportunityId: this.opportunityId,
            investorListId: this.investorListId,
            limitSize: 25
        })
        .then( result => {
            // SECTION FOR ITEMS WITHOUT CONTACTS STARTS
            let data = JSON.parse(result);

            this._listItemsWithoutContacts = data.investorListItemsWithoutContacts ? data.investorListItemsWithoutContacts : [];
            this.listItemsWithoutContacts = this._listItemsWithoutContacts;
            this.showInvestorlistItemsWithoutContactsTable = this.listItemsWithoutContacts && this.listItemsWithoutContacts.length > 0 ? true : false;
            this.listItemLoadedSize = this.listItemsWithoutContacts.length;
            // SECTION FOR ITEMS WITHOUT CONTACTS ENDS

            // SECTION FOR ITEMS WITH CONTACTS STARTS
            this._listItemsWithContacts = data.investorListItemsWithContacts ? data.investorListItemsWithContacts : [];

            this.listItemsWithContacts = this._listItemsWithContacts;
            this.showInvestorListItemsWithContactsTable = this.listItemsWithContacts && this.listItemsWithContacts.length > 0 ? true : false;
            this.listItemContactLoadedSize = this.listItemsWithContacts.length;
            // SECTION FOR ITEMS WITH CONTACTS ENDS
            
            if (this.showInvestorlistItemsWithoutContactsTable || this.showInvestorListItemsWithContactsTable)
            {
                this.showInvestorListTable = true;
            }
            else
            {
                this.showInvestorListTable = false;
            }

            this.isLoading = false;
        }).catch(error => {
            this.isLoading = false;
        });
    }

    refreshSelectedRecords(event)
    {
        this.selectedRecords = [];
    }

	disconnectedCallback() {
		unregisterAllListeners(this);
    }

    // lazy load variables for search / matching / bidder
    searchDataLimitSize = 25;
    searchDataLimitSizeStep = 25;
    @track
    searchParameter = '';

    @track
    searchDataTableIsLoading = false;

    @track
    loadedDataSize = 0;

    @track
    dataLoadingFinished = false;

    // lazy load variables for selected records
    selectedRecordsLimitSize = 25;

    selectedInvestorsLoadingFinished = false;
    selectedInvestorsLimitSize = 25
    selectedInvestorsLimitStep = 10;
    selectedInvestorsLoadedSize = 0;

    selectedInvestorsContactsLoadingFinished = false;
    selectedInvestorsContactsLimitSize = 25;
    selectedInvestorsContactsLimitStep = 10;
    selectedInvestorsContactsLoadedSize = 0;

    @track
    selectedContactsLoading = false;
    @track
    selectedAccountsLoading = false;

    // lazy load variables for list tab
    @track
    investorListItemsIsLoading = false;
    @track
    investorListItemContactsIsLoading = false;
    listItemLimitSize = 25;
    listItemLoadedSize = 0;
    listItemContactLimitSize = 25;
    listItemContactLoadedSize = 0;
    listItemLimitSizeStep = 10;

    listItemLoadingFinished = false;
    listItemContactLoadingFinished = false;
    investorListId;
    

    initLazyLoadState()
    {
        this.searchDataLimitSize = 25;
        this.searchDataLimitSizeStep = 25;
        this.selectedRecordsLimitSize = 25;
        this.selectedInvestorsLimitSize = 25
        this.selectedInvestorsLimitStep = 10;
        this.selectedInvestorsContactsLimitSize = 25;
        this.selectedInvestorsContactsLimitStep = 10;

        this.loadedDataSize = 0;
        this.selectedInvestorsLoadedSize = 0;
        this.selectedInvestorsContactsLoadedSize = 0;

        this.dataLoadingFinished = false;
        this.selectedContactsLoading = false;
        this.selectedAccountsLoading = false;
        this.selectedInvestorsLoadingFinished = false;
        this.selectedInvestorsContactsLoadingFinished = false;

        this.investorListItemsIsLoading = false;
        this.investorListItemContactsIsLoading = false;
        this.listItemLimitSize = 25;
        this.listItemContactLimitSize = 25;
        this.listItemLimitSizeStep = 10;
        this.listItemLoadingFinished = false;
        this.listItemContactLoadingFinished = false;
        this.listItemLoadedSize = 0;
        this.listItemContactLoadedSize = 0;

        this.searchListViewObjectTab = '';
        // this.investorListId = '';
    }

    /**
     * Lazy load for bidder tab
     * invokes from investorSelectionTabBidder to prevent lazy loading if there are no any records
     * turns off is-loading flag on datatable
     */
    stopLoadingBidderRecords()
    {
        this.dataLoadingFinished = true;
        this.loadMoreData();
    }


    renderData(data) {
        this.initLazyLoadState();
        
        // this.displaySelectedItemsTable = false;
        this.accountFormFields = data.formMetadata.fields.account;
        this.contactFormFields = data.formMetadata.fields.contact;
        this.formColumns = data.formMetadata.columns;
        this.searchParameter = data.searchParameter.string_searchTerm;

        this.tableColumns = data.tableMetadata.columns;
        this.objectApiName = data.searchParameter.string_objectApiName;

        if (this.currentMode !== MODES[this.objectApiName]) {
            // flush selectedRecords on mode change
            this.selectedRecords = [];
        }
        this.currentMode = MODES[this.objectApiName];

        searchRecords({
            opportunityId : this.opportunityId,
            string_objectApiName : this.objectApiName,
            string_searchTerm : this.searchParameter,
            limitSize : this.searchDataLimitSize
        })
        .then(result => {
            // Notify investorSelection.js to show or information
            this.notifyParentThatRecordsAvailableEvent(result);

            this.error = undefined;
            if (this.objectApiName === 'Account') {
                this.data = result.map(row => {
                    const Id = row['record']['Id'];
                    const _InvestorId = '/' + Id;
                    const InvestorName = row['record']['Name'];
                    const InvestorBillingStreet = row['record']['BillingStreet'];
                    const InvestorBillingCity = row['record']['BillingCity'];
                    const _InvestorBrandId = row['record']['Brand__c'] ? '/' + row['record']['Brand__c'] : '';
                    const InvestorBrandName = row['record']['Brand__r'] ? row['record']['Brand__r']['Name'] : '';
                    const disabledandchecked = row['bAlreadyExists'];
                    const bgClass = disabledandchecked ? 'slds-theme_shade' : '';
                    return { ...row, disabledandchecked, Id, _InvestorId, InvestorName, InvestorBillingStreet, InvestorBillingCity, _InvestorBrandId, InvestorBrandName, bgClass };
                });
                this._data = JSON.parse(JSON.stringify(this.data));
                this.loadedDataSize = this._data.length;
            } else {
                const tempResult = JSON.parse(JSON.stringify(result));
                this._data = this._preprocessData(tempResult);// Raw unfiltered records
                this.data = JSON.parse(JSON.stringify(this._data));// Initial setup for filtered list.
                this.loadedDataSize = this._data.length;
            }

            this.showSearchResults = this.data.length > 0;
            this.isLoading = false;
        }).catch(error => {
            this.error = error;
            this.isLoading = false;
        })
    }
    
    loadMoreData()
    {
        // prevent lazy load if we try to filter table using searchInDatatableOnChangeHandler method
        if (this.filterValue)
        {
            return;
        }

        // prevent lazy load if we have no records anymore
        if (this.dataLoadingFinished)
        {
            this.searchDataTableIsLoading = false;
            return;
        }

        this.searchDataTableIsLoading = true;
        // load more for Search tab
        switch(this.selectedTab) 
        {
            case 'Detailed Search':
                this.searchDataLimitSize = this.searchDataLimitSize + this.searchDataLimitSizeStep;

                if (this.searchListViewObjectTab)
                {
                    // Lazy load for tab Search / searching by list view
                    searchByObjectIds({
                        objectName: this.searchListViewObjectTab, 
                        recordIds : this.searchListViewRecordIds, 
                        opportunityId : this.opportunityId,
                        limitSize : this.searchDataLimitSize
                    })
                    .then(result => {
                        const tempResult = JSON.parse(JSON.stringify(result));

                        if (this.loadedDataSize < tempResult.length)
                        {
                            this._data = this._preprocessData(tempResult);
                            this.data = JSON.parse(JSON.stringify(this._data));
                            this.loadedDataSize = tempResult.length;
                        }
                        else
                        {
                            this.dataLoadingFinished = true;
                        }
                        this.searchDataTableIsLoading = false;
                    });
                }
                else
                {
                    // Lazy load for tab Search / searching by input search
                    searchRecords({
                        opportunityId : this.opportunityId,
                        string_objectApiName : this.objectApiName,
                        string_searchTerm : this.searchParameter,
                        limitSize : this.searchDataLimitSize
                    })
                    .then(result => {
                        const tempResult = JSON.parse(JSON.stringify(result));
        
                        if (this.loadedDataSize < tempResult.length)
                        {
                            this.error = undefined;
                            if (this.objectApiName === 'Account') {
                                this.data = result.map(row => {
                                    const Id = row['record']['Id'];
                                    const _InvestorId = '/' + Id;
                                    const InvestorName = row['record']['Name'];
                                    const InvestorBillingStreet = row['record']['BillingStreet'];
                                    const InvestorBillingCity = row['record']['BillingCity'];
                                    const _InvestorBrandId = row['record']['Brand__c'] ? '/' + row['record']['Brand__c'] : '';
                                    const InvestorBrandName = row['record']['Brand__r'] ? row['record']['Brand__r']['Name'] : '';
                                    const disabledandchecked = row['bAlreadyExists'];
                                    const bgClass = disabledandchecked ? 'slds-theme_shade' : '';
                                    return { ...row, disabledandchecked, Id, _InvestorId, InvestorName, InvestorBillingStreet, InvestorBillingCity, _InvestorBrandId, InvestorBrandName, bgClass };
                                });
                                this._data = JSON.parse(JSON.stringify(this.data));
                                this.loadedDataSize = tempResult.length;
                            } else {
                                this._data = this._preprocessData(tempResult);// Raw unfiltered records
                                this.data = JSON.parse(JSON.stringify(this._data));// Initial setup for filtered list.
                                this.loadedDataSize = tempResult.length;
                            }
        
                            this.searchDataTableIsLoading = false;
                        }
                        else
                        {
                            this.searchDataTableIsLoading = false;
                            this.dataLoadingFinished = true;
                        }
                    }).catch(error => {
                        this.error = error;
                        this.searchDataTableIsLoading = false;
                    })
                }
            break;
            case 'bidder':
                fireEvent(this.pageRef, 'InvestorSelectionTabBidder__bidderTabLoadMoreRecords', null);
            break;
            case 'List':
            break;
            case 'Matching':
                this.searchDataLimitSize = this.searchDataLimitSize + this.searchDataLimitSizeStep;

                searchMatchingContacts({
                    searchTerm : this.dataMatchingFilterList,
                    opportunityId : this.opportunityId,
                    limitSize : this.searchDataLimitSize
                })
                .then(result => {
                    const tempResult = JSON.parse(result);
                    this._data = this._preprocessData(tempResult['matchingRecords']);


                    if (this.loadedDataSize < this._data.length)
                    {
                        this.data = JSON.parse(JSON.stringify(this._data));
                        this.loadedDataSize = this._data.length;
                    }
                    else
                    {
                        this.dataLoadingFinished = true;
                    }

                    this.searchDataTableIsLoading = false;

                }).catch(error => {
                    this.error = error;
                })
            break;
            default:
        }
        
        
       
        
    }

    // Tab Search / searching records by list view
    @track
    searchListViewObjectTab;

    @track
    searchListViewRecordIds;
    renderDataByListview(data){
        this.initLazyLoadState();
        this.searchListViewObjectTab = data.searchParameters.objectName;
        this.searchListViewRecordIds = JSON.stringify(data.searchParameters.recordIds);
        this.accountFormFields = data.formMetadata.fields.account;
        this.contactFormFields = data.formMetadata.fields.contact;
        this.formColumns = data.formMetadata.columns;

        this.tableColumns = data.tableMetadata.columns;
        this.objectApiName = data.searchParameters.objectName;
        searchByObjectIds({
            objectName: this.searchListViewObjectTab, 
            recordIds : this.searchListViewRecordIds, 
            opportunityId : this.opportunityId,
            limitSize : this.searchDataLimitSize
        })
        .then(result => {
            // this.displaySelectedItemsTable = false;
            this.notifyParentThatRecordsAvailableEvent(result);
            const tempResult = JSON.parse(JSON.stringify(result));
            this._data = this._preprocessData(tempResult);
            this.data = JSON.parse(JSON.stringify(this._data));
            this.showSearchResults = this.data.length > 0;
            this.isLoading = false;
        });
    }


    notifyParentThatRecordsAvailableEvent(result)
    {
        let recordsAvailableEvent;

        if(result.length > 0){
            recordsAvailableEvent = new CustomEvent('recordsavailable', { detail: {
                showInformation: true
            }});
        } else {
            recordsAvailableEvent = new CustomEvent('recordsavailable', { detail: {
                showInformation: false
            }});
        }
        this.dispatchEvent(recordsAvailableEvent);
    }

    _preprocessData(result){
        let preprocessData = [];
        result.forEach(wrapper => {
            let element = wrapper.record;

            if(wrapper.bAlreadyExists === true){
                element.bgClass = 'slds-theme_shade';
                element.disabledandchecked = true;
            }

            if(typeof wrapper.investorContactID !== "undefined"){
                element.investorContactID = wrapper.investorContactID;
            }

            if(typeof element.FirstName !== 'undefined'){
                element._ContactIdForName = '/' + element.Id;
            }
            element._ContactIdForSurname = '/' + element.Id;

            if (typeof element.Account !== "undefined"){
                element.InvestorName = element.Account.Name;
                element._InvestorId = '/' + element.AccountId;
                element.InvestorBillingStreet = element.Account.BillingStreet;
                element.InvestorBillingCity = element.Account.BillingCity;

                if (typeof wrapper.investorId !== "undefined"){
                    if(wrapper.bAlreadyExists && wrapper.investorId !== element.Account.Id){
                        element.isRepresentative = 'X';
                    }
                }else if(element.AccountId !== element.Account.Id){
                    element.isRepresentative = 'X';
                }

            }
            if (typeof element.Brand__r !== "undefined" && Object.entries(element.Brand__r).length !== 0){
                element._InvestorBrandId = '/' + element.Brand__r.Id;
                element.InvestorBrandName = element.Brand__r.Name;
            }

            if (wrapper.acquisitionProfiles){
                element.AcquisitionProfiles = wrapper.acquisitionProfiles;
            }

            if(wrapper.investorRole){
                element.investorRole = wrapper.investorRole;
            }

            if (wrapper.isActive){
                element.isContactActive = true;
            }

            preprocessData.push(element);
        });

       return preprocessData;
    }

    // ema : added new method in order not to break exisiting logic
    _preprocessSelectedData(result, isLoadMore, selectedType){
        let investorList = [];
        let investorContactList = [];

        result.forEach(wrapper => {
            let element = wrapper.record;


            if(wrapper.bAlreadyExists === true){
                element.bgClass = 'slds-theme_shade';
                element.disabledandchecked = true;
            }

            if (typeof element.Brand__r !== "undefined" && Object.entries(element.Brand__r).length !== 0){
                element._InvestorBrandId = '/' + element.Brand__r.Id;
                element.InvestorBrandName = element.Brand__r.Name;
            }

            if (wrapper.isInvestor)
            {
                if (typeof wrapper.investorId !== "undefined"){
                    element.investorId = wrapper.investorId;
                }

                element.InvestorName = element.Name;
                element._InvestorId = '/' + element.Id;
                element.InvestorBillingStreet = element.BillingStreet;
                element.InvestorBillingCity = element.BillingCity;

                investorList.push(element);
            }
            else if (wrapper.isInvestorContact)
            {
                if (typeof wrapper.investorContactID !== "undefined"){
                    element.investorContactID = wrapper.investorContactID;
                }

                if (typeof element.FirstName !== 'undefined'){
                    element._ContactIdForName = '/' + element.Id;
                }
                element._ContactIdForSurname = '/' + element.Id;

                if (typeof element.Account !== "undefined"){
                    element.InvestorName = element.Account.Name;
                    element._InvestorId = '/' + element.AccountId;
                    element.InvestorBillingStreet = element.Account.BillingStreet;
                    element.InvestorBillingCity = element.Account.BillingCity;

                    if (typeof wrapper.investorId !== "undefined"){
                        if(wrapper.bAlreadyExists && wrapper.investorId !== element.Account.Id){
                            element.isRepresentative = 'X';
                        }
                    }else if(element.AccountId !== element.Account.Id){
                        element.isRepresentative = 'X';
                    }
                }

                investorContactList.push(element);
            }
        });

        if (isLoadMore)
        {
            if (selectedType === 'investors')
            {
                if (this.selectedInvestorsLoadedSize < investorList.length)
                {
                    this.selectedInvestorWithoutContacts = investorList;
                    this.selectedInvestorsLoadedSize = this.selectedInvestorWithoutContacts.length;
                }
                else
                {
                    this.selectedInvestorsLoadingFinished = true;
                    this.selectedAccountsLoading = false;
                }
            }
            else
            {
                if (this.selectedInvestorsContactsLoadedSize < investorContactList.length)
                {
                    this.selectedInvestorContact = investorContactList;
                    this.selectedInvestorsContactsLoadedSize = this.selectedInvestorContact.length;
                }
                else
                {
                    this.selectedInvestorsContactsLoadingFinished = true;
                    this.selectedContactsLoading = false;
                }
            }
        }
        else
        {
            this.selectedInvestorWithoutContacts = investorList;
            this.selectedInvestorContact = investorContactList;

            this.selectedInvestorsLoadedSize = this.selectedInvestorWithoutContacts ? this.selectedInvestorWithoutContacts.length : 0;
            this.selectedInvestorsContactsLoadedSize = this.selectedInvestorContact ? this.selectedInvestorContact.length : 0;
        }
        

        this.accountFormFields = ACCOUNT_FORM_FIELDS;
        this.contactFormFields = CONTACT_FORM_FIELDS;
    }

    /**
     * This is triggered when one record is selected on lightning-datatable.
     */
    @track
    filterValue;

    searchInDatatableOnChangeHandler(event){
        // Debouncing this method: Do not update the reactive property as long as this function is
        // being called within a delay of DELAY. This is to avoid a very large number of Apex method calls.
        window.clearTimeout(this.delayTimeout);
        const lowerCaseSearchTerm = event.target.value.toLowerCase();
        this.filterValue = lowerCaseSearchTerm;
        // eslint-disable-next-line @lwc/lwc/no-async-operation
        switch(this.selectedTab) 
        {
            case this.UNDERBIDDER_TAB:
                this.delayTimeout = setTimeout(() => {
                    this.data = this._data.filter(function (el) {
                        const accountName = ((el.accountName) ? el.accountName.toLowerCase().indexOf(lowerCaseSearchTerm) > -1 : false);
                        const investorLastBidDate = ((el.investorLastBidDate) ? el.investorLastBidDate.toLowerCase().indexOf(lowerCaseSearchTerm) > -1 : false);
                        const opportunityOwnerName = ((el.opportunityOwnerName) ? el.opportunityOwnerName.toLowerCase().indexOf(lowerCaseSearchTerm) > -1 : false);
                        const opportunityPropertyOrPortfolioTypeOfUse = ((el.opportunityPropertyOrPortfolioTypeOfUse) ? el.opportunityPropertyOrPortfolioTypeOfUse.toLowerCase().indexOf(lowerCaseSearchTerm) > -1 : false);
                        const opportunityPropertyOrPortfolioCity = ((el.opportunityPropertyOrPortfolioCity) ? el.opportunityPropertyOrPortfolioCity.toLowerCase().indexOf(lowerCaseSearchTerm) > -1 : false);
                        const opportunityRiskCategory = ((el.opportunityRiskCategory) ? el.opportunityRiskCategory.toLowerCase().indexOf(lowerCaseSearchTerm) > -1 : false);
                        const opportunityAssetShareDeal = ((el.opportunityAssetShareDeal) ? el.opportunityAssetShareDeal.toLowerCase().indexOf(lowerCaseSearchTerm) > -1 : false);
    
                        if(accountName || investorLastBidDate || opportunityOwnerName || opportunityPropertyOrPortfolioTypeOfUse || opportunityPropertyOrPortfolioCity || opportunityRiskCategory || opportunityAssetShareDeal)
                        {
                            return true;
                        }
    
                        return false;
                    });
                        this.sortData(this.sortBy, this.sortDirection, false);
                    }, DELAY);
                break;

            case this.LIST_TAB:
                switch(event.target.name) 
                {
                    case 'listTabInvestor':
                        this.delayTimeout = setTimeout(() => {
                            this.listItemsWithoutContacts = this._listItemsWithoutContacts.filter(function (el) 
                            {
                                const accountNameMatch = ((el.accountName) ? el.accountName.toLowerCase().indexOf(lowerCaseSearchTerm) > -1 : false);
                                const accountBillingStreetMatch = ((el.accountStreet) ? el.accountStreet.toLowerCase().indexOf(lowerCaseSearchTerm) > -1 : false);
                                const accountBillingCityMatch = ((el.accountCity) ? el.accountCity.toLowerCase().indexOf(lowerCaseSearchTerm) > -1 : false);
                                const brandNameMatch = ((el.accountBrandName) ? el.accountBrandName.toLowerCase().indexOf(lowerCaseSearchTerm) > -1 : false);

                                if(accountNameMatch || accountBillingStreetMatch || accountBillingCityMatch || brandNameMatch){
                                    return true;
                                }
            
                                return false;
                            });
                            // this.listItemsWithoutContacts = tempData;
                            this.sortListInvestorItemsData(this.sortedByListInvestorWithoutContacts, this.sortedDirection, 'listTabInvestor');

                        }, DELAY);
                        break;

                    case 'listTabInvestorContact':
                        this.delayTimeout = setTimeout(() => {
                            this.listItemsWithContacts = this._listItemsWithContacts.filter(function (el) 
                            {
                                const accountNameMatch = ((el.InvestorName) ? el.InvestorName.toLowerCase().indexOf(lowerCaseSearchTerm) > -1 : false);
                                const firstNameMatch = ((el.FirstName) ? el.FirstName.toLowerCase().indexOf(lowerCaseSearchTerm) > -1 : false);
                                const lastNameMatch = ((el.LastName) ? el.LastName.toLowerCase().indexOf(lowerCaseSearchTerm) > -1 : false);
                                const emailMatch = ((el.Email) ? el.Email.toLowerCase().indexOf(lowerCaseSearchTerm) > -1 : false);
                                const accountBillingStreetMatch = ((el.accountStreet) ? el.accountStreet.toLowerCase().indexOf(lowerCaseSearchTerm) > -1 : false);
                                const accountBillingCityMatch = ((el.accountCity) ? el.accountCity.toLowerCase().indexOf(lowerCaseSearchTerm) > -1 : false);
                                const brandNameMatch = ((el.accountBrandName) ? el.accountBrandName.toLowerCase().indexOf(lowerCaseSearchTerm) > -1 : false);

                                if(firstNameMatch || lastNameMatch || accountNameMatch || emailMatch || accountBillingStreetMatch || accountBillingCityMatch || brandNameMatch){
                                    return true;
                                }
            
                                return false;
                            });
                            // this.listItemsWithContacts = tempData;
                            this.sortListInvestorItemsData(this.sortedByListInvestorWithContacts, this.sortedDirection, 'listTabInvestorContact');
                        }, DELAY);
                        
                        break;
                    default:
                }
                break;

            default:
                this.delayTimeout = setTimeout(() => {
                    this.data = this._data.filter(function (el) {
                        let accountBillingStreetMatch = false;
                        let accountBillingCityMatch = false;
                        let brandNameMatch = false;
    
    
                        if(typeof el.Account !== "undefined"){
                            accountBillingStreetMatch = ((el.Account.BillingStreet) ? el.Account.BillingStreet.toLowerCase().indexOf(lowerCaseSearchTerm) > -1 : false);
                            accountBillingCityMatch = ((el.Account.BillingCity) ? el.Account.BillingCity.toLowerCase().indexOf(lowerCaseSearchTerm) > -1 : false);
                        } else if (el.record) {
                            accountBillingStreetMatch = ((el.record.BillingStreet) ? el.record.BillingStreet.toLowerCase().indexOf(lowerCaseSearchTerm) > -1 : false);
                            accountBillingCityMatch = ((el.record.BillingCity) ? el.record.BillingCity.toLowerCase().indexOf(lowerCaseSearchTerm) > -1 : false);
                        }
    
                        if(typeof el.Brand__r !== "undefined"){
                            brandNameMatch = ((el.Brand__r.Name) ? el.Brand__r.Name.toLowerCase().indexOf(lowerCaseSearchTerm) > -1 : false);
                        } else if (el.record && el.record.Brand__r) {
                            brandNameMatch = ((el.record.Brand__r.Name) ? el.record.Brand__r.Name.toLowerCase().indexOf(lowerCaseSearchTerm) > -1 : false);
                        }
    
                        const accountNameMatch = ((el.InvestorName) ? el.InvestorName.toLowerCase().indexOf(lowerCaseSearchTerm) > -1 : false);
                        const firstNameMatch = ((el.FirstName) ? el.FirstName.toLowerCase().indexOf(lowerCaseSearchTerm) > -1 : false);
                        const lastNameMatch = ((el.LastName) ? el.LastName.toLowerCase().indexOf(lowerCaseSearchTerm) > -1 : false);
                        const emailMatch = ((el.Email) ? el.Email.toLowerCase().indexOf(lowerCaseSearchTerm) > -1 : false);
    
                        if(firstNameMatch || lastNameMatch || accountNameMatch || emailMatch || accountBillingStreetMatch || accountBillingCityMatch || brandNameMatch){
                            return true;
                        }
    
                        return false;
                    });
                        this.sortData(this.sortBy, this.sortDirection, false);
                    }, DELAY);
        }
    }

    /**
     *
     * @param {*} event Datatable Sort Event
     *
     * @description event handler for sorting the header fields
     */
    updateColumnSorting(event) {
        this.sortedBy = event.detail.fieldName;
        this.sortedDirection = event.detail.sortDirection;

        this.sortData(this.sortedBy, this.sortedDirection);
   }

    /**
     *
     * @param {*} fieldname Name of the field to be sorted
     * @param {*} direction "asc" or "desc"
     * @param {*} isContactInvestorTable if sorting comes from contact table then true of false in other case
     *
     * @description function to sort the array
     */
    sortData(fieldname, direction){
        // Bug 2047, Bug 1712
        let fieldNameTypeAttributeMap = new Map([
            ["linkToAccount", "InvestorName"],
            ["linkToContactForName", "FirstName"],
            ["linkToContactForSurname", "LastName"],
            ["linkToBrand", "accountBrandName"],
            ["_InvestorId", "InvestorName"],
            ["_ContactIdForName", "FirstName"],
            ["_ContactIdForSurname", "LastName"],
            ["_InvestorBrandId", "InvestorBrandName"],
            ["Id", "disabledandchecked"],
        ]);

        fieldname = fieldNameTypeAttributeMap.get(fieldname) ? fieldNameTypeAttributeMap.get(fieldname) : fieldname;

        // serialize the data before calling sort function
        let parseData = this.displaySelectedItemsTable ? JSON.parse(JSON.stringify(this.selectedInvestorContact)) : JSON.parse(JSON.stringify(this.data));


        // Return the value stored in the field
        let keyValue = (a) => {
            return a[fieldname];
        };

        // cheking reverse direction
        let isReverse = direction === 'asc' ? 1 : -1;

        // sorting data
        parseData.sort((x, y) => {
            x = keyValue(x) ? keyValue(x) : ''; // handling null values
            y = keyValue(y) ? keyValue(y) : '';

            x = typeof x === 'string' ? x.toLowerCase() : x;
            y = typeof y === 'string' ? y.toLowerCase() : y;

            // sorting values based on direction
            return isReverse * ((x > y) - (y > x));
        });

        // set the sorted data to data table data
        if (this.displaySelectedItemsTable)
        {
            this.selectedInvestorContact = parseData;
        }
        else
        {
            this.data = parseData;
        }
    }

    sortSelectedInvestorWithoutContacts(event) {
        this.sortedBySelectedDataInvestor = event.detail.fieldName;
        this.sortedDirection = event.detail.sortDirection;

        this.sortSelectedData(this.sortedBySelectedDataInvestor, this.sortedDirection, 'investor');
    }

    sortSelectedInvestorContact(event) {
        this.sortedBySelectedDataInvestorContact = event.detail.fieldName;
        this.sortedDirection = event.detail.sortDirection;

        this.sortSelectedData(this.sortedBySelectedDataInvestorContact, this.sortedDirection, 'investorContact');
    }

    sortSelectedData(fieldname, direction, type){
        // Bug 2047, Bug 1712
        let fieldNameTypeAttributeMap = new Map([
            ["linkToAccount", "InvestorName"],
            ["linkToContactForName", "FirstName"],
            ["linkToContactForSurname", "LastName"],
            ["linkToBrand", "accountBrandName"],
            ["_InvestorId", "InvestorName"],
            ["_ContactIdForName", "FirstName"],
            ["_ContactIdForSurname", "LastName"],
            ["_InvestorBrandId", "InvestorBrandName"],
            ["Id", "disabledandchecked"],
            ["investorId", "investorId"],
            ["InvestorBillingStreet", "InvestorBillingStreet"],
            ["InvestorBillingCity", "InvestorBillingCity"],
        ]);

        fieldname = fieldNameTypeAttributeMap.get(fieldname) ? fieldNameTypeAttributeMap.get(fieldname) : fieldname;

        // serialize the data before calling sort function
        let parseData = type === 'investor' ? JSON.parse(JSON.stringify(this.selectedInvestorWithoutContacts)) : JSON.parse(JSON.stringify(this.selectedInvestorContact));


        // Return the value stored in the field
        let keyValue = (a) => {
            return a[fieldname];
        };

        // cheking reverse direction
        let isReverse = direction === 'asc' ? 1 : -1;

        // sorting data
        parseData.sort((x, y) => {
            x = keyValue(x) ? keyValue(x) : ''; // handling null values
            y = keyValue(y) ? keyValue(y) : '';

            x = typeof x === 'string' ? x.toLowerCase() : x;
            y = typeof y === 'string' ? y.toLowerCase() : y;

            // sorting values based on direction
            return isReverse * ((x > y) - (y > x));
        });

        // set the sorted data to data table data
        if (type === 'investor')
        {
            this.selectedInvestorWithoutContacts = parseData;
        }
        else if (type === 'investorContact')
        {
            this.selectedInvestorContact = parseData;
        }
    }

    sortListInvestorWithoutContacts(event) {
        this.sortedByListInvestorWithoutContacts = event.detail.fieldName;
        this.sortedDirection = event.detail.sortDirection;
        this.sortListInvestorItemsData(this.sortedByListInvestorWithoutContacts, this.sortedDirection, 'listTabInvestor');
    }

    sortListInvestorWithContacts(event) {
        this.sortedByListInvestorWithContacts = event.detail.fieldName;
        this.sortedDirection = event.detail.sortDirection;
        this.sortListInvestorItemsData(this.sortedByListInvestorWithContacts, this.sortedDirection, 'listTabInvestorContact');
    }

    
    sortListInvestorItemsData(fieldname, direction, type)
    {
        // Bug 2047, Bug 1712
        let fieldNameInvestorContactTypeAttributeMap = new Map([
            ["Id", "disabledandchecked"],
            ["linkToAccount", "InvestorName"],
            ["linkToContactForName", "FirstName"],
            ["linkToContactForSurname", "LastName"],
            ["isRepresentative", "isRepresentative"],
            ["accountStreet", "accountStreet"],
            ["accountCity", "accountCity"],
            ["Email", "Email"],
            ["linkToBrand", "accountBrandName"],
        ]);

        let fieldNameInvestorTypeAttributeMap = new Map([
            ["accountId", "disabledandchecked"],
            ["linkToAccount", "accountName"],
            ["accountStreet", "accountStreet"],
            ["accountCity", "accountCity"],
            ["accountBrandLink", "accountBrandName"],
        ]);
        if (type === 'listTabInvestor')
        {
            fieldname = fieldNameInvestorTypeAttributeMap.get(fieldname) ? fieldNameInvestorTypeAttributeMap.get(fieldname) : fieldname;
        }
        else if (type === 'listTabInvestorContact')
        {
            fieldname = fieldNameInvestorContactTypeAttributeMap.get(fieldname) ? fieldNameInvestorContactTypeAttributeMap.get(fieldname) : fieldname;
        }

        // serialize the data before calling sort function
        let parseData = type === 'listTabInvestor' ? JSON.parse(JSON.stringify(this.listItemsWithoutContacts)) : JSON.parse(JSON.stringify(this.listItemsWithContacts));

        // Return the value stored in the field
        let keyValue = (a) => {
            return a[fieldname];
        };

        // cheking reverse direction
        let isReverse = direction === 'asc' ? 1 : -1;

        // sorting data
        parseData.sort((x, y) => {
            x = keyValue(x) ? keyValue(x) : ''; // handling null values
            y = keyValue(y) ? keyValue(y) : '';

            x = typeof x === 'string' ? x.toLowerCase() : x;
            y = typeof y === 'string' ? y.toLowerCase() : y;

            // sorting values based on direction
            return isReverse * ((x > y) - (y > x));
        });
        // set the sorted data to data table data
        if (type === 'listTabInvestor')
        {
            this.listItemsWithoutContacts = parseData;
        }
        else if (type === 'listTabInvestorContact')
        {
            this.listItemsWithContacts = parseData;
        }
    }

    addSelectionHandler()
    {
        const child = this.template.querySelector('c-investor-selection-add-records-modal');
        child.openModal(this.selectedRecords, this.objectApiName, this.opportunityId, this.selectedTab);
    }

    addSelectionHandlerInvestor()
    {
        const child = this.template.querySelector('c-investor-selection-add-records-modal');
        child.openModal(Array.from(this.selectedInvestorItems.values()), 'Account', this.opportunityId, this.selectedTab);
    }

    addSelectionHandlerInvestorContacts()
    {
        const child = this.template.querySelector('c-investor-selection-add-records-modal');
        child.openModal(Array.from(this.selectedInvestorItemContacts.values()), 'Contact', this.opportunityId, this.selectedTab);
    }

    toggleAddSelectionButtonAvailability(isAvailable)
    {
        this.buttonAddSelectionIsDisabled = isAvailable;
    }

    indicateLoading(data){
        this.isLoading = data.isLoading;
    }
    
    @track
    dataMatchingFilterList;

    refreshMatchingRecords(data){
        this.initLazyLoadState();
        this.isLoading = true;
        // this.displaySelectedItemsTable = false;
        this.accountFormFields = data.formMetadata.fields.account;
        this.contactFormFields = data.formMetadata.fields.contact;
        this.formColumns = data.formMetadata.columns;
        this.tableColumns = data.tableMetadata.columns;
        this.dataMatchingFilterList = data.filterList;
        searchMatchingContacts({
            searchTerm : this.dataMatchingFilterList,
            opportunityId : this.opportunityId,
            limitSize : this.searchDataLimitSize
        })
        .then(result => {
            this.notifyParentThatRecordsAvailableEvent(result);

            this.error = undefined;
            const tempResult = JSON.parse(result);
            this._data = this._preprocessData(tempResult['matchingRecords']);
            this.loadedDataSize = this._data.length;
            this.data = JSON.parse(JSON.stringify(this._data));
            if (this.data.length > 0){
                this.showSearchResults = this.data.length > 0;
                // this.displaySelectedItemsTable = false;
            }
            else{
                // this.displaySelectedItemsTable = true;
                this.showSearchResults = false;
            }
            this.isLoading = false;
            fireEvent(this.pageRef, 'searchMatchingContactsCompleted', tempResult);
        }).catch(error => {
            this.error = error;
            this.isLoading = false;
        })
    }

    selectAll(){
        console.log('...selectAll');
        this.selectAllButtonVisible = false;
        const obj = {
            isChecked: true,
            tableName: 'cb'
        }
        fireEvent(this.pageRef, 'investorsSelection_selectOrUnSelectAll', obj);
    }

    unselectAll(){
        this.selectAllButtonVisible = true;
        const obj = {
            isChecked: false,
            tableName: 'cb'
        }
        fireEvent(this.pageRef, 'investorsSelection_selectOrUnSelectAll', obj);
    }

    @track
    selectAllInvestorsIsVisible = true;
    selectAllInvestors()
    {
        this.selectAllInvestorsIsVisible = false;
        const obj = {
            isChecked: true,
            tableName: 'listTabInvestor'
        }
        fireEvent(this.pageRef, 'investorsSelection_selectOrUnSelectAll', obj);
    }

    unselectAllInvestors()
    {
        this.selectAllInvestorsIsVisible = true;
        const obj = {
            isChecked: false,
            tableName: 'listTabInvestor'
        }
        fireEvent(this.pageRef, 'investorsSelection_selectOrUnSelectAll', obj);
    }

    @track
    selectAllInvestorContactsIsVisible = true;
    selectAllInvestorContacts()
    {
        this.selectAllInvestorContactsIsVisible = false;
        const obj = {
            isChecked: true,
            tableName: 'listTabInvestorContact'
        }
        fireEvent(this.pageRef, 'investorsSelection_selectOrUnSelectAll', obj);
    }

    unselectAllInvestorContacts()
    {
        this.selectAllInvestorContactsIsVisible = true;
        const obj = {
            isChecked: false,
            tableName: 'listTabInvestorContact'
        }
        fireEvent(this.pageRef, 'investorsSelection_selectOrUnSelectAll', obj);
    }

    toggleSelectedRecords(event)
    {
        if (event.target.checked)
        {
            this.fetchInvestorContactRecords();
        }
        else
        {
            this.toggleControlButtonsToInitialState();

            this.handleRecordAdded();
        }
        // else
        // {
        //     this.toggleControl(event.target.checked, true, false)
        // }
        this.displaySelectedItemsTable = event.target.checked;
    }

    toggleControl(displaySelectedItemsTable, showSearchResults, isLoading)
    {
        this.displaySelectedItemsTable = displaySelectedItemsTable;
        this.showSearchResults = showSearchResults;
        this.isLoading = isLoading;
    }
    
    get displayHelpMessage()
    {
        let displayMessage = false;
        // this.helpMessage = this.label.investorsSelectionAdjustSearchParameter;

        switch(this.selectedTab)
        {
            case this.UNDERBIDDER_TAB:
                displayMessage = this.data && this.data.length > 0 ? false : true;
                break;
            case this.LIST_TAB:
                displayMessage = this.showInvestorListTable ? false : true;
                break;
            case this.SEARCH_TAB:
                displayMessage = this.data && this.data.length > 0 ? false : true;
                break;
            case this.MATCHING_TAB:
                displayMessage = this.data && this.data.length > 0 ? false : true;
                break;
            default:
        }

        if (this.displaySelectedItemsTable)
        {
            displayMessage = (this.selectedInvestorWithoutContacts && this.selectedInvestorWithoutContacts.length > 0) || (this.selectedInvestorContact && this.selectedInvestorContact.length > 0) ? false : true;
            // this.helpMessage = this.label.generalLabelNoRecordsFound;
        }

        // const obj = {
        //     displayMessage: displayMessage,
        // }

        // fireEvent(this.pageRef, 'InvestorSelection__toggleRightSideHelpMessage', obj);

        return displayMessage;
    }


    searchTabGetRecords()
    {
        searchRecords({
            opportunityId : this.opportunityId,
            string_objectApiName : this.objectApiName,
            string_searchTerm : this.searchParameter,
            limitSize : this.searchDataLimitSize
        })
        .then(result => {
            // Notify investorSelection.js to show or information
            this.notifyParentThatRecordsAvailableEvent(result);

            this.error = undefined;
            if (this.objectApiName === 'Account') {
                this.data = result.map(row => {
                    const Id = row['record']['Id'];
                    const _InvestorId = '/' + Id;
                    const InvestorName = row['record']['Name'];
                    const InvestorBillingStreet = row['record']['BillingStreet'];
                    const InvestorBillingCity = row['record']['BillingCity'];
                    const _InvestorBrandId = row['record']['Brand__c'] ? '/' + row['record']['Brand__c'] : '';
                    const InvestorBrandName = row['record']['Brand__r'] ? row['record']['Brand__r']['Name'] : '';
                    const disabledandchecked = row['bAlreadyExists'];
                    const bgClass = disabledandchecked ? 'slds-theme_shade' : '';
                    return { ...row, disabledandchecked, Id, _InvestorId, InvestorName, InvestorBillingStreet, InvestorBillingCity, _InvestorBrandId, InvestorBrandName, bgClass };
                });
                this._data = JSON.parse(JSON.stringify(this.data));
                this.loadedDataSize = this._data.length;
            } else {
                const tempResult = JSON.parse(JSON.stringify(result));
                this._data = this._preprocessData(tempResult);// Raw unfiltered records
                this.data = JSON.parse(JSON.stringify(this._data));// Initial setup for filtered list.
                this.loadedDataSize = this._data.length;
            }

            this.showSearchResults = this.data.length > 0;
            this.isLoading = false;
        }).catch(error => {
            this.error = error;
            this.isLoading = false;
        })
    }



    resetPageState(event)
    {
        this.initLazyLoadState();
        if (event && event.isSelectedRecordDeleted)
        {
            this.fetchInvestorContactRecords();
        }
        else
        {
            this.data = [];
            this._data = [];
            this.investorSelectionListItems = [];
            this.investorSelectionListItemsWithoutContacts = [];
            this.displaySelectedItemsTable = false;
            this.toggleControlButtonsToInitialState();
        }
    }

    toggleControlButtonsToInitialState()
    {
        this.selectAllButtonVisible = true;
        this.selectAllInvestorsIsVisible = true;
        this.selectAllInvestorContactsIsVisible = true;
        this.addButtonForInvestorContactIsDisabled = true;
        this.addButtonForInvestorIsDisabled = true;
        this.buttonAddSelectionIsDisabled = true;

        this.selectedRecords = [];
        this.selectedInvestorItemContacts = new Map();
        this.selectedInvestorItems = new Map();
    }

    /**
     * notify user
     * @param {string} title
     * @param {string} message
     * @param {string} variant
     */
    notifyUser(title, message, variant) {
        const toastEvent = new ShowToastEvent({
            title,
            message,
            variant
        });
        this.dispatchEvent(toastEvent);
    }
}