import { LightningElement, api, track, wire } from 'lwc';
import { createMarker } from 'c/mapObject';

import { getRecord } from 'lightning/uiRecordApi';
import { getObjectInfo } from 'lightning/uiObjectInfoApi';

import { NavigationMixin } from 'lightning/navigation';

import OBJECT from '@salesforce/schema/PropertyObject__c';

import NAME_FIELD from '@salesforce/schema/PropertyObject__c.Name';
import MAIN_THUMBNAIL from '@salesforce/schema/PropertyObject__c.MainThumbnail__c';
import SQM_IN_TOTAL_FIELD from '@salesforce/schema/PropertyObject__c.SqmInTotal__c';
import OCCUPANCY_RATE_FIELD from '@salesforce/schema/PropertyObject__c.OccupancyRate__c';

import PROPERTY_OWNER_FIELD from '@salesforce/schema/PropertyObject__c.PropertyOwner__c';
import PROPERTY_OWNER__PARENT_FIELD from '@salesforce/schema/PropertyObject__c.PropertyOwner__r.Name';
import PROPERTY_STREET_FIELD from '@salesforce/schema/PropertyObject__c.Street__c';
import PROPERTY_POSTALCODE_FIELD from '@salesforce/schema/PropertyObject__c.PostalCode__c';
import PROPERTY_COUNTRY_FIELD from '@salesforce/schema/PropertyObject__c.Country__c';

export default class ObjectMap extends NavigationMixin(LightningElement) {
    @api recordId;

    //@track center = undefined;
    @track mapMarkers = [];
    @track zoomLevel = 16;

    @wire(getObjectInfo, { objectApiName: OBJECT })
    objectObjectInfo;

    @wire(getRecord, 
        {
            recordId: '$recordId',
            fields: [
                PROPERTY_STREET_FIELD,
                PROPERTY_POSTALCODE_FIELD,
                PROPERTY_COUNTRY_FIELD,
                PROPERTY_OWNER_FIELD,
                PROPERTY_OWNER__PARENT_FIELD,
                NAME_FIELD,
                MAIN_THUMBNAIL,
                'PropertyObject__c.Geolocation__Longitude__s',
                'PropertyObject__c.Geolocation__Latitude__s',
                SQM_IN_TOTAL_FIELD,
                OCCUPANCY_RATE_FIELD]
        })
    wiredRecord({ error, data }) {

        if(data){
            const latitudeValue = data.fields.Geolocation__Latitude__s.value;
            const longitudeValue = data.fields.Geolocation__Longitude__s.value;
            const nameValue = data.fields.Name.value;
            const sqmInTotalFieldName = (this.objectObjectInfo.data.fields[SQM_IN_TOTAL_FIELD.fieldApiName] !== 'undefined' ? this.objectObjectInfo.data.fields[SQM_IN_TOTAL_FIELD.fieldApiName].label : '');
            const sqmInTotalValue = ((data.fields.SqmInTotal__c.value !== null && data.fields.SqmInTotal__c.value !== '') ? this._replacePointWithComma(data.fields.SqmInTotal__c.value) + ' m²' : '');
            const occupancyRateFieldName = (this.objectObjectInfo.data.fields[OCCUPANCY_RATE_FIELD.fieldApiName] !== 'undefined' ? this.objectObjectInfo.data.fields[OCCUPANCY_RATE_FIELD.fieldApiName].label : '');
            const occupancyRateValue = ((data.fields.OccupancyRate__c.value !== null && data.fields.OccupancyRate__c.value !== '') ? data.fields.OccupancyRate__c.value : '');

            const propertyOwnerFieldName = (this.objectObjectInfo.data.fields.PropertyOwner__c !== 'undefined' ? this.objectObjectInfo.data.fields.PropertyOwner__c.label : '');
            const propertyOwnerValue = ((data.fields.PropertyOwner__r.value !== null && data.fields.PropertyOwner__r.value !== '') ? data.fields.PropertyOwner__r.displayValue : '');
            const propertyOwnerId = ((data.fields.PropertyOwner__r.value !== null && data.fields.PropertyOwner__r.value !== '') ? data.fields.PropertyOwner__r.value.id : '');
            const street = data.fields.Street__c ? data.fields.Street__c.value : ''; 
            const postalCode = data.fields.PostalCode__c ? data.fields.PostalCode__c.value : ''; 
            const country = data.fields.Country__c ? data.fields.Country__c.value : '';

            // this.center = { location: {Latitude: latitudeValue, Longitude: longitudeValue } };

            let propertyOwnerRecordPage = {
                type: "standard__recordPage",
                attributes: {
                    "recordId": propertyOwnerId,
                    "objectApiName": "Account",
                    "actionName": "view"
                }
            };

            let tempMapMarkers = [];
            this[NavigationMixin.GenerateUrl](propertyOwnerRecordPage)
                .then(url => {
                    const urlToPropertyOwner = window.location.origin + url;
                    const mainThumbNail = (data.fields.MainThumbnail__c.value !== null ? data.fields.MainThumbnail__c.value : '');
                    
                    let marker = createMarker(
                        '', 
                        latitudeValue,
                        longitudeValue,
                        nameValue,
                        `<div style="display: table; width: 100%">
                            <div style="display: table-row-group;">
                                <div style="display: table-row;">
                                    <div style="display: table-cell; padding: 3px 10px; vertical-align: top;">${mainThumbNail}</div>
                                    <div style="display: table-cell; padding: 3px 10px; vertical-align: top;">
                                        ${sqmInTotalFieldName}: ${sqmInTotalValue}<br />
                                        ${occupancyRateFieldName}: ${occupancyRateValue}<br />
                                        ${propertyOwnerFieldName}: <a target="_blank" href="${urlToPropertyOwner}">${propertyOwnerValue}</a><br />
                                    </div>
                                </div>
                            </div>
                        </div>`,
                        street,
                        postalCode, 
                        country, 
                        ''
                    );

                    tempMapMarkers.push(marker);
                    this.mapMarkers = tempMapMarkers;
                });
        } else if (error){
            this.error = error;
        }
    }

    _replacePointWithComma(number){
        return number.toString().replace('.', ',');
    }
}