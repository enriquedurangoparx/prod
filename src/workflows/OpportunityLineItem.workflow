<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Recalculate_Commission</fullName>
        <field>Discount</field>
        <formula>IF(
    AND(
        NOT(ISBLANK(Commission__c)),
        Commission__c &gt; 0
    ),
    1 - Commission__c,
    IF(
        ISCHANGED(Discount),
        Discount,
        0
    )
)</formula>
        <name>Recalculate Commission</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Commission Calculation</fullName>
        <actions>
            <name>Recalculate_Commission</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>OR(     ISCHANGED(Discount),     ISCHANGED(Commission__c),     AND(         NOT(ISBLANK(Commission__c)),         NOT(ISNULL(Commission__c)),         ISNEW()     ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
