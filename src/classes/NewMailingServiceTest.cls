/**
*	Tests for NewMailingService, please, note that most of the tests are just for coverage.
*	@author npo
*	@copyright PARX
*/
@IsTest
public class NewMailingServiceTest
{
    @testSetup
    static void setup()
    {
        TestDataFactory.initializeProducts();

        Embargo__c embargo = new Embargo__c();
        embargo.Name = 'Embargo';
        embargo.Countries__c = 'Egypt;Afghanistan;Belarus;Burundi;Iraq;Yemen';
        insert embargo;

        Account newAcc = TestDataFactory.createAccount('Test 1');
        insert newAcc;

        List<Contact> contacts = TestDataFactory.createContacts(newAcc.Id, 2);
        insert contacts;

        PropertyObject__c propertyObject = TestDataFactory.buildPropertyObject('property', true);
        Opportunity newOpportunity = TestDataFactory.createOpportunity(newAcc.Id, propertyObject);
        insert newOpportunity;

        AccountToOpportunity__c newAccToOpp = TestDataFactory.createAccountToOpportunity(newAcc.Id, newOpportunity.Id);
        insert newAccToOpp;

        List<ContactToInvestor__c> contactToInvestors = TestDataFactory.createContactToInvestor(newAccToOpp.Id, contacts);
        insert contactToInvestors;

        Campaign activeCampaign = new Campaign(IsActive = true, Name = 'Active', Opportunity__c = newOpportunity.Id, Sender__c = UserInfo.getUserId());
        Campaign inactiveCampaign = new Campaign(IsActive = false, Name = 'Inactive', Opportunity__c = newOpportunity.Id);

        List<EmailTemplate> emailTemplates = [SELECT Id, Subject, HtmlValue FROM EmailTemplate LIMIT 1];
        if (!emailTemplates.isEmpty())
        {
            activeCampaign.GermanEmailTemplateId__c = emailTemplates.get(0).Id;
            activeCampaign.EnglishEmailTemplateId__c = emailTemplates.get(0).Id;
        }

        insert new List<Campaign> {activeCampaign, inactiveCampaign};

        InvestorMailingConfiguration__c mailingConfiguration = new InvestorMailingConfiguration__c(Campaign__c = activeCampaign.Id,
                Investor__c = newAccToOpp.Id);
        CampaignMember member1 =
                new CampaignMember(CampaignId = activeCampaign.Id,
                        InvestorContact__c = contactToInvestors.get(0).Id,
                        Language__c = 'English',
                        SendingType__c = 'To',
                        ContactId = contacts.get(0).Id);
        CampaignMember member2 =
                new CampaignMember(CampaignId = activeCampaign.Id,
                        InvestorContact__c = contactToInvestors.get(1).Id,
                        Language__c = 'German',
                        SendingType__c = 'To',
                        ContactId = contacts.get(1).Id);
        insert new List<CampaignMember>{member1, member2};

        insert mailingConfiguration;
    }

    @IsTest
    static void testPrepareMessages()
    {
        Campaign activeCampaign = [SELECT Id, Opportunity__c, Sender__c, EnglishEmailTemplateId__c, GermanEmailTemplateId__c FROM Campaign WHERE IsActive = true LIMIT 1];
        List<InvestorMailingConfiguration__c> mailingConfigurations =
        [
                SELECT Id, Investor__c, GermanSalutationSequence__c, EnglishSalutationSequence__c
                FROM InvestorMailingConfiguration__c
                WHERE Campaign__c = :activeCampaign.Id
        ];
        Map<Id, CampaignMember> campaignMemberByInvestorContact = NewMailingService.prepareCampaignMemberMap(activeCampaign.Id);

        Test.startTest();

        List<Messaging.SingleEmailMessage> result = NewMailingService.prepareMessages(mailingConfigurations, campaignMemberByInvestorContact, activeCampaign.Id);

        Test.stopTest();

        if (result != null)
        {
            System.assertEquals(2, result.size(), 'We expect emails count to be equal to 2, as we have 2 campaign members with different languages.');
        }
    }

    @IsTest
    static void testPopulateEmails()
    {
        Campaign activeCampaign = [SELECT Id, Name, Opportunity__c, Sender__c, Bcc__c, InvestorStatus__c, EnglishEmailTemplateId__c, GermanEmailTemplateId__c, IsActive FROM Campaign WHERE IsActive = true LIMIT 1];

        AccountToOpportunity__c investor = [SELECT Id FROM AccountToOpportunity__c LIMIT 1];
        Contact c = [SELECT Id FROM Contact LIMIT 1];
        List<String> ids = new List<String>();
        ids.add(c.Id);
        User sender = [SELECT Id, Email, Name, CompanyName, FirstName, LastName, Title, Country, Department, Phone, MobilePhone FROM User WHERE Id = :UserInfo.getUserId()];
        Id orgWideEmailAddressId;
        List<OrgWideEmailAddress> owea =
        [
                SELECT Id
                FROM OrgWideEmailAddress
                LIMIT 1
        ];
        if (!owea.isEmpty())
        {
            orgWideEmailAddressId = owea.get(0).Id;
        }
//        Map<Id, CampaignMember> campaignMemberByInvestorContact = NewMailingService.prepareCampaignMemberMap(activeCampaign.Id);
        EmailTemplate emailTemplate;
        List<EmailTemplate> emailTemplates = [SELECT Id, Subject, HtmlValue FROM EmailTemplate LIMIT 1];
        if (!emailTemplates.isEmpty())
        {
            emailTemplate = emailTemplates.get(0);
        }
        Test.startTest();

        NewMailingService.populateEmails(orgWideEmailAddressId,
                sender, investor.Id, emailTemplate, 'Salutation', ids, ids, new List<String>(), new List<Id>());

        Test.stopTest();
    }

    @IsTest
    static void testSetError()
    {
        List<InvestorMailingConfiguration__c> mailingConfigurations =
        [
                SELECT Id, Investor__c, GermanSalutationSequence__c, EnglishSalutationSequence__c,
                        SendingErrorMessage__c, SendingStatus__c
                FROM InvestorMailingConfiguration__c
        ];
        Test.startTest();

        NewMailingService.setError(mailingConfigurations, 'Error');

        Test.stopTest();

        for (InvestorMailingConfiguration__c configuration : mailingConfigurations)
        {
            System.assertEquals('Error', configuration.SendingErrorMessage__c, 'We expect SendingErrorMessage__c to be filled');
            System.assertEquals(NewMailingService.CAMPAIGN_SENDING_FINISHED, configuration.SendingStatus__c, 'We expect SendingStatus__c to be set');
        }
    }

    @IsTest
    static void testSetSuccess()
    {
        List<InvestorMailingConfiguration__c> mailingConfigurations =
        [
                SELECT Id, Investor__c, GermanSalutationSequence__c, EnglishSalutationSequence__c,
                        SendingErrorMessage__c, SendingStatus__c
                FROM InvestorMailingConfiguration__c
        ];
        Test.startTest();

        NewMailingService.setSuccess(mailingConfigurations);

        Test.stopTest();

        for (InvestorMailingConfiguration__c configuration : mailingConfigurations)
        {
            System.assertEquals('', configuration.SendingErrorMessage__c, 'We expect SendingErrorMessage__c to be empty');
            System.assertEquals(NewMailingService.CAMPAIGN_SENDING_FINISHED, configuration.SendingStatus__c, 'We expect SendingStatus__c to be set');
        }
    }
}