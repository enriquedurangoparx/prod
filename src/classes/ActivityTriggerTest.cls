/**
* Testclass for ActivityTrigger.cls
*
* @author: <Egor Markuzov> (egor.markuzov@parx.com)
*
* @history:
* version		    | author				                                | changes
* ====================================================================================
* 0.1 13.11.2019	| Egor Markuzov (egor.markuzov@parx.com)    | initial version.
*/

@isTest
private class ActivityTriggerTest
{
    @testSetup
    static void setup()
    {
        TestDataFactory.initializeProducts();

        Embargo__c embargo = new Embargo__c();
        embargo.Name = 'Embargo';
        embargo.Countries__c = 'Egypt;Afghanistan;Belarus;Burundi;Iraq;Yemen';
        insert embargo;
    }

    /**
    * test AcquisitionProfileTrigger hookAfterInsert for task trigger
    */
    @isTest
    public static void testPopulateLastActivityDateTask() 
    {
        Account newAccount = TestDataFactory.createAccount('Test Account 1');
        insert newAccount;

        PropertyObject__c propertyObject = TestDataFactory.buildPropertyObject('property', true);
        Opportunity newOpportunity = TestDataFactory.createOpportunity(newAccount.Id, propertyObject);
        insert newOpportunity;

        AccountToOpportunity__c newAccToOpp = TestDataFactory.createAccountToOpportunity(newAccount.Id, newOpportunity.Id);
        insert newAccToOpp;

        Task newTask = TestDataFactory.createTask(newAccToOpp.Id);
        newTask.RecordTypeId = Schema.SObjectType.Task.getRecordTypeInfosByDeveloperName().get('Offer').getRecordTypeId();
        newTask.InvestorStatus__c = 'Offer';
        insert newTask;

        System.assertEquals(newTask.ActivityDate, [SELECT LastBidDate__c
                                              FROM AccountToOpportunity__c 
                                              WHERE Id = :newAccToOpp.Id].LastBidDate__c,
                                              'Wrong date on AccountToOpportunity__c after Activity insertion');
    }

    /**
    * test AcquisitionProfileTrigger hookAfterInsert for event trigger
    */
    @isTest
    public static void testPopulateLastBidDateAfterDelete() 
    {
        Account newAccount = TestDataFactory.createAccount('Test Account 1');
        insert newAccount;

        PropertyObject__c propertyObject = TestDataFactory.buildPropertyObject('property', true);
        Opportunity newOpportunity = TestDataFactory.createOpportunity(newAccount.Id, propertyObject);
        insert newOpportunity;

        AccountToOpportunity__c newAccToOpp = TestDataFactory.createAccountToOpportunity(newAccount.Id, newOpportunity.Id);
        insert newAccToOpp;
        Date td = Date.today();

        Task newTask = TestDataFactory.createTask(newAccToOpp.Id);
        newTask.InvestorStatus__c = 'Offer';
        newTask.Amount__c = 3;
        newTask.RecordTypeId = Schema.SObjectType.Task.getRecordTypeInfosByDeveloperName().get('Offer').getRecordTypeId();


        Task newTask2 = TestDataFactory.createTask(newAccToOpp.Id);
        newTask2.InvestorStatus__c = 'Offer';
        newTask2.Amount__c = 4;
        newTask2.ActivityDate = td.addDays(1);
        newTask2.RecordTypeId = Schema.SObjectType.Task.getRecordTypeInfosByDeveloperName().get('Offer').getRecordTypeId();

        Task newTask3 = TestDataFactory.createTask(newAccToOpp.Id);
        newTask3.InvestorStatus__c = 'Offer';
        newTask3.Amount__c = 5;
        newTask3.ActivityDate = td.addDays(2);
        newTask3.RecordTypeId = Schema.SObjectType.Task.getRecordTypeInfosByDeveloperName().get('Offer').getRecordTypeId();

        List<Task> taskList = new List<Task>{newTask, newTask2, newTask3};
        insert taskList;

        delete newTask3;

        List<AccountToOpportunity__c> atoList = [SELECT LastBidDate__c, 
                                                        LastBidAmount__c 
                                                 FROM AccountToOpportunity__c 
                                                 WHERE Id = :newAccToOpp.Id];

        System.assertEquals(td.addDays(1), atoList[0].LastBidDate__c, 'Wrong date');
        System.assertEquals(4, atoList[0].LastBidAmount__c, 'Wrong amount');
    }
}