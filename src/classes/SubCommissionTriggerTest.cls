/**
* Testclass. Test aggregation for SubCommission.
*
* @author: <Christian Schwabe> (Christian.Schwabe@colliers.com)
*
* @history:
* version		    | author				                                | changes
* ====================================================================================
* 0.1 02.07.2019	| Christian Schwabe (Christian.Schwabe@colliers.com)	| initial version.
* 0.2 31.07.2019	| Christian Schwabe (Christian.Schwabe@colliers.com)	| Added embargo-object and new required field StartState__c.
*/
@isTest
private class SubCommissionTriggerTest {

	private static final String ACCOUNT_NAME = 'Test GmbH';
	private static final String OPPORTUNITY_NAME = 'Test-Opportunity';
	private static final String VALUATION_USERNAME = 'Max.Mustermann@muster.de.valuation';
	//private static final String FINANCE_USERNAME = 'Max.Mustermann@muster.de.finance';
	//private static final String FINANCE_USERROLE_DEVELOPERNAME = 'ColliersInternationalDeutschlandGmbH';

    //private static final String INVOICE_NAME = '4711';

    @testSetup static void setup() {
		Profile valuationProfile = [SELECT Id FROM Profile WHERE Name = 'Colliers Valuation'];
    	//Profile financeProfile = [SELECT Id FROM Profile WHERE Name = 'Colliers Finance'];

    	User valuationUser = buildUser(valuationProfile, VALUATION_USERNAME);
    	insert valuationUser;

		initializeProducts();
		


    	/*User financeUser = buildUser(financeProfile, FINANCE_USERNAME);
    	financeUser.UserRoleId = [SELECT Id FROM UserRole WHERE DeveloperName = :FINANCE_USERROLE_DEVELOPERNAME].Id;
    	insert financeUser;

    	PermissionSet psJustOnAdditionalFields = [SELECT Id FROM PermissionSet WHERE Name = 'JustOn_Additional_Fields'];//JustOn Additional Fields
		insert new PermissionSetAssignment(AssigneeId = financeUser.Id, PermissionSetId = psJustOnAdditionalFields.Id);

    	PermissionSet psJustOnReadWrite = [SELECT Id FROM PermissionSet WHERE Name = 'DefaultPermissions'];//JustOn Read/Write
		insert new PermissionSetAssignment(AssigneeId = financeUser.Id, PermissionSetId = psJustOnReadWrite.Id);

		PackageLicense plJustOn = [SELECT Id FROM PackageLicense WHERE NamespacePrefix = 'ONB2'];
		insert new UserPackageLicense(UserId = financeUser.Id, PackageLicenseId = plJustOn.Id);

    	preventTriggerExecutionOnInvoice();*/
    }

    /**
     * Deactivate trigger execution on ONB2__Invoice__c for test.
     * Added @future-Annotation based on the following help-article: https://help.salesforce.com/articleView?id=000325253&language=en_US&type=1
     */
    /*@future public static void preventTriggerExecutionOnInvoice(){
        insert new List<ONB2__TriggerSettings__c>{
		    new ONB2__TriggerSettings__c(
		        Name = 'InvoiceBeforeInsert'
		    ),
		    new ONB2__TriggerSettings__c(
		        Name = 'InvoiceBeforeUpdate'
		    ),
		    new ONB2__TriggerSettings__c(
		        Name = 'InvoiceLineItemBeforeInsert'
		    ),
		    new ONB2__TriggerSettings__c(
		        Name = 'InvoiceLineItemBeforeUpdate'
		    )
		};
    }*/

	/**
     * Builds up a User for System.runAs(...).
     */
    public static User buildUser(Profile profile, String username){

        User user = new User();
        user.FirstName = 'Max';
        user.LastName = 'Mustermann';
        user.Alias = 'mmuster';
        user.Email = user.FirstName + '.' + user.Lastname + '@muster.de.invalid';
        user.Username = username;
        user.ProfileId = profile.Id;
        user.TimeZoneSidKey = 'Europe/Berlin';
        user.LocaleSidKey = 'de_DE_EURO';
        user.EmailEncodingKey = 'ISO-8859-1';
        user.LanguageLocaleKey = 'de';

        return user;
    }

	private static Account prepareAccount(){
		Account account = new Account();
		account.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Account').getRecordTypeId();
		account.Name = ACCOUNT_NAME;

		return account;
	}

    /*private static ONB2__Invoice__c prepareInvoice(Account account, ONB2__Template__c invoiceTemplate){
    	ONB2__Invoice__c invoice = new ONB2__Invoice__c();
    	invoice.Name = INVOICE_NAME;
    	invoice.ONB2__Account__c = account.Id;
    	invoice.ONB2__Template__c = invoiceTemplate.Id;
    	invoice.ONB2__Tenant__c = 'Deutschland';
    	return invoice;
    }*/

    /*private static ONB2__Template__c prepareInvoiceTemplate(){
    	ONB2__Template__c invoiceTemplate = new ONB2__Template__c();
    	invoiceTemplate.Name = 'DE';
    	invoiceTemplate.ONB2__DisplayType__c = 'Rechnung';

    	return invoiceTemplate;
    }*/

    private static Opportunity prepareOpportunity(Account account){
    	Opportunity opportunity = new Opportunity();
    	opportunity.Name = OPPORTUNITY_NAME;
        opportunity.RecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('ValuationAcquisition').getRecordTypeId();
    	opportunity.AccountId = account.Id;
    	opportunity.StageName = SObjectType.Opportunity.fields.StageName.getPicklistValues().get(0).getValue();
		opportunity.FrameworkContractType__c = 'Preferred in Panel';
    	opportunity.FrameworkContractPanel__c = 'Germany';
    	opportunity.Region__c = 'EMEA';
    	opportunity.Country__c = 'Germany';
		opportunity.ProjectStart__c = Date.today();
		opportunity.CloseDate = Date.today().addDays(3);

    	return opportunity;
    }

    private static SubCommissions__c prepareSubCommission(Opportunity opportunity, ONB2__Invoice__c invoice, Decimal amount, String classification){
    	SubCommissions__c subCommission = new SubCommissions__c();
    	subCommission.Opportunity__c = (opportunity != null) ? opportunity.Id : null;
    	subCommission.Invoice__c = (invoice != null) ? invoice.Id : null;
    	subCommission.AmountNet__c = amount;
    	subCommission.Classification__c = classification;

    	return subCommission;
    }

	/**
	 * Test correct aggregation on Invoice (ONB2__Invoice__c) AND Opportunity
	 * for insert / update on field AmountNet__c for SubCommissions__c if subcommission is related to both objects.
	 */
    /*@isTest static void testInvoiceAndOpportunityInsertAndChangeSubCommissionAmount() {
    	User financeUser = [SELECT Id FROM User WHERE Username = :FINANCE_USERNAME];
    	User valuationUser = [SELECT Id FROM User WHERE Username = :VALUATION_USERNAME];

    	Account account = prepareAccount();
    	insert account;

    	Opportunity opportunity = prepareOpportunity(account);
        opportunity.OwnerId = financeUser.Id;
    	insert opportunity;

    	ONB2__Template__c invoiceTemplate = prepareInvoiceTemplate();
		invoiceTemplate.OwnerId = financeUser.Id;
		insert invoiceTemplate;

		ONB2__Invoice__c invoice = prepareInvoice(account, invoiceTemplate);
		invoice.OwnerId = financeUser.Id;
		insert invoice;

    	// Implement test code
    	Test.startTest();
	        System.runAs(financeUser){
    	    	SubCommissions__c subCommission1 = prepareSubCommission(opportunity, invoice, 10.00, 'Third parties');
		    	SubCommissions__c subCommission2 = prepareSubCommission(opportunity, invoice, 10.00, 'Third parties');
		    	SubCommissions__c subCommission3 = prepareSubCommission(opportunity, invoice, 10.00, 'Colliers Germany');
		    	SubCommissions__c subCommission4 = prepareSubCommission(opportunity, invoice, 10.00, 'Colliers Germany');
		    	SubCommissions__c subCommission5 = prepareSubCommission(opportunity, invoice, 10.00, 'Colliers International');
		    	SubCommissions__c subCommission6 = prepareSubCommission(opportunity, invoice, 10.00, 'Colliers International');
		    	insert new List<SubCommissions__c>{ subCommission1, subCommission2, subCommission3, subCommission4, subCommission5, subCommission6 };
		    	
    	    	invoice = [SELECT Id, SubProvisionSumAmountNet__c FROM ONB2__Invoice__c WHERE Name = :INVOICE_NAME];
    			System.assertEquals(60.00, invoice.SubProvisionSumAmountNet__c);

		    	opportunity = [SELECT Id, ReferralColliersInternational__c, ReferralColliersNational__c, X3rdPartyOther__c FROM Opportunity WHERE Name = :OPPORTUNITY_NAME];
		    	System.assertEquals(20.00, opportunity.ReferralColliersInternational__c);
		    	System.assertEquals(20.00, opportunity.ReferralColliersNational__c);
		    	System.assertEquals(20.00, opportunity.X3rdPartyOther__c);

	            subCommission1.AmountNet__c = 5.00;
	            subCommission3.AmountNet__c = 5.00;
	            subCommission5.AmountNet__c = 5.00;

	            update new List<SubCommissions__c>{ subCommission1, subCommission3, subCommission5 };
	        }
        Test.stopTest();

    	invoice = [SELECT Id, SubProvisionSumAmountNet__c FROM ONB2__Invoice__c WHERE Name = :INVOICE_NAME];
		System.assertEquals(45.00, invoice.SubProvisionSumAmountNet__c);

    	opportunity = [SELECT Id, ReferralColliersInternational__c, ReferralColliersNational__c, X3rdPartyOther__c FROM Opportunity WHERE Name = :OPPORTUNITY_NAME];
		System.assertEquals(15.00, opportunity.ReferralColliersInternational__c);
		System.assertEquals(15.00, opportunity.ReferralColliersNational__c);
		System.assertEquals(15.00, opportunity.X3rdPartyOther__c);
	}*/


	/**
	 * Test correct aggregation on ONB2__Invoice__c for insert / update on field AmountNet__c for SubCommissions__c.
	 */
    /*@isTest static void testInvoiceInsertAndChangeSubCommissionAmount() {
    	User financeUser = [SELECT Id FROM User WHERE Username = :FINANCE_USERNAME];

    	Account account = prepareAccount();
		insert account;

    	ONB2__Template__c invoiceTemplate = prepareInvoiceTemplate();
		invoiceTemplate.OwnerId = financeUser.Id;
		insert invoiceTemplate;

		ONB2__Invoice__c invoice = prepareInvoice(account, invoiceTemplate);
		invoice.OwnerId = financeUser.Id;
		insert invoice;

    	SubCommissions__c subCommission1 = prepareSubCommission(null, invoice, 10.00, 'Third parties');
    	SubCommissions__c subCommission2 = prepareSubCommission(null, invoice, 10.00, 'Third parties');
    	SubCommissions__c subCommission3 = prepareSubCommission(null, invoice, 10.00, 'Colliers Germany');
    	SubCommissions__c subCommission4 = prepareSubCommission(null, invoice, 10.00, 'Colliers Germany');
    	SubCommissions__c subCommission5 = prepareSubCommission(null, invoice, 10.00, 'Colliers International');
    	SubCommissions__c subCommission6 = prepareSubCommission(null, invoice, 10.00, 'Colliers International');
    	insert new List<SubCommissions__c>{ subCommission1, subCommission2, subCommission3, subCommission4, subCommission5, subCommission6 };
    
    	invoice = [SELECT Id, SubProvisionSumAmountNet__c FROM ONB2__Invoice__c WHERE Name = :INVOICE_NAME];
    	System.assertEquals(60.00, invoice.SubProvisionSumAmountNet__c);

        // Implement test code
    	Test.startTest();
	        System.runAs(financeUser) {
	            subCommission1.AmountNet__c = 5.00;
	            subCommission3.AmountNet__c = 5.00;
	            subCommission5.AmountNet__c = 5.00;

	            update new List<SubCommissions__c>{ subCommission1, subCommission3, subCommission5 };
	        }
        Test.stopTest();

    	invoice = [SELECT Id, SubProvisionSumAmountNet__c FROM ONB2__Invoice__c WHERE Name = :INVOICE_NAME];
		System.assertEquals(45.00, invoice.SubProvisionSumAmountNet__c);
    }*/
    
	@future
	private static void initializeProducts(){
		Embargo__c embargo = new Embargo__c();
        embargo.Name = 'Embargo';
        embargo.Countries__c = 'Egypt;Afghanistan;Belarus;Burundi;Iraq;Yemen';
        insert embargo;

		OpportunityHandlerTest.initializeProducts();// Needed to setup product, pricebook and pricebookentry.
	}

    /**
	 * Test correct aggregation on Opportunity for insert / update on field AmountNet__c for SubCommissions__c.
	 */
    @isTest static void testOpportunityInsertAndChangeSubCommissionAmount() {
	  	Account account = prepareAccount();
    	insert account;

    	Opportunity opportunity = prepareOpportunity(account);
    	insert opportunity;

    	SubCommissions__c subCommission1 = prepareSubCommission(opportunity, null, 10.00, 'Third parties');
    	SubCommissions__c subCommission2 = prepareSubCommission(opportunity, null, 10.00, 'Third parties');
    	SubCommissions__c subCommission3 = prepareSubCommission(opportunity, null, 10.00, 'Colliers Germany');
    	SubCommissions__c subCommission4 = prepareSubCommission(opportunity, null, 10.00, 'Colliers Germany');
    	SubCommissions__c subCommission5 = prepareSubCommission(opportunity, null, 10.00, 'Colliers International');
    	SubCommissions__c subCommission6 = prepareSubCommission(opportunity, null, 10.00, 'Colliers International');
    	insert new List<SubCommissions__c>{ subCommission1, subCommission2, subCommission3, subCommission4, subCommission5, subCommission6 };

    	opportunity = [SELECT Id, ReferralColliersInternational__c, ReferralColliersNational__c, X3rdPartyOther__c FROM Opportunity WHERE Name = :OPPORTUNITY_NAME];
    	System.assertEquals(20.00, opportunity.ReferralColliersInternational__c);
    	System.assertEquals(20.00, opportunity.ReferralColliersNational__c);
    	System.assertEquals(20.00, opportunity.X3rdPartyOther__c);
    	
    	User valuationUser = [SELECT Id FROM User WHERE Username = :VALUATION_USERNAME];
        // Implement test code
    	Test.startTest();
	        System.runAs(valuationUser) {
	            subCommission1.AmountNet__c = 5.00;
	            subCommission3.AmountNet__c = 5.00;
	            subCommission5.AmountNet__c = 5.00;

	            update new List<SubCommissions__c>{ subCommission1, subCommission3, subCommission5 };
	        }
		Test.stopTest();

    	opportunity = [SELECT Id, ReferralColliersInternational__c, ReferralColliersNational__c, X3rdPartyOther__c FROM Opportunity WHERE Name = :OPPORTUNITY_NAME];
		System.assertEquals(15.00, opportunity.ReferralColliersInternational__c);
		System.assertEquals(15.00, opportunity.ReferralColliersNational__c);
		System.assertEquals(15.00, opportunity.X3rdPartyOther__c);
    }

    /**
	 * Test correct aggregation on Opportunity for deletion on SubCommissions__c
	 * Currently finance-user have no permission on SubCommissions__c to delete records. For this reason the test failed.
	 * Therefore is no test provided to test undeletion.
	 */
    /*@isTest static void testInvoiceDeleteSubCommission() {
    	User financeUser = [SELECT Id FROM User WHERE Username = :FINANCE_USERNAME];
		
		Account account = prepareAccount();
    	insert account;

    	ONB2__Template__c invoiceTemplate = prepareInvoiceTemplate();
		invoiceTemplate.OwnerId = financeUser.Id;
		insert invoiceTemplate;

		ONB2__Invoice__c invoice = prepareInvoice(account, invoiceTemplate);
		invoice.OwnerId = financeUser.Id;
		insert invoice;

    	SubCommissions__c subCommission1 = prepareSubCommission(null, invoice, 10.00, 'Third parties');
    	SubCommissions__c subCommission2 = prepareSubCommission(null, invoice, 10.00, 'Third parties');
    	SubCommissions__c subCommission3 = prepareSubCommission(null, invoice, 10.00, 'Colliers Germany');
    	SubCommissions__c subCommission4 = prepareSubCommission(null, invoice, 10.00, 'Colliers Germany');
    	SubCommissions__c subCommission5 = prepareSubCommission(null, invoice, 10.00, 'Colliers International');
    	SubCommissions__c subCommission6 = prepareSubCommission(null, invoice, 10.00, 'Colliers International');
    	insert new List<SubCommissions__c>{ subCommission1, subCommission2, subCommission3, subCommission4, subCommission5, subCommission6 };
    
 		// Implement test code
    	Test.startTest();
	        System.runAs(financeUser) {
	            delete new List<SubCommissions__c>{ subCommission1, subCommission3, subCommission5 };
	        }
		Test.stopTest();

    	invoice = [SELECT Id, SubProvisionSumAmountNet__c FROM ONB2__Invoice__c WHERE Name = :INVOICE_NAME];
		System.assertEquals(30.00, invoice.SubProvisionSumAmountNet__c);
	}*/

    /**
	 * Test correct aggregation on Opportunity for deletion on SubCommissions__c
	 */
    @isTest static void testOpportunityDeleteSubCommission() {
		Account account = prepareAccount();
    	insert account;

    	Opportunity opportunity = prepareOpportunity(account);
    	insert opportunity;
    	
    	User valuationUser = [SELECT Id FROM User WHERE Username = :VALUATION_USERNAME];

    	SubCommissions__c subCommission1 = prepareSubCommission(opportunity, null, 10.00, 'Third parties');
    	subCommission1.OwnerId = valuationUser.Id;
    	SubCommissions__c subCommission2 = prepareSubCommission(opportunity, null, 10.00, 'Third parties');
    	SubCommissions__c subCommission3 = prepareSubCommission(opportunity, null, 10.00, 'Colliers Germany');
    	subCommission3.OwnerId = valuationUser.Id;
    	SubCommissions__c subCommission4 = prepareSubCommission(opportunity, null, 10.00, 'Colliers Germany');
    	SubCommissions__c subCommission5 = prepareSubCommission(opportunity, null, 10.00, 'Colliers International');
    	subCommission5.OwnerId = valuationUser.Id;
    	SubCommissions__c subCommission6 = prepareSubCommission(opportunity, null, 10.00, 'Colliers International');
    	insert new List<SubCommissions__c>{ subCommission1, subCommission2, subCommission3, subCommission4, subCommission5, subCommission6 };

        // Implement test code
    	Test.startTest();
	        System.runAs(valuationUser) {
	            delete new List<SubCommissions__c>{ subCommission1, subCommission3, subCommission5 };
	        }
		Test.stopTest();

    	opportunity = [SELECT Id, ReferralColliersInternational__c, ReferralColliersNational__c, X3rdPartyOther__c FROM Opportunity WHERE Name = :OPPORTUNITY_NAME];
		System.assertEquals(10.00, opportunity.ReferralColliersInternational__c);
		System.assertEquals(10.00, opportunity.ReferralColliersNational__c);
		System.assertEquals(10.00, opportunity.X3rdPartyOther__c);
    }

     /**
	 * Test correct aggregation on Opportunity for undeletion on SubCommissions__c
	 */
    @isTest static void testOpportunityUndeleteSubCommission() {
    	Account account = prepareAccount();
    	insert account;

    	Opportunity opportunity = prepareOpportunity(account);
    	insert opportunity;
    	
    	User valuationUser = [SELECT Id FROM User WHERE Username = :VALUATION_USERNAME];

    	SubCommissions__c subCommission1 = prepareSubCommission(opportunity, null, 10.00, 'Third parties');
    	subCommission1.OwnerId = valuationUser.Id;
    	SubCommissions__c subCommission2 = prepareSubCommission(opportunity, null, 10.00, 'Third parties');
    	SubCommissions__c subCommission3 = prepareSubCommission(opportunity, null, 10.00, 'Colliers Germany');
    	subCommission3.OwnerId = valuationUser.Id;
    	SubCommissions__c subCommission4 = prepareSubCommission(opportunity, null, 10.00, 'Colliers Germany');
    	SubCommissions__c subCommission5 = prepareSubCommission(opportunity, null, 10.00, 'Colliers International');
    	subCommission5.OwnerId = valuationUser.Id;
    	SubCommissions__c subCommission6 = prepareSubCommission(opportunity, null, 10.00, 'Colliers International');
    	insert new List<SubCommissions__c>{ subCommission1, subCommission2, subCommission3, subCommission4, subCommission5, subCommission6 };

        // Implement test code
    	Test.startTest();
	        System.runAs(valuationUser) {
	            delete new List<SubCommissions__c>{ subCommission1, subCommission3, subCommission5 };

	            undelete new List<SubCommissions__c>{ subCommission1, subCommission3, subCommission5 };
	        }
		Test.stopTest();

    	opportunity = [SELECT Id, ReferralColliersInternational__c, ReferralColliersNational__c, X3rdPartyOther__c FROM Opportunity WHERE Name = :OPPORTUNITY_NAME];
		System.assertEquals(20.00, opportunity.ReferralColliersInternational__c);
		System.assertEquals(20.00, opportunity.ReferralColliersNational__c);
		System.assertEquals(20.00, opportunity.X3rdPartyOther__c);
    }
}