/**
* Handle business logic for SubCommission__c.
*
* @author: <Christian Schwabe> (Christian.Schwabe@colliers.com)
*
* @history:
* version		    | author				                                | changes
* ====================================================================================
* 0.1 01.07.2019	| Christian Schwabe (Christian.Schwabe@colliers.com)	| initial version.
* 0.2 02.07.2019	| Christian Schwabe (Christian.Schwabe@colliers.com)	| added the possibility to change AmountNet__c.
*/
public without sharing class SubCommissionHandler
{
    /**
     * Handle aggregation for SubCommissions__c.
     * 
     * @param mode					'addition' or 'subtraction'.
     * @param listOfSubCommission	contains List of added or removed.
     */
    public static void aggregate(String mode, List<SubCommissions__c> listOfSubCommission){
    	Map<String, Set<Id>> mapOfIdsByObjectName = SubCommissionHelper.extractParentRecordId(listOfSubCommission);
    	//Map<Id, ONB2__Invoice__c> mapOfInvoiceById = new Map<Id, ONB2__Invoice__c>();
    	Map<Id, Opportunity> mapOfOpportunityById = new Map<Id, Opportunity>();

    	/*Set<Id> setOfInvoiceId = mapOfIdsByObjectName.get('ONB2__Invoice__c');
    	if(setOfInvoiceId.size() > 0){// If subcommissions are related to invoices.
    		mapOfInvoiceById = new Map<Id, ONB2__Invoice__c>([SELECT Id, SubProvisionSumAmountNet__c FROM ONB2__Invoice__c WHERE Id = :setOfInvoiceId]);
    	}*/

    	Set<Id> setOfOpportunityId = mapOfIdsByObjectName.get('Opportunity');
		if(setOfOpportunityId.size() > 0){// If subcommissions are related to opportunities.
    		mapOfOpportunityById = new Map<Id, Opportunity>([SELECT Id, X3rdPartyOther__c, ReferralColliersInternational__c, ReferralColliersNational__c FROM Opportunity WHERE Id = :setOfOpportunityId]);
    	}

		Map<Id, Map<String, Decimal>> mapOfParentIdByFieldApiName = new Map<Id, Map<String, Decimal>>();//Outer key: record id -- inner key: apiname of field // value: decimal value
		List<SObject> listOfObjectsToUpdate = new List<SObject>();

		for(SubCommissions__c subCommission : listOfSubCommission){// iterate through added or deleted records.
			/* !!! ATTENTION !!! 
			SKIP Sub Commissions, which are NOT related to an Opportunity.
			*/
			if(String.isEmpty(subCommission.Opportunity__c)){
				continue;
			}

			String relatedTo = SubCommissionHelper.checkRelation(subCommission);
			Decimal amount = subCommission.AmountNet__c;
    		Id opportunityId = subCommission.Opportunity__c;
    		Id invoiceId = subCommission.Invoice__c;

			/**
			 * BEGIN -- initialize values, if there is no decimal value set.
			 */
    		Opportunity opportunity = null;
    		if(relatedTo.equalsIgnoreCase('Opportunity')/* || relatedTo.equalsIgnoreCase('both')*/){
    			opportunity = mapOfOpportunityById.get(opportunityId);
    			opportunity.X3rdPartyOther__c = (opportunity.X3rdPartyOther__c == null) ? 0 : opportunity.X3rdPartyOther__c;
    			opportunity.ReferralColliersInternational__c = (opportunity.ReferralColliersInternational__c == null) ? 0 : opportunity.ReferralColliersInternational__c;
    			opportunity.ReferralColliersNational__c = (opportunity.ReferralColliersNational__c == null) ? 0 : opportunity.ReferralColliersNational__c;

    			/**
    			 * Remember each value for parent opportunity.
    			 * Retrieve the current value and add or delete amount. This procedure is choosen to set the value outside the loop.
    			 */
    			mapOfParentIdByFieldApiName.put(opportunityId, 
    				new Map<String, Decimal>{	'X3rdPartyOther__c' => opportunity.X3rdPartyOther__c,
												'ReferralColliersInternational__c' => opportunity.ReferralColliersInternational__c,
												'ReferralColliersNational__c' => opportunity.ReferralColliersNational__c
											}
				);
			}

			/*ONB2__Invoice__c invoice = null;
    		if(relatedTo.equalsIgnoreCase('ONB2__Invoice__c') || relatedTo.equalsIgnoreCase('both')){
    			invoice = mapOfInvoiceById.get(invoiceId);
    			invoice.SubProvisionSumAmountNet__c = (invoice.SubProvisionSumAmountNet__c == null) ? 0 : invoice.SubProvisionSumAmountNet__c;
				
				//remember each value for parent invoice
    			mapOfParentIdByFieldApiName.put(invoiceId, 
    				new Map<String, Decimal>{ 'SubProvisionSumAmountNet__c' => invoice.SubProvisionSumAmountNet__c }
				);
			}*/
			/**
			 * END -- initialize values, if there is no decimal value set.
			 */
			
			if(subCommission.Classification__c.equalsIgnoreCase('Third parties')){// Aggregate subcommissions which are classified as third party
				if(mode.equalsIgnoreCase('addition')){
					if(relatedTo.equalsIgnoreCase('Opportunity')/* || relatedTo.equalsIgnoreCase('both')*/){
						mapOfParentIdByFieldApiName.get(opportunityId).put(
							'X3rdPartyOther__c',
							mapOfParentIdByFieldApiName.get(opportunityId).get('X3rdPartyOther__c') + amount
						);
					}
					/*if(relatedTo.equalsIgnoreCase('ONB2__Invoice__c') || relatedTo.equalsIgnoreCase('both')){
						mapOfParentIdByFieldApiName.get(invoiceId).put(
							'SubProvisionSumAmountNet__c',
							mapOfParentIdByFieldApiName.get(invoiceId).get('SubProvisionSumAmountNet__c') + amount
						);
					}*/

				} else if(mode.equalsIgnoreCase('subtraction')){
					if(relatedTo.equalsIgnoreCase('Opportunity')/* || relatedTo.equalsIgnoreCase('both')*/){
						mapOfParentIdByFieldApiName.get(opportunityId).put(
							'X3rdPartyOther__c',
							mapOfParentIdByFieldApiName.get(opportunityId).get('X3rdPartyOther__c') - amount
						);
					}

					/**
					 * This part of code is (currently) never executed because finance-user have no permission on SubCommissions__c to delete records.
					 */
					/*if(relatedTo.equalsIgnoreCase('ONB2__Invoice__c') || relatedTo.equalsIgnoreCase('both')){
						mapOfParentIdByFieldApiName.get(invoiceId).put(
							'SubProvisionSumAmountNet__c',
							mapOfParentIdByFieldApiName.get(invoiceId).get('SubProvisionSumAmountNet__c') - amount
						);
					}*/
				}
			} else if(subCommission.Classification__c.equalsIgnoreCase('Colliers Germany')){// Aggregate subcommissions which are classified as national
				if(mode.equalsIgnoreCase('addition')){// Hold temporary amount
					if(relatedTo.equalsIgnoreCase('Opportunity')/* || relatedTo.equalsIgnoreCase('both')*/){
						mapOfParentIdByFieldApiName.get(opportunityId).put(
							'ReferralColliersNational__c',
							mapOfParentIdByFieldApiName.get(opportunityId).get('ReferralColliersNational__c') + amount
						);
					}

					/*if(relatedTo.equalsIgnoreCase('ONB2__Invoice__c') || relatedTo.equalsIgnoreCase('both')){
						mapOfParentIdByFieldApiName.get(invoiceId).put(
							'SubProvisionSumAmountNet__c',
							mapOfParentIdByFieldApiName.get(invoiceId).get('SubProvisionSumAmountNet__c') + amount
						);
					}*/
				} else if(mode.equalsIgnoreCase('subtraction')){
					if(relatedTo.equalsIgnoreCase('Opportunity')/* || relatedTo.equalsIgnoreCase('both')*/){
						mapOfParentIdByFieldApiName.get(opportunityId).put(
							'ReferralColliersNational__c',
							mapOfParentIdByFieldApiName.get(opportunityId).get('ReferralColliersNational__c') - amount
						);
					}

					/**
					 * This part of code is (currently) never executed because finance-user have no permission on SubCommissions__c to delete records.
					 */
					/*if(relatedTo.equalsIgnoreCase('ONB2__Invoice__c') || relatedTo.equalsIgnoreCase('both')){
						mapOfParentIdByFieldApiName.get(invoiceId).put(
							'SubProvisionSumAmountNet__c',
							mapOfParentIdByFieldApiName.get(invoiceId).get('SubProvisionSumAmountNet__c') - amount
						);
					}*/
				}
			} else if(subCommission.Classification__c.equalsIgnoreCase('Colliers International')){// Aggregate subcommissions which are classified as international
				if(mode.equalsIgnoreCase('addition')){
					if(relatedTo.equalsIgnoreCase('Opportunity')/* || relatedTo.equalsIgnoreCase('both')*/){
						mapOfParentIdByFieldApiName.get(opportunityId).put(
							'ReferralColliersInternational__c',
							mapOfParentIdByFieldApiName.get(opportunityId).get('ReferralColliersInternational__c') + amount
						);
					}

					/*if(relatedTo.equalsIgnoreCase('ONB2__Invoice__c') || relatedTo.equalsIgnoreCase('both')){
						mapOfParentIdByFieldApiName.get(invoiceId).put(
							'SubProvisionSumAmountNet__c',
							mapOfParentIdByFieldApiName.get(invoiceId).get('SubProvisionSumAmountNet__c') + amount
						);
					}*/
				} else if(mode.equalsIgnoreCase('subtraction')){
					if(relatedTo.equalsIgnoreCase('Opportunity')/* || relatedTo.equalsIgnoreCase('both')*/){
						mapOfParentIdByFieldApiName.get(opportunityId).put(
							'ReferralColliersInternational__c',
							mapOfParentIdByFieldApiName.get(opportunityId).get('ReferralColliersInternational__c') - amount
						);
					}
					
					/**
					 * This part of code is (currently) never executed because finance-user have no permission on SubCommissions__c to delete records.
					 */
					/*if(relatedTo.equalsIgnoreCase('ONB2__Invoice__c') || relatedTo.equalsIgnoreCase('both')){
						mapOfParentIdByFieldApiName.get(invoiceId).put(
							'SubProvisionSumAmountNet__c',
							mapOfParentIdByFieldApiName.get(invoiceId).get('SubProvisionSumAmountNet__c') - amount
						);
					}*/
				}
			}
			
			/**
			 * Assign new / calculated amount to parent object.
			 */
			if(relatedTo.equalsIgnoreCase('Opportunity')/* || relatedTo.equalsIgnoreCase('both')*/){
				opportunity.X3rdPartyOther__c = mapOfParentIdByFieldApiName.get(opportunityId).get('X3rdPartyOther__c');
				opportunity.ReferralColliersInternational__c = mapOfParentIdByFieldApiName.get(opportunityId).get('ReferralColliersInternational__c');
				opportunity.ReferralColliersNational__c = mapOfParentIdByFieldApiName.get(opportunityId).get('ReferralColliersNational__c');

			}
			/*if(relatedTo.equalsIgnoreCase('ONB2__Invoice__c') || relatedTo.equalsIgnoreCase('both')){
				invoice.SubProvisionSumAmountNet__c = mapOfParentIdByFieldApiName.get(invoiceId).get('SubProvisionSumAmountNet__c');
			}*/
		} // End of for-loop

		/*if(mapOfInvoiceById.values().size() > 0){
			listOfObjectsToUpdate.addAll(mapOfInvoiceById.values());
		}*/

		if(mapOfOpportunityById.values().size() > 0){
			listOfObjectsToUpdate.addAll(mapOfOpportunityById.values());
		}

		System.debug('>>>listOfObjectsToUpdate: ' + listOfObjectsToUpdate);

		update listOfObjectsToUpdate;
    }


    /**
     * @TODO:
     * -Berücksichtigen sobald eine Nachschlagebeziehung geändert wurde, dass sich auch die Werte auf alter / neuer Opportunity und / oder Rechnung der Betrag ändert.
     * -Berücksichtigen sobald sich eine Classification ändert.
     * Ist es im Sinne der Rechnungsstellung "okay" diese nicht umzuhängen?
     * 
     * @param listOfNewSubCommission [description]
     * @param listOfOldSubCommission [description]
     */
    public static void aggregate(List<SubCommissions__c> listOfNewSubCommission, List<SubCommissions__c> listOfOldSubCommission){
    	Map<Id, SubCommissions__c> mapOfNewSubCommissionById = new Map<Id, SubCommissions__c>(listOfNewSubCommission);
    	Map<Id, SubCommissions__c> mapOfOldSubCommissionById = new Map<Id, SubCommissions__c>(listOfOldSubCommission);
    	List<SubCommissions__c> listOfChangedSubCommission = new List<SubCommissions__c>();//Needed?

    	Map<String, Set<Id>> mapOfIdsByObjectName = SubCommissionHelper.extractParentRecordId(mapOfNewSubCommissionById.values());
    	//Map<Id, ONB2__Invoice__c> mapOfInvoiceById = new Map<Id, ONB2__Invoice__c>();
    	Map<Id, Opportunity> mapOfOpportunityById = new Map<Id, Opportunity>();

    	/*Set<Id> setOfInvoiceId = mapOfIdsByObjectName.get('ONB2__Invoice__c');
    	if(setOfInvoiceId.size() > 0){// If subcommissions are related to invoices.
    		mapOfInvoiceById = new Map<Id, ONB2__Invoice__c>([SELECT Id, SubProvisionSumAmountNet__c FROM ONB2__Invoice__c WHERE Id = :setOfInvoiceId]);
    	}*/

    	Set<Id> setOfOpportunityId = mapOfIdsByObjectName.get('Opportunity');
		if(setOfOpportunityId.size() > 0){// If subcommissions are related to opportunities.
    		mapOfOpportunityById = new Map<Id, Opportunity>([SELECT Id, X3rdPartyOther__c, ReferralColliersInternational__c, ReferralColliersNational__c FROM Opportunity WHERE Id = :setOfOpportunityId]);
    	}

		Map<Id, Map<String, Decimal>> mapOfParentIdByFieldApiName = new Map<Id, Map<String, Decimal>>();//Outer key: record id -- inner key: apiname of field // value: decimal value
		List<SObject> listOfObjectsToUpdate = new List<SObject>();

    	Set<SObject> setOfObjectsToUpdate = new Set<SObject>();
    	for(Id subCommissionId : mapOfNewSubCommissionById.keySet()){
    		SubCommissions__c newSubCommission = mapOfNewSubCommissionById.get(subCommissionId);
    		SubCommissions__c oldSubCommission = mapOfOldSubCommissionById.get(subCommissionId);
    		Decimal newAmount = newSubCommission.AmountNet__c;
    		Decimal oldAmount = oldSubCommission.AmountNet__c;
    		Boolean amountNetHasChanged = newAmount <> oldAmount;
    		//Boolean opportunityLookupHasChanged = newSubCommission.Opportunity__c <> oldSubCommission.Opportunity__c;
    		//Boolean invoiceLookupHasChanged = newSubCommission.Invoice__c <> oldSubCommission.Invoice__c;
			
			/* !!! ATTENTION !!! 
			SKIP Sub Commissions, which are related to an Invoice__c
			*/
			if(String.isEmpty(newSubCommission.Opportunity__c)){
				continue;
			}

			/** 
			 * Attention to the three lines below if it is possible to change lookup!!
			 */
			String relatedTo = SubCommissionHelper.checkRelation(newSubCommission);
    		Id opportunityId = newSubCommission.Opportunity__c;
    		Id invoiceId = newSubCommission.Invoice__c;

    		if(amountNetHasChanged){
    			Decimal diffAmount = Math.abs(oldAmount - newAmount);
    			diffAmount = (oldAmount < newAmount) ? diffAmount * -1 : diffAmount;//Needed to differentiate if amount is now more or less.

    			/**
    			 * BEGIN -- initialize values, if there is no decimal value set.
    			 */
    			Opportunity opportunity = null;
	    		if(relatedTo.equalsIgnoreCase('Opportunity')/* || relatedTo.equalsIgnoreCase('both')*/){
	    			opportunity = mapOfOpportunityById.get(opportunityId);
	    			opportunity.X3rdPartyOther__c = (opportunity.X3rdPartyOther__c == null) ? 0 : opportunity.X3rdPartyOther__c;
	    			opportunity.ReferralColliersInternational__c = (opportunity.ReferralColliersInternational__c == null) ? 0 : opportunity.ReferralColliersInternational__c;
	    			opportunity.ReferralColliersNational__c = (opportunity.ReferralColliersNational__c == null) ? 0 : opportunity.ReferralColliersNational__c;

	    			/**
	    			 * Remember each value for parent opportunity.
	    			 * Retrieve the current value and add or delete amount. This procedure is choosen to set the value outside the loop.
	    			 */
	    			mapOfParentIdByFieldApiName.put(opportunityId, 
	    				new Map<String, Decimal>{	'X3rdPartyOther__c' => opportunity.X3rdPartyOther__c,
													'ReferralColliersInternational__c' => opportunity.ReferralColliersInternational__c,
													'ReferralColliersNational__c' => opportunity.ReferralColliersNational__c
												}
					);
				}

				/*ONB2__Invoice__c invoice = null;
	    		if(relatedTo.equalsIgnoreCase('ONB2__Invoice__c') || relatedTo.equalsIgnoreCase('both')){
	    			invoice = mapOfInvoiceById.get(invoiceId);
	    			invoice.SubProvisionSumAmountNet__c = (invoice.SubProvisionSumAmountNet__c == null) ? 0 : invoice.SubProvisionSumAmountNet__c;
					
					//remember each value for parent invoice
	    			mapOfParentIdByFieldApiName.put(invoiceId, 
	    				new Map<String, Decimal>{ 'SubProvisionSumAmountNet__c' => invoice.SubProvisionSumAmountNet__c }
					);
				}*/
				/**
    			 * END -- initialize values, if there is no decimal value set.
    			 */


				if(newSubCommission.Classification__c.equalsIgnoreCase('Third parties')){// Aggregate subcommissions which are classified as third party
    			 	if(relatedTo.equalsIgnoreCase('Opportunity')/* || relatedTo.equalsIgnoreCase('both')*/){
						mapOfParentIdByFieldApiName.get(opportunityId).put(
							'X3rdPartyOther__c',
							mapOfParentIdByFieldApiName.get(opportunityId).get('X3rdPartyOther__c') - diffAmount
						);
					}
					/*if(relatedTo.equalsIgnoreCase('ONB2__Invoice__c') || relatedTo.equalsIgnoreCase('both')){
						mapOfParentIdByFieldApiName.get(invoiceId).put(
							'SubProvisionSumAmountNet__c',
							mapOfParentIdByFieldApiName.get(invoiceId).get('SubProvisionSumAmountNet__c') - diffAmount
						);
					}*/
				} else if(newSubCommission.Classification__c.equalsIgnoreCase('Colliers Germany')){// Aggregate subcommissions which are classified as national
					if(relatedTo.equalsIgnoreCase('Opportunity')/* || relatedTo.equalsIgnoreCase('both')*/){
						mapOfParentIdByFieldApiName.get(opportunityId).put(
							'ReferralColliersNational__c',
							mapOfParentIdByFieldApiName.get(opportunityId).get('ReferralColliersNational__c') - diffAmount
						);
					}

					/*if(relatedTo.equalsIgnoreCase('ONB2__Invoice__c') || relatedTo.equalsIgnoreCase('both')){
						mapOfParentIdByFieldApiName.get(invoiceId).put(
							'SubProvisionSumAmountNet__c',
							mapOfParentIdByFieldApiName.get(invoiceId).get('SubProvisionSumAmountNet__c') - diffAmount
						);
					}*/
				} else if(newSubCommission.Classification__c.equalsIgnoreCase('Colliers International')){// Aggregate subcommissions which are classified as international
					if(relatedTo.equalsIgnoreCase('Opportunity')/* || relatedTo.equalsIgnoreCase('both')*/){
						mapOfParentIdByFieldApiName.get(opportunityId).put(
							'ReferralColliersInternational__c',
							mapOfParentIdByFieldApiName.get(opportunityId).get('ReferralColliersInternational__c') - diffAmount
						);
					}

					/*if(relatedTo.equalsIgnoreCase('ONB2__Invoice__c') || relatedTo.equalsIgnoreCase('both')){
						mapOfParentIdByFieldApiName.get(invoiceId).put(
							'SubProvisionSumAmountNet__c',
							mapOfParentIdByFieldApiName.get(invoiceId).get('SubProvisionSumAmountNet__c') - diffAmount
						);
					}*/
				}

	    		/**
				 * Assign new / calculated amount to parent object.
				 */
				if(relatedTo.equalsIgnoreCase('Opportunity')/* || relatedTo.equalsIgnoreCase('both')*/){
					opportunity.X3rdPartyOther__c = mapOfParentIdByFieldApiName.get(opportunityId).get('X3rdPartyOther__c');
					opportunity.ReferralColliersInternational__c = mapOfParentIdByFieldApiName.get(opportunityId).get('ReferralColliersInternational__c');
					opportunity.ReferralColliersNational__c = mapOfParentIdByFieldApiName.get(opportunityId).get('ReferralColliersNational__c');

				} 
				/*if(relatedTo.equalsIgnoreCase('ONB2__Invoice__c') || relatedTo.equalsIgnoreCase('both')){
					invoice.SubProvisionSumAmountNet__c = mapOfParentIdByFieldApiName.get(invoiceId).get('SubProvisionSumAmountNet__c');
				}*/
    		}// End of if (amountHasChanged).
		} // End of for-loop.

		/*if(mapOfInvoiceById.values().size() > 0){
			listOfObjectsToUpdate.addAll(mapOfInvoiceById.values());
		}*/

		if(mapOfOpportunityById.values().size() > 0){
			listOfObjectsToUpdate.addAll(mapOfOpportunityById.values());
		}

		System.debug('>>>listOfObjectsToUpdate: ' + listOfObjectsToUpdate);

		update listOfObjectsToUpdate;
    }

}