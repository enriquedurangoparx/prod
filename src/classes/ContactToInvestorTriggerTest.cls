/**
* Testclass for ContactToInvestorTrigger.cls
*
* @author: <Egor Markuzov> (egor.markuzov@parx.com)
*
* @history:
* version		    | author				                                | changes
* ====================================================================================
* 0.1 13.01.2020	| Egor Markuzov (egor.markuzov@parx.com)                | initial version.
* 0.2 17.03.2020	| Christian Schwabe (Christian.Schwabe@colliers.com)    | [SFINV/US 2407] Added Saluation for Contact.
*/
@isTest
private class ContactToInvestorTriggerTest
{
    @testSetup
    static void setup()
    {
        TestDataFactory.initializeProducts();

        Embargo__c embargo = new Embargo__c();
        embargo.Name = 'Embargo';
        embargo.Countries__c = 'Egypt;Afghanistan;Belarus;Burundi;Iraq;Yemen';
        insert embargo;
    }

    /**
    * test ContactToInvestorTrigger hookAfterInsert for onInvestorContact trigger
    */
    @isTest
    public static void testPopulateListOfContactsAfterInsert() 
    {
        Account newAccount = TestDataFactory.createAccount('Test Account 1');
        insert newAccount;

        List<Contact> newContactList = TestDataFactory.createContacts(newAccount.Id, 5);
        insert newContactList;

        PropertyObject__c propertyObject = TestDataFactory.buildPropertyObject('property', true);
        Opportunity newOpportunity = TestDataFactory.createOpportunity(newAccount.Id, propertyObject);
        insert newOpportunity;

        AccountToOpportunity__c newAccToOpp = TestDataFactory.createAccountToOpportunity(newAccount.Id, newOpportunity.Id);
        insert newAccToOpp;

        AccountToOpportunity__c accToOppToCheck = [SELECT Id, ListOfContacts__c FROM AccountToOpportunity__c WHERE Id = :newAccToOpp.Id LIMIT 1];
        System.assertEquals(true, String.isBlank(accToOppToCheck.ListOfContacts__c), 'List of contacts should be blank');

        List<ContactToInvestor__c> newContactToInvestorList = TestDataFactory.createContactToInvestor(newAccToOpp.Id, newContactList);
        insert newContactToInvestorList;

        accToOppToCheck = [SELECT Id, ListOfContacts__c FROM AccountToOpportunity__c WHERE Id = :newAccToOpp.Id LIMIT 1];
        System.assertEquals(true, String.isNotBlank(accToOppToCheck.ListOfContacts__c), 'List of contacts should not be blank');

        delete newContactToInvestorList;
        accToOppToCheck = [SELECT Id, ListOfContacts__c FROM AccountToOpportunity__c WHERE Id = :newAccToOpp.Id LIMIT 1];
        System.assertEquals(true, String.isBlank(accToOppToCheck.ListOfContacts__c), 'List of contacts should be blank');
    }

    /**
    * test preventDeletionIfHasRelatedActivity hookBeforeDelete for ContactToInvestorTrigger
    */
    @isTest
    public static void testPreventDeletionIfHasRelatedActivity()
    {
        Account newAccount = TestDataFactory.createAccount('Test Account 1');
        insert newAccount;

        List<Contact> newContactList = TestDataFactory.createContacts(newAccount.Id, 5);
        insert newContactList;

        PropertyObject__c propertyObject = TestDataFactory.buildPropertyObject('property', true);
        Opportunity newOpportunity = TestDataFactory.createOpportunity(newAccount.Id, propertyObject);
        insert newOpportunity;

        AccountToOpportunity__c newAccToOpp = TestDataFactory.createAccountToOpportunity(newAccount.Id, newOpportunity.Id);
        insert newAccToOpp;

        List<ContactToInvestor__c> investorContactList = TestDataFactory.createContactToInvestor(newAccToOpp.Id, newContactList);
        insert investorContactList;

        Task newTask = TestDataFactory.createTask(newAccToOpp.Id);
        newTask.RecordTypeId = Schema.SObjectType.Task.getRecordTypeInfosByDeveloperName().get('Offer').getRecordTypeId();
        newTask.InvestorStatus__c = 'Offer';
        insert newTask;

        System.assertEquals(newTask.Id, [SELECT Id FROM Task WHERE Id = :newTask.Id].Id);

        try 
        {
            delete investorContactList[0];
        }
        catch (Exception e)
        {
            System.assertEquals(true, e.getMessage().contains(System.Label.GeneralLabelErrorBecauseHasActivityOrStatus));
        }
    }
}