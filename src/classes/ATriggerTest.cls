/**
 * @author ile
 * @copyright PARX
 */
@istest
private class ATriggerTest
{
    /********************UNIT TESTS REGION********************/
    private class ConcreteTrigger extends ATrigger {/* exists only for testing sake */}

    @istest
    private static void coverAll()
    {
        ConcreteTrigger triggerInstance = new ConcreteTrigger();
        Schema.User record = new Schema.User();

        List<Schema.User> records = new List<Schema.User>{record};

        triggerInstance.execute();
        ATrigger.ALLOW_EXECUTION = true;
        triggerInstance.execute();

        triggerInstance.hookInitialize();
        triggerInstance.onBefore();
        triggerInstance.onAfter();
        triggerInstance.onBeforeInsert(records);
        triggerInstance.onAfterInsert(records);
        triggerInstance.onBeforeUpdate(records, records);
        triggerInstance.onAfterUpdate(records, records);
        triggerInstance.onBeforeDelete(records);
        triggerInstance.onAfterDelete(records);
        triggerInstance.onAfterUndelete(records);
        triggerInstance.hookFinalize();

        triggerInstance.disableForUserWithPermission(ATrigger.CAN_SKIP_TRIGGERS_PERMISSION_NAME);

        try
        {
            triggerInstance.hookException(new Exceptions.GenericException('A'));
            System.assert(false, 'Expecting exception above!');
        }
        catch (Exception e)
        {
            System.debug('Expected exception caught: ' + e);
            System.assert(true);
        }
    }
}