/**
 * Created by ille on 2019-05-28.
 */
public with sharing class ColliersContractTrigger extends ATrigger
{
    public override void hookAfterInsert(List<SObject> records)
    {
        maintainAmountOfFrameContracts((List<ColliersContract__c>) records, null);
    }

    public override void hookAfterDelete(List<SObject> records)
    {
        maintainAmountOfFrameContracts(null, (List<ColliersContract__c>) records);
    }

    public override void hookAfterUpdate(List<SObject> records, List<SObject> oldRecords)
    {
        maintainAmountOfFrameContracts((List<ColliersContract__c>) records, (List<ColliersContract__c>) oldRecords);
    }

    /**
     * Maintains Account.AmountOfFrameContracts__c for related Account records.
     *
     * @param colliersContracts     - inserted or updated ColliersContract__c records, NULL if it is called for DELETE operation.
     * @param oldColliersContracts  - updated, or deleted ColliersContract__c records, NULL if it is called for INSERT operation
     */
    private void maintainAmountOfFrameContracts(List<ColliersContract__c> colliersContracts, List<ColliersContract__c> oldColliersContracts)
    {
        List<ColliersContract__c> increasingContracts = new List<ColliersContract__c>();
        List<ColliersContract__c> decreasingContracts = new List<ColliersContract__c>();
        if (colliersContracts != null && oldColliersContracts != null)//update is happening
        {
            for (Integer i = 0, n = colliersContracts.size(); i < n; i++)
            {
                ColliersContract__c colliersContract = colliersContracts.get(i);
                ColliersContract__c oldColliersContract = oldColliersContracts.get(i);

                if (colliersContract.Account__c != oldColliersContract.Account__c || colliersContract.Status__c != oldColliersContract.Status__c)
                {
                    decreasingContracts.add(oldColliersContract);
                    increasingContracts.add(colliersContract);
                }
            }
        }
        else if (colliersContracts != null)//insert is happening
        {
            increasingContracts.addAll(colliersContracts);
        }
        else if (oldColliersContracts != null)//delete is happening
        {
            decreasingContracts.addAll(oldColliersContracts);
        }

        List<ColliersContract__c> allColliersContracts = new List<ColliersContract__c>();
        allColliersContracts.addAll(increasingContracts);
        allColliersContracts.addAll(decreasingContracts);

        List<Id> accountIDs = new List<Id>();
        for (ColliersContract__c colliersContract : allColliersContracts)
        {
            accountIDs.add(colliersContract.Account__c);
        }

        maintainAmountOfFrameContracts(accountIDs);
    }

    /**
     * Populates Account.AmountOfFrameContracts__c with the proper data.
     *
     * @param accountIDs
     */
    private void maintainAmountOfFrameContracts(List<Id> accountIDs)
    {
        List<Account> affectedAccounts = new List<Account>();
        for (Account relatedAccount : [select AmountOfFrameContracts__c, (select Id from Contracts__r where Status__c = 'Activated') from Account where Id in :accountIDs])
        {
            relatedAccount.AmountOfFrameContracts__c = 0;
            for (ColliersContract__c colliersContract : relatedAccount.Contracts__r)
            {
                relatedAccount.AmountOfFrameContracts__c++;
            }

            affectedAccounts.add(relatedAccount);
        }

        update affectedAccounts;
    }
}