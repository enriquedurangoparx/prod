/**
 * Created by ille on 2019-04-02.
 */

public with sharing class ExpenseTrigger extends ATrigger
{
    public override void hookBeforeDelete(List<SObject> records)
    {
        validateExpenseDeletion((List<Expense__c>) records);
        decreaseScheduleRevenue((List<Expense__c>) records);
    }

    public override void hookBeforeInsert(List<SObject> records)
    {
        setProduct((List<Expense__c>) records);
    }

    public override void hookAfterInsert(List<SObject> records)
    {
        updateLineItemSchedules((List<Expense__c>) records);
    }

    public override void hookBeforeUpdate(List<SObject> records, List<SObject> oldRecords)
    {
        decreaseScheduleRevenue((List<Expense__c>) records, (List<Expense__c>) oldRecords);
        setProduct((List<Expense__c>) records);
    }

    public override void hookAfterUpdate(List<SObject> records, List<SObject> oldRecords)
    {
        increaseScheduleRevenueAndMaintainAssociation((List<Expense__c>) records, (List<Expense__c>) oldRecords);
    }

    /**
     * Records can only be deleted, as long "ExpenseAccount__r.Status__c" is "Open"
     * (except for the administrator, the admin should still be able to delete the lines)
     */
    private void validateExpenseDeletion(List<Expense__c> expenses)
    {
        //Either there are no Expense__c records where ExpenseAccount__r.Status__c != 'Open'
        Exceptions.assert([select count() from Expense__c where ExpenseAccount__r.Status__c != 'Open' and Id in :expenses] == 0
                //or Current User is System Administrator
                || [select count() from Profile where Id = :UserInfo.getProfileId() and Name = 'System Administrator'] != 0,
            new Exceptions.GenericException(Label.SubmittedExpenseChangeError));
    }

    private void updateLineItemSchedules(List<Expense__c> expenses)
    {
        List<Expense__c> extendedExpenses = selectExtendedExpenses(expenses);
        Map<Id, OpportunityLineItemSchedule> lineItemScheduleByOpportunityProductId = new Map<Id, OpportunityLineItemSchedule>();

        for (OpportunityLineItemSchedule lineItemSchedule : selectLineItemSchedules(extendedExpenses))
        {
            if (!lineItemScheduleByOpportunityProductId.containsKey(lineItemSchedule.OpportunityLineItemId))
            {
                lineItemScheduleByOpportunityProductId.put(lineItemSchedule.OpportunityLineItemId, lineItemSchedule);
            }
        }

        modifyLineItemSchedules(lineItemScheduleByOpportunityProductId, extendedExpenses);

        Map<Id, Id> expenseToLineItemSchedule = new Map<Id, Id>();
        for (Expense__c extendedExpense : extendedExpenses)
        {
            OpportunityLineItemSchedule lineItemSchedule = lineItemScheduleByOpportunityProductId.get(extendedExpense.Product__r.OpportunityProductId__c);
            if (lineItemSchedule != null)
            {
                expenseToLineItemSchedule.put(extendedExpense.Id, lineItemSchedule.Id);
            }
        }

        update lineItemScheduleByOpportunityProductId.values();
        futureAssociateLineItemSchedulesWithExpenses(expenseToLineItemSchedule);
    }

    private List<OpportunityLineItemSchedule> selectLineItemSchedules(List<Expense__c> extendedExpenses)
    {
        List<Id> opportunityProductIDs = new List<Id>();
        for (Expense__c extendedExpense : extendedExpenses)
        {
            opportunityProductIDs.add(extendedExpense.Product__r.OpportunityProductId__c);
        }

        return [select 
                	OpportunityLineItemId, 
                	IsAutoRecalculated__c, 
                	Revenue 
               	from 
                	OpportunityLineItemSchedule 
                where 
                	ON_Invoice__c = null 
                and 
                	OpportunityLineItemId in :opportunityProductIDs 
                and 
                	OpportunityLineItem.Opportunity.BillingType__c INCLUDES('Time and Material') 
                order by 
                	ScheduleDate asc];
    }

    private List<Expense__c> selectExtendedExpenses(List<Expense__c> expenses)
    {
        return [select Name, Product__r.OpportunityProductId__c, DocumentAmountNet__c, OpportunityLineItemScheduleId__c from Expense__c where Id in :expenses];
    }

    private void modifyLineItemSchedules(Map<Id, OpportunityLineItemSchedule> lineItemScheduleByOpportunityProductId, List<Expense__c> extendedExpenses)
    {
        for (Expense__c extendedExpense : extendedExpenses)
        {
            OpportunityLineItemSchedule lineItemSchedule = lineItemScheduleByOpportunityProductId.get(extendedExpense.Product__r.OpportunityProductId__c);
            if (lineItemSchedule != null)
            {
//                lineItemSchedule.Quantity = LINE_ITEM_SCHEDULES_UPDATE_QUANTITY;//todo: we're providing revenue thus we're using "Revenue" Type and Quantity should be omitted.
                lineItemSchedule.Revenue = (lineItemSchedule.IsAutoRecalculated__c ? lineItemSchedule.Revenue : 0)
                    + extendedExpense.DocumentAmountNet__c;

                lineItemSchedule.IsAutoRecalculated__c = true;
                OpportunityLineItemScheduleTrigger.allowQuantityRevenueChange(lineItemSchedule.Id);
            }
        }
    }

    /**
   * Decreases corresponding OpportunityLineItemSchedule.Revenue - should be called before update/delete
   */
    private void decreaseScheduleRevenue(List<Expense__c> expenses, List<Expense__c> oldExpenses)
    {
        List<Expense__c> modifiedExpenses = new List<Expense__c>();
        for (Integer i = 0, n = expenses.size(); i < n; i++)
        {
            Expense__c expense = expenses.get(i);
            Expense__c oldExpense = oldExpenses.get(i);

            if (oldExpense.Product__c != null && expense.Product__c != oldExpense.Product__c
                    || expense.DocumentAmountNet__c != oldExpense.DocumentAmountNet__c)
            {
                modifiedExpenses.add(expense);
            }
        }

        decreaseScheduleRevenue(modifiedExpenses);
    }

    private void decreaseScheduleRevenue(List<Expense__c> expenses)
    {
        List<Id> lineItemScheduleIDs = new List<Id>();
        for (Expense__c expense : expenses)
        {
            lineItemScheduleIDs.add(expense.OpportunityLineItemScheduleId__c);
        }

        List<Expense__c> extendedExpenses = selectExtendedExpenses(expenses);
        Map<Id, OpportunityLineItemSchedule> lineItemScheduleMap = new Map<Id, OpportunityLineItemSchedule>([select Revenue from OpportunityLineItemSchedule where Id in :lineItemScheduleIDs and ON_Invoice__c = null]);
        for (Expense__c extendedExpense : extendedExpenses)
        {
            OpportunityLineItemSchedule lineItemSchedule = lineItemScheduleMap.get(extendedExpense.OpportunityLineItemScheduleId__c);
            if (lineItemSchedule != null)
            {
                lineItemSchedule.Revenue = (lineItemSchedule.Revenue != null ? lineItemSchedule.Revenue : 0)
                    - extendedExpense.DocumentAmountNet__c;

                if (lineItemSchedule.Revenue == 0)
                {
                    lineItemSchedule.IsAutoRecalculated__c = false;
                }

                OpportunityLineItemScheduleTrigger.allowQuantityRevenueChange(lineItemSchedule.Id);
            }
        }

        update lineItemScheduleMap.values();
    }

    private void increaseScheduleRevenueAndMaintainAssociation(List<Expense__c> expenses, List<Expense__c> oldExpenses)
    {
        List<Expense__c> modifiedExpenses = new List<Expense__c>();
        Set<Id> expenseIDsWithProductChanged = new Set<Id>();
        Map<Id, Id> expenseToLineItemSchedule = new Map<Id, Id>();
        //Find out which TimeCard__c records had proper modifications
        for (Integer i = 0, n = expenses.size(); i < n; i++)
        {
            Expense__c expense = expenses.get(i);
            Expense__c oldExpense = oldExpenses.get(i);

            if (expense.Product__c != null && oldExpense.Product__c != null && expense.Product__c != oldExpense.Product__c)
            {
                modifiedExpenses.add(expense);
                expenseIDsWithProductChanged.add(expense.Id);
                expenseToLineItemSchedule.put(expense.Id, null);
            }
            else if (expense.DocumentAmountNet__c != oldExpense.DocumentAmountNet__c || expense.Product__c != oldExpense.Product__c)
            {
                modifiedExpenses.add(expense);
            }
        }

        //Retrieve extended copies of TimeCard__c records and build OpportunityLineItemSchedule map
        List<Expense__c> extendedExpenses = selectExtendedExpenses(modifiedExpenses);
        Map<Id, OpportunityLineItemSchedule> lineItemScheduleByOpportunityProductId = new Map<Id, OpportunityLineItemSchedule>();
        for (OpportunityLineItemSchedule lineItemSchedule : selectLineItemSchedules(extendedExpenses))
        {
            if (!lineItemScheduleByOpportunityProductId.containsKey(lineItemSchedule.OpportunityLineItemId))
            {
                lineItemScheduleByOpportunityProductId.put(lineItemSchedule.OpportunityLineItemId, lineItemSchedule);
            }
        }

        //Modify (increase) OpportunityLineItemSchedule Revenue values
        modifyLineItemSchedules(lineItemScheduleByOpportunityProductId, extendedExpenses);
        update lineItemScheduleByOpportunityProductId.values();


        //If necessary remove old TimeCard__c - OpportunityLineItem association and create a new one
        if (expenseIDsWithProductChanged.isEmpty())
        {
            return;
        }

        for (Expense__c extendedExpense : extendedExpenses)
        {
            OpportunityLineItemSchedule lineItemSchedule = lineItemScheduleByOpportunityProductId.get(extendedExpense.Product__r.OpportunityProductId__c);
            if (expenseIDsWithProductChanged.contains(extendedExpense.Id) && lineItemSchedule != null)
            {
                expenseToLineItemSchedule.put(extendedExpense.Id, lineItemSchedule.Id);
            }
        }

        maintainLineItemScheduleExpensesAssociation(expenseToLineItemSchedule);
    }

    @future
    private static void maintainLineItemScheduleExpensesAssociation(Map<Id, Id> timeCardToLineItemSchedule)
    {
        deAssociateLineItemSchedulesWithExpenses(timeCardToLineItemSchedule.values());
        associateLineItemSchedulesWithExpenses(timeCardToLineItemSchedule);
    }

    /**
     * Future wrapper for associateLineItemSchedulesWithTimeCards method.
     */
    @future
    private static void futureAssociateLineItemSchedulesWithExpenses(Map<Id, Id> timeCardToLineItemSchedule)
    {
        associateLineItemSchedulesWithExpenses(timeCardToLineItemSchedule);
    }

    private static void associateLineItemSchedulesWithExpenses(Map<Id, Id> expenseToLineItemSchedule)
    {
        List<Expense__c> associatedExpenses = new List<Expense__c>();
        for (Id expenseId : expenseToLineItemSchedule.keySet())
        {
            associatedExpenses.add(new Expense__c(Id = expenseId, OpportunityLineItemScheduleId__c = expenseToLineItemSchedule.get(expenseId)));
        }

        update associatedExpenses;
    }

    private static void deAssociateLineItemSchedulesWithExpenses(List<Id> lineItemScheduleIDs)
    {
        List<Expense__c> deAssociatedExpenses = new List<Expense__c>();
        for (Expense__c timeCard : [select OpportunityLineItemScheduleId__c from Expense__c where OpportunityLineItemScheduleId__c in :lineItemScheduleIDs and OpportunityLineItemScheduleId__c != null])
        {
            timeCard.OpportunityLineItemScheduleId__c = null;
            deAssociatedExpenses.add(timeCard);
        }

        update deAssociatedExpenses;
    }

    public static void setProduct(List<Expense__c> expenses) 
    {

        Map<Id, List<Expense__c>> oppIdExpensesMap = new Map<Id, List<Expense__c>>();
        Map<Id, OpportunityLineItem__c> oppIdOppLineItemMap = new Map<Id, OpportunityLineItem__c>();

        for (Expense__c expense : expenses) 
        {

            if (expense.Product__c == null) 
            {

                if (oppIdExpensesMap.containsKey(expense.Opportunity__c)) 
                {
                    oppIdExpensesMap.get(expense.Opportunity__c).add(expense);
                }
                else 
                {
                    oppIdExpensesMap.put(expense.Opportunity__c, new List<Expense__c>{expense});
                }

            }

        }

        if (!oppIdExpensesMap.isEmpty()) 
        {

            for (OpportunityLineItem__c oppLineItem : [
                    SELECT Id, Opportunity__c
                    FROM OpportunityLineItem__c
                    WHERE ProductCode__c = 'Other'
                    AND Opportunity__c IN: oppIdExpensesMap.keySet()
                    ORDER BY CreatedDate DESC
                ])
            {
                oppIdOppLineItemMap.put(oppLineItem.Opportunity__c, oppLineItem);
            }

            for (Id oppId : oppIdExpensesMap.keySet()) 
            {

                if (oppIdOppLineItemMap.containsKey(oppId)) 
                {

                    for (Expense__c expense : oppIdExpensesMap.get(oppId)) 
                    {
                        expense.Product__c = oppIdOppLineItemMap.get(oppId).Id;
                    }

                }
                else
                {

                    for (Expense__c expense : oppIdExpensesMap.get(oppId)) 
                    {
                        expense.addError(Label.No_product_Other_found_in_the_Opportunity_Please_create_a_new_Opportunity_Pro);
                    }

                }

            }

        }

    }

}