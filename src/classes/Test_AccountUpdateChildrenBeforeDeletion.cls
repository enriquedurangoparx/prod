@isTest
public class Test_AccountUpdateChildrenBeforeDeletion {
    
    public static testMethod void testAccountDeletion() {
        Database.DMLOptions dml = new Database.DMLOptions();
        dml.DuplicateRuleHeader.AllowSave = true; 
        Account custAccount = new Account(Name='My number customer 123',AccountType__c='Customer');
        Database.SaveResult sr = Database.insert(custAccount, dml);
        
        Account custAccountChild = new Account(Name='My number customer 456',AccountType__c='Customer',ParentId=custAccount.Id);
        Database.SaveResult sr2 = Database.insert(custAccountChild, dml);
        
        Test.startTest();
            delete custAccount;
        Test.stopTest();
    }
}