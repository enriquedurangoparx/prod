/**
 *  @description KeyAccountManagerHandler functionality
 *  @author ikl
 *  @copyright PARX
*/
public class KeyAccountManagerHandler extends TriggerTemplate.TriggerHandlerAdapter
{

	public override void onAfterInsert(List<sObject> newList, Map<Id, sObject> newMap)
	{
		BrandService.updateKeyAccountManagerFieldOnRelatedBrandRecord(getBrandRelatedIdFromKeyAccountManagers(newList));
	}

	public override void onAfterUpdate(List<sObject> newList, Map<Id, sObject> newMap, List<sObject> oldList, Map<Id, sObject> oldMap)
	{
		BrandService.updateKeyAccountManagerFieldOnRelatedBrandRecord(getBrandRelatedIdFromKeyAccountManagers(newList));
	}


	public override void onAfterDelete (List<sObject> oldList, Map<Id, sObject> oldMap)
	{
		BrandService.updateKeyAccountManagerFieldOnRelatedBrandRecord(getBrandRelatedIdFromKeyAccountManagers(oldList));
	}

	public static Set<Id> getBrandRelatedIdFromKeyAccountManagers(List<KeyAccountManager__c> keyAccountManagers)
	{
		Set<Id> brandIds = new Set<Id>();
		for (KeyAccountManager__c keyAccountManager : keyAccountManagers)
		{
			brandIds.add(keyAccountManager.Brand__c);
		}
		return brandIds;
	}

}