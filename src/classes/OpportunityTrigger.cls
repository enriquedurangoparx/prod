/**
* Handle business logic for Opportunity, executed by the trigger
*
* @author: <Parx Admin> (salesforce.admin+colliers@parx.com)
*
* @history:
* version           | author                                                | changes
* ====================================================================================
* 0.1 08.05.2019    | Parx Admin (salesforce.admin+colliers@parx.com)       | initial version.
* 0.2 23.07.2019    | Christian Schwabe (Christian.Schwabe@colliers.com)    | added a two new test method (testSetPricebook) to test automatic evaluation for the correct pricebook on opportunity and
*                                                                             add the correct opportunity-product (testCreateStandardOpportunityProduct)
* 0.3 25.07.2019    | Christian Schwabe (Christian.Schwabe@colliers.com)    | Changed setOpportunityName(...) to setOpportunityName(..., ...) in case of US594 to provide new naming convention.
* 0.4 26.07.2019    | Parx Admin (salesforce.admin+colliers@parx.com)       | removed the method to send notifcation about the owner change (moved to process builder).
* 0.5 26.07.2019    | Christian Schwabe (Christian.Schwabe@colliers.com)    | Added OpportunityHandler.checkEmbargolist in hookBeforeInsert(...) and hookBeforeUpdate(...) based on US567.
* 0.6 03.02.2020    | Maksim Fedorenko (maksim.fedorneko@parx.com)          | Added OpportunityHandler.copyPropertyPortfolioData in hookAfterInsert(...) and hookBeforeUpdate(...) based on US 705.
* 0.7 19.03.2020    | Nadzeya Polautsava (nadzeya.polautsava@parx.com)      | Added deactivateRelatedCampaigns for US2522
*/

	public with sharing class OpportunityTrigger extends ATrigger
{
    private static final Id VALUATION_PROJECT_RECORD_TYPE_ID;
    private static final Id VALUATION_FRAMEWORK_CONTRACT_RECORD_TYPE_ID;
    private static final List<SObjectField> FALSE_OS_FEE_SHARE_FIELDS_TO_NULLIFY = new List<SObjectField>{
        FeeSharing__c.PercentageForExecution__c,
        FeeSharing__c.PercentageForIdentification__c,
        FeeSharing__c.PercentageForOrderAcquisition__c,
        FeeSharing__c.PercentageForReporting__c,
        FeeSharing__c.DistributionForExecution__c,
        FeeSharing__c.DistributionForIdentification__c,
        FeeSharing__c.DistributionForOrderAcquisition__c,
        FeeSharing__c.DistributionForReporting__c
    };

    private static final List<SObjectField> TRUE_OS_FEE_SHARE_FIELDS_TO_NULLIFY = new List<SObjectField>{
        FeeSharing__c.Description__c,
        FeeSharing__c.SharePercentage__c
    };

    @testvisible
    private static final Map<Id, Id> ACQUISITION_TO_PROJECT_RECORDTYPE_ID_MAP = new Map<Id, Id>();
    static
    {
        Map<String, RecordTypeInfo> opportunityRecordTypeInfosByDeveloperName = SObjectType.Opportunity.getRecordTypeInfosByDeveloperName();
        for (String recordtypePrefix : new List<String>{'OS', 'Valuation', 'WPC'})
        {
            RecordTypeInfo acquisitionRecordTypeInfo = opportunityRecordTypeInfosByDeveloperName.get(recordtypePrefix + 'Acquisition');
            RecordTypeInfo projectRecordTypeInfo = opportunityRecordTypeInfosByDeveloperName.get(recordtypePrefix + 'Project');

            if (acquisitionRecordTypeInfo != null && projectRecordTypeInfo != null)
            {
                ACQUISITION_TO_PROJECT_RECORDTYPE_ID_MAP.put(acquisitionRecordTypeInfo.getRecordTypeId(), projectRecordTypeInfo.getRecordTypeId());
            }
        }

        if (opportunityRecordTypeInfosByDeveloperName.get('ValuationProject') != null)
        {
            VALUATION_PROJECT_RECORD_TYPE_ID = opportunityRecordTypeInfosByDeveloperName.get('ValuationProject').getRecordTypeId();
        }

        if (opportunityRecordTypeInfosByDeveloperName.get('ValuationFrameworkContract') != null)
        {
            VALUATION_FRAMEWORK_CONTRACT_RECORD_TYPE_ID = opportunityRecordTypeInfosByDeveloperName.get('ValuationFrameworkContract').getRecordTypeId();
        }
    }

    public override void hookBeforeInsert(List<SObject> records)
    {
        woSharing().maintainOsFeeShare((List<Opportunity>) records, null);
        populateOpportunityName((List<Opportunity>) records);
        OpportunityHandler.setPricebook(records);
        OpportunityHandler.checkEmbargolist((List<Opportunity>) records);
        OpportunityHandler.copyPropertyPortfolioData(records, null);
    }

    public override void hookBeforeUpdate(List<SObject> records, List<SObject> oldRecords)
    {
        modifyInstructedAcquisitionRecords((List<Opportunity>) records, (List<Opportunity>) oldRecords);
        woSharing().maintainOsFeeShare((List<Opportunity>) records, (List<Opportunity>) oldRecords);
        populateOpportunityName((List<Opportunity>) records, (List<Opportunity>) oldRecords);
        OpportunityHandler.createOpportunityFollowUps((List<Opportunity>) records, (List<Opportunity>) oldRecords);
        OpportunityHandler.clearOpportunityDelegatedOwner((List<Opportunity>) records, (List<Opportunity>) oldRecords);
        OpportunityHandler.checkEmbargolist((List<Opportunity>) records);
        OpportunityHandler.copyPropertyPortfolioData(records, oldRecords);
        OpportunityHandler.copyPortfolioDataOnOpportunityClosing(records, oldRecords);
    }

    public override void hookAfterInsert(List<SObject> records)
    {
        OpportunityHandler.createStandardOpportunityProduct(records);
    }

    public override void hookAfterUpdate(List<SObject> records, List<SObject> oldRecords)
    {
        OpportunityHandler.deactivateRelatedCampaigns(records, oldRecords);
    }

    private OpportunityTriggerWoSharing woSharing()
    {
        return new OpportunityTriggerWoSharing(this);
    }

    /**
     * When Opportunity with record type '*Acquisition' is enters "Instructed" Stage - change record type to corresponding Project record type.
     */
    private void modifyInstructedAcquisitionRecords(List<Opportunity> opportunities, List<Opportunity> oldOpportunities)
    {
        for (Integer i = 0, n = opportunities.size(); i < n; i++)
        {
            Opportunity opportunityRecord = opportunities.get(i);
            Opportunity oldOpportunity = oldOpportunities.get(i);

            modifyInstructedAcquisitionRecords(opportunityRecord, oldOpportunity);
        }
    }

    /**
     * When Opportunity with record type '*Acquisition' is enters "Commissioned" Stage - change record type to corresponding Project record type.
     */
    private void modifyInstructedAcquisitionRecords(Opportunity opportunityRecord, Opportunity oldOpportunity)
    {
        if (opportunityRecord.StageName == 'Commissioned' && opportunityRecord.StageName != oldOpportunity.StageName && isAcquisitionRecord(opportunityRecord))
        {
            opportunityRecord.RecordTypeId = ACQUISITION_TO_PROJECT_RECORDTYPE_ID_MAP.get(opportunityRecord.RecordTypeId);
        }
    }

    private Boolean isAcquisitionRecord(Opportunity opportunityRecord)
    {
        return ACQUISITION_TO_PROJECT_RECORDTYPE_ID_MAP.get(opportunityRecord.RecordTypeId) != null;
    }

    /**
     * Shorthand for populateOpportunityName(opportunities, new List<Opportunity>());
     */
    private void populateOpportunityName(List<Opportunity> opportunities)
    {
        populateOpportunityName(opportunities, new List<Opportunity>());
    }

    /**
     * Populates Opportunity.Name automatically for "Valuation Project" records.
     *
     * @param list_opportunities
     * @param list_oldOpportunities
     */
    private void populateOpportunityName(List<Opportunity> list_opportunities, List<Opportunity> list_oldOpportunities)
    {
        List<Opportunity> list_tempClonedOpportunities = new List<Opportunity>();
        List<Opportunity> list_valuationOpportunities = new List<Opportunity>();
        for (Integer i = 0, n = list_opportunities.size(); i < n; i++)
        {
            Opportunity opportunity = list_opportunities.get(i);
            Opportunity oldOpportunity = ! list_oldOpportunities.isEmpty() ? list_oldOpportunities.get(i) : null;
            Boolean boolean_isUpdate = oldOpportunity != null ? true : false;

            if (opportunity.RecordTypeId == VALUATION_PROJECT_RECORD_TYPE_ID
                    && (oldOpportunity == null || oldOpportunity.RecordTypeId != opportunity.RecordTypeId)
                )
            {
                list_valuationOpportunities.add(opportunity);

                if(oldOpportunity != null){// Prevent disbalance between list_valuationOpportunities and list_oldOpportunities for further processing.
                    list_tempClonedOpportunities.add(oldOpportunity);
                }
            }
            else if (opportunity.RecordTypeId == VALUATION_FRAMEWORK_CONTRACT_RECORD_TYPE_ID &&
                        boolean_isUpdate && oldOpportunity.StageName != opportunity.StageName && opportunity.StageName == 'Commissioned'
                    )//CSW: If a existing Valuation Framework Contract-Opportunity is updated to StageName 'Commissioned' => Add autonumber to Name.
            {
                list_valuationOpportunities.add(opportunity);

                if(oldOpportunity != null){// Prevent disbalance between list_valuationOpportunities and list_oldOpportunities for further processing.
                    list_tempClonedOpportunities.add(oldOpportunity);
                }
            }
        }
        OpportunityHandler.setOpportunityName(list_valuationOpportunities, new Map<Id, Opportunity>(list_tempClonedOpportunities)/* CSW */);
    }

    /**
     * A "without sharing" class to bypass sharing settings.
     */
    private without sharing class OpportunityTriggerWoSharing
    {
        private OpportunityTrigger wSharing;
        public OpportunityTriggerWoSharing(OpportunityTrigger wSharing)
        {
            this.wSharing = wSharing;
        }

        /**
     * Maintains Opportunity.OSFeeShare__c field value when record is created or Owner is changed,
     * and also updates child FeeShare__c records if Opportunity.OSFeeShare__c has been changed.
     */
        private void maintainOsFeeShare(List<Opportunity> opportunities, List<Opportunity> oldOpportunities)
        {
            Set<Id> ownersWithOsFeeSharePermission = new Map<Id, User>(selectOwnersWithOsFeeSharePermission(opportunities)).keySet();
            List<FeeSharing__c> feeSharings = selectChildFeeSharing(opportunities);
            List<FeeSharing__c> modifiedFeeSharings = new List<FeeSharing__c>();

            for (Integer i = 0, n = opportunities.size(); i < n; i++)
            {
                Opportunity opportunity = opportunities.get(i);
                Opportunity oldOpportunity = (oldOpportunities != null ? oldOpportunities.get(i) : null);

                if (oldOpportunity == null
                    || opportunity.OwnerId != oldOpportunity.OwnerId && !hasFeeSharings(opportunity, feeSharings))
                {
                    opportunity.OSFeeShare__c = ownersWithOsFeeSharePermission.contains(opportunity.OwnerId);
                }

                if (oldOpportunity != null && opportunity.OSFeeShare__c != oldOpportunity.OSFeeShare__c)
                {
                    modifiedFeeSharings.addAll(modifyFeeSharings(opportunity, feeSharings));
                }
            }

            update modifiedFeeSharings;
        }

        /**
         * Nullifies certain field on child FeeSharing__c records based on Opportunity.OSFeeShare__c field value,
         * when Opportunity.OSFeeShare__c has been changed.
         */
        private List<FeeSharing__c> modifyFeeSharings(Opportunity opportunityRecord, List<FeeSharing__c> feeSharings)
        {
            List<FeeSharing__c> modifiedFeeSharings = new List<FeeSharing__c>();
            for (FeeSharing__c feeSharing : feeSharings)
            {
                if (feeSharing.Opportunity__c != opportunityRecord.Id)
                {
                    continue;
                }

                for (SObjectField field : (opportunityRecord.OSFeeShare__c ? TRUE_OS_FEE_SHARE_FIELDS_TO_NULLIFY : FALSE_OS_FEE_SHARE_FIELDS_TO_NULLIFY))
                {
                    feeSharing.put(field, null);
                }

                modifiedFeeSharings.add(feeSharing);
            }

            return modifiedFeeSharings;
        }

        /**
        * Selects Users which are owners of passed Opportunity records and have "OS Fee Share" Custom Permission
         */
        public List<User> selectOwnersWithOsFeeSharePermission(List<Opportunity> opportunities)
        {
            List<Id> ownerIDs = new List<Id>();
            for (Opportunity each : opportunities)
            {
                ownerIDs.add(each.OwnerId);
            }

            List<CustomPermission> osFeeShareCustomPermissions = OpportunityService.osFeeShare;
            List<PermissionSet> osFeeSharePermissionSets = [select Id from PermissionSet where Id in (select ParentId from SetupEntityAccess where SetupEntityId in :osFeeShareCustomPermissions)];

            return [select Username from User where Id in (select AssigneeId from PermissionSetAssignment where PermissionSetId in :osFeeSharePermissionSets and AssigneeId in :ownerIDs) and Id in :ownerIDs];
        }

        /**
         * Selects child FeeSharing__c records for passed Opportunity records.
         */
        private List<FeeSharing__c> selectChildFeeSharing(List<Opportunity> opportunities)
        {
            return [select Opportunity__c from FeeSharing__c where Opportunity__c in :opportunities];
        }

        /**
         * Returns TRUE if passed Opportunity record has child FeeSharing__c record amongst FeeSharing__c records from feeSharings
         */
        private Boolean hasFeeSharings(Opportunity opportunityRecord, List<FeeSharing__c> feeSharings)
        {
            for (FeeSharing__c each : feeSharings)
            {
                if (each.Opportunity__c == opportunityRecord.Id)
                {
                    return true;
                }
            }

            return false;
        }
    }
}