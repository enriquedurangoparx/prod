/**
* Test class for LWCAreaMapController.cls
*
* @author: <Egor Markuzov> (egor.markuzov@parx.com)
*
* @history:
* version		    | author				                                | changes
* ====================================================================================
* 0.1 26.11.2019	| Egor Markuzov (egor.markuzov@parx.com)                | initial version.
*/
@isTest
private class LWCAreaMapControllerTest {
    
    /**
    * test getPropertyObjects()
    */
    @isTest
    public static void testGetPropertyObjects() 
    {
        Area__c newArea = TestDataFactory.createArea('Test Area');
        insert newArea;

        List<PropertyObject__c> newObjectList = TestDataFactory.createPropertyObjectWithArea(newArea.Id);
        insert newObjectList;

        List<PropertyObject__c> listToCheck = LWCAreaMapController.getPropertyObjects(newArea.Id);

        System.assertEquals(newObjectList.size(), listToCheck.size(), 'Wrong number of property object records.');

    }
}