/**
* Portfolio service class
*
* @author: <Maksim Fedorenko> (maksim.fedorenko@parx.com)
*
* @history:
* version		    | author				                                | changes
* ====================================================================================
* 0.1 31.01.2020	| Maksim Fedorenko (maksim.fedorenko@parx.com)          | initial version.
* 0.2 17.02.2020	| Christian Schwabe (Christian.Schwabe@colliers.com)    | [SFINV/US 1977] Added a validation rule to prevent partial changing on Portfolio__c and
                                                                            | add a field-update on PreventPartialChanging__c if there are more than one PropertObject__c related.
*/
public with sharing class PortfolioService
{
    public static final Map<SObjectField, SObjectField> sourceTargetFieldMap = new Map<SObjectField, SObjectField> {
            Portfolio__c.WALT__c => Opportunity.WALTInYears__c,
            Portfolio__c.GrossYield__c => Opportunity.GrossYield__c,
            Portfolio__c.NetYield__c => Opportunity.NetYield__c,
            Portfolio__c.FactorActual__c => Opportunity.FactorActual__c,
            Portfolio__c.FactorTarget__c => Opportunity.FactorTarget__c,
            Portfolio__c.IRR__c => Opportunity.IRR__c,
            Portfolio__c.CoC__c => Opportunity.CoC__c,
            Portfolio__c.AmountOfSpaceLet__c => Opportunity.AmountOfSpaceLet__c,
            Portfolio__c.SqmPlot__c => Opportunity.PlotSqm__c,
            Portfolio__c.SqmInTotal__c => Opportunity.TotalSqm__c
    };

    /**
     * @azure US 705 Übernahme der Feldwerte aus dem verlinkten Objekt / Portfolio in die Opportunity
     * @description Copies Portfolio fields, described in the sourceTargetFieldMap to respective opportunity records
     * @param newRecords List of new Portfolio records
     */
    public static void copyDataToOpportunity (List<Portfolio__c> newRecords)
    {
        Map<Id, List<Opportunity>> portfolioIdOpportunityListMap = getPortfolioIdOpportunityListMap(new Map<Id, Portfolio__c>(newRecords));
        OpportunityService.copyDataToOpportunity((List<SObject>) newRecords, portfolioIdOpportunityListMap, sourceTargetFieldMap);
    }

    /**
     * Returns a map of PropertyObject => Opportunity list for a given map of PropertyObject
     * @param propertyIdList Map of PropertyObject__c records
     * @return Map of PropertyObjectId => Opportunity list
     */
    public static Map<Id, List<Opportunity>> getPortfolioIdOpportunityListMap(Map<Id, Portfolio__c> portfolioObjectMap)
    {
        Map<Id, List<Opportunity>> portfolioIdOpportunityListMap = new Map<Id, List<Opportunity>>();
        List<Portfolio__c> portfolios = [
                SELECT Id, (
                        SELECT Id, WALTInYears__c, GrossYield__c, NetYield__c, FactorActual__c, FactorTarget__c, IRR__c, CoC__c, AmountOfSpaceLet__c,
                                PlotSqm__c, TotalSqm__c, IsClosed
                        FROM Opportunities__r)
                FROM Portfolio__c
                WHERE Id IN :portfolioObjectMap.keySet()
        ];

        for (Portfolio__c portfolio : portfolios)
        {
            portfolioIdOpportunityListMap.put(portfolio.Id, portfolio.Opportunities__r);
        }
        return portfolioIdOpportunityListMap;
    }

    /**
     * Returns a map of Id => Portfolio__c records for a given portfolioIdSet
     * @param portfolioIdSet Set of Portfolio__c ids
     * @return Map of Id => Portfolio__c records for a given portfolioIdSet
     */
    public static Map<Id, Portfolio__c> getPortfolioMap(Set<Id> portfolioIdSet)
    {
        Map<Id, Portfolio__c> result = new Map<Id, Portfolio__c>();
        if (!portfolioIdSet.isEmpty())
        {
            result = new Map<Id, Portfolio__c>([
                    SELECT WALT__c, GrossYield__c, NetYield__c, FactorActual__c, FactorTarget__c, IRR__c, CoC__c, AmountOfSpaceLet__c, SqmPlot__c, SqmInTotal__c
                    FROM Portfolio__c
                    WHERE Id IN :portfolioIdSet
            ]);
        }
        return result;
    }

    /**
     * Prevent changing on Countries__c, FederalState__c, MacroLocations__c, Markets__c, MicroLocations__c, NumberOfProperties__c, Submarkets__c.
     * Prevent manual changing of the above-mentioned fields, if you change fields manually on Portfolio__c if there is one or more PropertyObject__c related.
     * NumerOfProperties__c is aggregated by rollup-helper.
     */
    public static void preventPartialChangingOnPortfolio(List<Portfolio__c> newRecords, List<Portfolio__c> oldRecords){
        Map<Id, Portfolio__c> mapOfOldPortfolioById = new Map<Id, Portfolio__c>(oldRecords);

        for(Portfolio__c newPortfolio : newRecords){
            Id portfolioId = newPortfolio.Id;
            Portfolio__c oldPortfolio = mapOfOldPortfolioById.get(portfolioId);

            // Prevent changing if validation is active.
            if(newPortfolio.PreventPartialChanging__c && (
                newPortfolio.Countries__c != oldPortfolio.Countries__c ||
                newPortfolio.Markets__c != oldPortfolio.Markets__c ||
                newPortfolio.Submarkets__c != oldPortfolio.Submarkets__c ||
                newPortfolio.FederalState__c != oldPortfolio.FederalState__c ||
                newPortfolio.MacroLocations__c != oldPortfolio.MacroLocations__c ||
                newPortfolio.MicroLocations__c != oldPortfolio.MicroLocations__c
            )){
                newPortfolio.addError(System.Label.PreventPartialChangeOnPortfolio);
            }
        }
    }

    /**
     * Set necessary field for custom apex validation rule related to SFINV/US 1977.
     */
    public static void setPartialChangingOnPortfolio(List<Portfolio__c> newRecords, List<Portfolio__c> oldRecords){
        for(Portfolio__c newPortfolio : newRecords){
            if(newPortfolio.NumberOfProperties__c > 0){
                newPortfolio.PreventPartialChanging__c = true;
            }
        }
    }
}