/**
*	
*	@author npo
*	@copyright PARX
*/
public with sharing class TriggerUtil
{
	/**
	Get Id set of modified object by defined list of fields.
	That means we can list of objects, were one or more fields where modified.
	*/
	public static Set<Id> getModifiedObj(List<String> theFieldsList, List<sObject> theObjects, Map<Id, sObject> theOldObjects)
	{
		Set<Id> theResult = new Set<Id>();
		for (sObject anObject : theObjects)
		{
			if (theOldObjects == null)
			{
				theResult.add(anObject.Id);
			} else
			{
				sObject anOldObject = theOldObjects.get(anObject.Id);
				if (anOldObject == null)
				{
					theResult.add(anObject.Id);
				} else
				{
					for (String aField : theFieldsList)
					{
						if (anObject.get(aField) != anOldObject.get(aField))
						{
							theResult.add(anObject.Id);
						}
					}
				}
			}
		}
		return theResult;
	}
	/**
	Get Id set of modified object by defined list of fields.
	That means we can list of objects, were one or more fields where modified.
*/
	public static Set<sObject> getModifiedObjects(List<String> theFieldsList, List<sObject> theObjects, Map<Id, sObject> theOldObjects)
	{
		Set<sObject> aResult = new Set<sObject>();
		if (theObjects == null)
		{
			// On delete
			aResult = new Set<sObject>(theOldObjects.values());
		} else if (theObjects != null && theOldObjects != null)
		{
			// On update
			Map<Id, sObject> newObjectsMap = new Map<Id, sObject>(theObjects);
			for (Id soId : getModifiedObj(theFieldsList, theObjects, theOldObjects))
			{
				aResult.add(newObjectsMap.get(soId));
			}
		} else if (theObjects != null && theOldObjects == null)
		{
			// on after insert
			for (sObject anObject : theObjects)
			{
				aResult.add(anObject);
			}
		}
		return aResult;
	}

	/*
		Get list of modified objects by defined list of modified fields.
	*/
	public static List<sObject> getModifiedObjectsList(List<String> fieldsList, List<sObject> newObjects, Map<Id, sObject> oldObjectsMap)
	{
		Set<sObject> aResult = getModifiedObjects(fieldsList, newObjects, oldObjectsMap);
		List<sObject> resultList = new List<sObject>();
		resultList.addAll(aResult);
		return resultList;
	}
}