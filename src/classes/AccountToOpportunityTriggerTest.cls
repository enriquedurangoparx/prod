/**
* Testclass for AccountToOpportunityTrigger.cls
*
* @author: <Egor Markuzov> (egor.markuzov@parx.com)
*
* @history:
* version		    | author				                                | changes
* ====================================================================================
* 0.1 02.20.2020	| Egor Markuzov (egor.markuzov@parx.com)                | initial version.
*/
@isTest
private class AccountToOpportunityTriggerTest {
    @testSetup
    static void setup()
    {
        TestDataFactory.initializeProducts();

        Embargo__c embargo = new Embargo__c();
        embargo.Name = 'Embargo';
        embargo.Countries__c = 'Egypt;Afghanistan;Belarus;Burundi;Iraq;Yemen';
        insert embargo;
    }

    /**
    * test preventDeletionIfHasRelatedActivity hookBeforeDelete for AccountToOpportunityTrigger
    */
    @isTest
    public static void testPreventDeletionIfHasRelatedActivity()
    {
        Account newAccount = TestDataFactory.createAccount('Test Account 1');
        insert newAccount;

        PropertyObject__c propertyObject = TestDataFactory.buildPropertyObject('property', true);
        Opportunity newOpportunity = TestDataFactory.createOpportunity(newAccount.Id, propertyObject);
        insert newOpportunity;

        AccountToOpportunity__c newAccToOpp = TestDataFactory.createAccountToOpportunity(newAccount.Id, newOpportunity.Id);
        insert newAccToOpp;

        Task newTask = TestDataFactory.createTask(newAccToOpp.Id);
        newTask.RecordTypeId = Schema.SObjectType.Task.getRecordTypeInfosByDeveloperName().get('Offer').getRecordTypeId();
        newTask.InvestorStatus__c = 'Offer';
        insert newTask;

        System.assertEquals(newTask.Id, [SELECT Id FROM Task WHERE Id = :newTask.Id].Id);

        try 
        {
            delete newAccToOpp;
        }
        catch (Exception e)
        {
            System.assertEquals(true, e.getMessage().contains(System.Label.GeneralLabelErrorBecauseHasActivityOrStatus));
        }

    }
}