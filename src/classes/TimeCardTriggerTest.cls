/**
 * @author ile
 * @copyright PARX
 */

/**
* Testclass for TimeCard__c.
*
* @author: <Christian Schwabe> (Christian.Schwabe@colliers.com)
*
* @history:
* version		    | author				                                | changes
* ====================================================================================
* 0.1 03.04.2019	| Parx Admin (salesforce.admin+colliers@parx.com)	    | initial version.
* 0.2 31.07.2019	| Christian Schwabe (Christian.Schwabe@colliers.com)	| Added embargo-object and new required field StartState__c.
*/
@istest
private class TimeCardTriggerTest
{
    @testsetup
    private static void setup()
    {
        insert new List<ONB2__TriggerSettings__c>{
            new ONB2__TriggerSettings__c(
                Name = 'InvoiceBeforeInsert'
            ),
            new ONB2__TriggerSettings__c(
                Name = 'InvoiceBeforeUpdate'
            ),
            new ONB2__TriggerSettings__c(
                Name = 'InvoiceLineItemBeforeInsert'
            ),
            new ONB2__TriggerSettings__c(
                Name = 'InvoiceLineItemBeforeUpdate'
            )
        };
    }

    private static User generateUser()
    {
        User testUser = UnitTestDataTest.buildUser();
        testUser.HoursPerDay__c = null;
		
        insert testUser;

        PermissionSetAssignment psAssignment;
        for (PermissionSet each : [select Name from PermissionSet where Name = 'ManageTimeCards'])
        {
            psAssignment = new PermissionSetAssignment(AssigneeId = testUser.Id, PermissionSetId = each.Id);
        }

        insert psAssignment;

        return testUser;
    }

    private static Map<String, SObject> prepareData()
    {
        OpportunityHandlerTest.initializeProducts();// Needed to setup product, pricebook and pricebookentry.
        
        Embargo__c embargo = new Embargo__c();
        embargo.Name = 'Embargo';
        embargo.Countries__c = 'Egypt;Afghanistan;Belarus;Burundi;Iraq;Yemen';
        insert embargo;

        PropertyObject__c propertyObject = TestDataFactory.buildPropertyObject('property', true);
        Opportunity project = UnitTestDataTest.buildOpportunity('Project Opportunity', propertyObject);
        project.BillingType__c = 'Time and Material';
        project.RecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('ValuationAcquisition').getRecordTypeId();
        project.ProjectStart__c = Date.today();
        project.CloseDate = Date.today().addDays(3);
        insert project;
		
        ProjectRole__c projectRole = new ProjectRole__c(Project__c = project.Id, DailyRate__c = 10, TargetAmountHours__c = 8, StartDate__c = System.today().addDays(-1), EndDate__c = System.today().addDays(10));
        insert projectRole;

        Product2 productA = new Product2(Name = 'A', CanUseRevenueSchedule = true, RelevantForFeeSharing__c = true);
        Product2 productB = new Product2(Name = 'B', CanUseRevenueSchedule = true, RelevantForFeeSharing__c = true);
        insert new List<Product2>{productA, productB};

        PricebookEntry standardPricebookEntry = new PricebookEntry();
        standardPricebookEntry.IsActive = true;
        standardPricebookEntry.Pricebook2Id = Test.getStandardPricebookId();
        standardPricebookEntry.Product2Id = productA.Id;
        standardPricebookEntry.UnitPrice = 100;
        insert standardPricebookEntry;

        Pricebook2 valuationPricebook = [SELECT Id FROM Pricebook2 WHERE Name = :OpportunityHandler.VALUATION_PRICEBOOKNAME];

        PricebookEntry pricebookEntryA = standardPricebookEntry;//new PricebookEntry(Pricebook2Id = Test.getStandardPricebookId(), Product2Id = productA.Id, UnitPrice = 1, IsActive = true);
        //PricebookEntry pricebookEntryB = new PricebookEntry(Pricebook2Id = Test.getStandardPricebookId(), Product2Id = productB.Id, UnitPrice = 1, IsActive = true);
        PricebookEntry pricebookEntryB = [SELECT Id FROM PriceBookEntry WHERE Pricebook2Id = :valuationPricebook.Id];//new PricebookEntry(Pricebook2Id = valuationPricebook.Id, Product2Id = productB.Id, UnitPrice = 1, IsActive = true);


        /*PricebookEntry pricebookEntryA = new PricebookEntry(Pricebook2Id = Test.getStandardPricebookId(), Product2Id = productA.Id, UnitPrice = 1, IsActive = true);
        PricebookEntry pricebookEntryB = new PricebookEntry(Pricebook2Id = Test.getStandardPricebookId(), Product2Id = productB.Id, UnitPrice = 1, IsActive = true);
        insert new List<PricebookEntry>{pricebookEntryA, pricebookEntryB};*/

        Opportunity testOpportunity = project;/*UnitTestDataTest.buildOpportunity('Test Opportunity');
        testOpportunity.BillingType__c = 'Time and Material';
        insert testOpportunity;*/

        Account accountA = UnitTestDataTest.buildAccount('Account A');
        insert accountA;

        OpportunityLineItem opportunityProduct = new OpportunityLineItem(Product2Id = productA.Id, PricebookEntryId = pricebookEntryB.Id, OpportunityId = testOpportunity.Id, Quantity = 1, UnitPrice = 10);
        insert opportunityProduct;
        
        List<OpportunityLineItem__c> list_customOpportunityProduct = [SELECT Id FROM OpportunityLineItem__c WHERE OpportunityProductId__c = :opportunityProduct.Id];
        system.assertEquals(1, list_customOpportunityProduct.size());

        TimeCard__c timeCard = new TimeCard__c(Product__c = list_customOpportunityProduct.get(0).Id, User__c = UserInfo.getUserId(), Project__c = testOpportunity.Id, HoursWorked__c = 1, Description__c = 'Testing', Role__c = projectRole.Id);
        insert timeCard;

        ONB2__Template__c templateA = new ONB2__Template__c();
        insert templateA;

        ONB2__Invoice__c invoiceA = new ONB2__Invoice__c(Name = 'A', ONB2__Account__c = accountA.Id, ONB2__Template__c = templateA.Id);
        ONB2__Invoice__c invoiceB = new ONB2__Invoice__c(Name = 'A', ONB2__Account__c = accountA.Id, ONB2__Template__c = templateA.Id);
        insert new List<ONB2__Invoice__c>{invoiceA, invoiceB};
		
        return new Map<String, SObject>{
            'Product A' => productA,
            'Product B' => productB,
            'Price Book Entry A' => pricebookEntryA,
            'Price Book Entry B' => pricebookEntryB,
            'Opportunity' => testOpportunity,
            'Opportunity Product' => opportunityProduct,
            'Opportunity Line Item Custom' => list_customOpportunityProduct.get(0),
            'Invoice A' => invoiceA,
            'Invoice B' => invoiceB,

            'Project' => project,
            'Project Role' => projectRole,
            'Time Card' => timeCard
        };
    }

    private static Map<String, SObject> prepareDataSmall()
    {
        OpportunityHandlerTest.initializeProducts();// Needed to setup product, pricebook and pricebookentry.

	    Embargo__c embargo = new Embargo__c();
	    embargo.Name = 'Embargo';
	    embargo.Countries__c = 'Egypt;Afghanistan;Belarus;Burundi;Iraq;Yemen';
	    insert embargo;

        PropertyObject__c propertyObject = TestDataFactory.buildPropertyObject('property', true);
        Opportunity project = UnitTestDataTest.buildOpportunity('Project Opportunity', propertyObject);
        project.BillingType__c = 'Time and Material';
        project.RecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('ValuationAcquisition').getRecordTypeId();
        project.ProjectStart__c = Date.today();
        project.CloseDate = Date.today().addDays(3);
        insert project;

        ProjectRole__c projectRole = new ProjectRole__c(Project__c = project.Id, DailyRate__c = 10, TargetAmountHours__c = 8, StartDate__c = System.today().addDays(-1), EndDate__c = System.today().addDays(10));
        insert projectRole;

        Product2 productA = new Product2(Name = 'A', CanUseRevenueSchedule = true, RelevantForFeeSharing__c = true);
        Product2 productB = new Product2(Name = 'B', CanUseRevenueSchedule = true, RelevantForFeeSharing__c = true);
        insert new List<Product2>{productA, productB};

        PricebookEntry standardPricebookEntry = new PricebookEntry();
        standardPricebookEntry.IsActive = true;
        standardPricebookEntry.Pricebook2Id = Test.getStandardPricebookId();
        standardPricebookEntry.Product2Id = productA.Id;
        standardPricebookEntry.UnitPrice = 100;
        insert standardPricebookEntry;

        Pricebook2 valuationPricebook = [SELECT Id FROM Pricebook2 WHERE Name = :OpportunityHandler.VALUATION_PRICEBOOKNAME];

        PricebookEntry pricebookEntryA = standardPricebookEntry;
        PricebookEntry pricebookEntryB = [SELECT Id FROM PriceBookEntry WHERE Pricebook2Id = :valuationPricebook.Id];

        Opportunity testOpportunity = project;

        Account accountA = UnitTestDataTest.buildAccount('Account A');
        insert accountA;

        OpportunityLineItem opportunityProduct = new OpportunityLineItem(Product2Id = productA.Id, PricebookEntryId = pricebookEntryB.Id, OpportunityId = testOpportunity.Id, Quantity = 1, UnitPrice = 10);
        insert opportunityProduct;

        List<OpportunityLineItem__c> list_customOpportunityProduct = [SELECT Id FROM OpportunityLineItem__c WHERE OpportunityProductId__c = :opportunityProduct.Id];
        system.assertEquals(1, list_customOpportunityProduct.size());

        TimeCard__c timeCard = new TimeCard__c(Product__c = list_customOpportunityProduct.get(0).Id, User__c = UserInfo.getUserId(), Project__c = testOpportunity.Id, HoursWorked__c = 1, Description__c = 'Testing', Role__c = projectRole.Id);
        insert timeCard;

        return new Map<String, SObject>{
            'Product A' => productA,
            'Product B' => productB,
            'Price Book Entry A' => pricebookEntryA,
            'Price Book Entry B' => pricebookEntryB,
            'Opportunity' => testOpportunity,
            'Opportunity Product' => opportunityProduct,
            'Opportunity Line Item Custom' => list_customOpportunityProduct.get(0),
            'Project' => project,
            'Project Role' => projectRole,
            'Time Card' => timeCard
        };
    }

    @istest
    private static void shouldDeleteTimeCard()
    {
        User testUser = generateUser();
        Test.startTest();
        
        System.runAs(testUser)
        {
            Map<String, SObject> dataMap = prepareData();
            
            TimeCard__c timeCard = (TimeCard__c) dataMap.get('Time Card');
            delete timeCard;
        }
        
        Test.stopTest();
    }

    @istest
    private static void shouldMaintainHoursWorkedForPercentage()
    {
        //User testUser = generateUser();
        Map<String, SObject> dataMap = prepareData();
		
        Test.startTest(); 
        User testUser = [ select Id, HoursPerDay__c from User where Id = :UserInfo.getUserId() ];	
        System.runAs(testUser)
        {
            Opportunity project = (Opportunity) dataMap.get('Project');
            project.TimeTrackingType__c = 'Percentage';
            update project;
            
            TimeCard__c timeCard = (TimeCard__c) dataMap.get('Time Card');
            try
            {
                update timeCard;
                System.assert(false, 'Should\'ve thrown exception about User.HoursPerDay__c above!');
            }
            catch (Exception e)
            {
                System.assert(true, 'Should\'ve thrown exception about User.HoursPerDay__c above!');
            }
            
            testUser.HoursPerDay__c = 8;
            update testUser;
            
            try
            {
                timeCard.PercentageWorked__c = 50;
                update timeCard;
            }
            catch (Exception e)
            {
                System.assert(false, 'Shouldn\'t have ve thrown exception above!');
            }
        }
    }

    @istest
    private static void shouldMaintainSchedulesRevenueAndAssociation()
    {
        Map<String, SObject> objectMap = prepareData();
        OpportunityLineItemSchedule lineItemSchedule;
        TimeCard__c timeCard;

        timeCard = ((TimeCard__c) objectMap.get('Time Card')).clone(false, true);
        timeCard.Project__c = objectMap.get('Opportunity').Id;
        timeCard.Product__c = objectMap.get('Opportunity Line Item Custom').Id;
        insert timeCard;

        Test.startTest();
        lineItemSchedule = new OpportunityLineItemSchedule(OpportunityLineItemId = objectMap.get('Opportunity Product').Id, Revenue = 100, ScheduleDate = System.today().addDays(10), Type = 'Revenue');
        insert lineItemSchedule;

        OpportunityLineItemSchedule existingLineItemSchedule = [SELECT Id, Revenue FROM OpportunityLineItemSchedule WHERE Id = :lineItemSchedule.Id LIMIT 1];
        System.assertEquals(2.5, existingLineItemSchedule.Revenue, '(10 daily rate / 8 hours per day) * 1 hour of work * 2 Timecards = 2.5');

        timeCard.Product__c = null;
        update timeCard;

        Test.stopTest();

        TimeCard__c updatedTimeCard = [SELECT OpportunityLineItemScheduleId__c FROM TimeCard__c WHERE Id = :timeCard.Id LIMIT 1];
        System.assertEquals(null, updatedTimeCard.OpportunityLineItemScheduleId__c, 'OpportunityLineItemScheduleId__c should be cleared if Product is set to null');
        existingLineItemSchedule = [SELECT Id, Revenue FROM OpportunityLineItemSchedule WHERE Id = :lineItemSchedule.Id LIMIT 1];
        System.assertEquals(1.25, existingLineItemSchedule.Revenue, '10 daily rate / 8 hours per day * 1 hour of work * 1 Timecard = 2.5');
    }

	@istest
	private static void shouldMaintainSchedulesRevenueAndAssociationBillability_Insert()
	{
		Map<String, SObject> objectMap = prepareDataSmall();
		OpportunityLineItemSchedule lineItemSchedule;

		List<TimeCard__c> timeCards = new List<TimeCard__c>();

		TimeCard__c timeCardBillable1 = ((TimeCard__c) objectMap.get('Time Card')).clone(false, true);
		timeCardBillable1.Project__c = objectMap.get('Opportunity').Id;
		timeCardBillable1.Product__c = objectMap.get('Opportunity Line Item Custom').Id;
		timeCards.add(timeCardBillable1);

		TimeCard__c timeCardInternal1 = ((TimeCard__c) objectMap.get('Time Card')).clone(false, true);
		timeCardInternal1.Project__c = objectMap.get('Opportunity').Id;
		timeCardInternal1.Product__c = objectMap.get('Opportunity Line Item Custom').Id;
		timeCardInternal1.BillingType__c = TimecardService.TIME_CARD_BILLING_TYPE_INTERNAL;
		timeCards.add(timeCardInternal1);

		insert timeCards;

		lineItemSchedule = new OpportunityLineItemSchedule(OpportunityLineItemId = objectMap.get('Opportunity Product').Id, Revenue = 100, ScheduleDate = System.today().addDays(10), Type = 'Revenue');
		insert lineItemSchedule;

		timeCards = [
			SELECT Id, BillingType__c, OpportunityLineItemScheduleId__c
			FROM TimeCard__c
			WHERE ID IN :timeCards
			ORDER BY CreatedDate ASC
		];
		System.assertNotEquals(null, timeCards[0].OpportunityLineItemScheduleId__c, '"OpportunityLineItemScheduleId__c" should have been set for Billable Time Card');
		System.assertEquals(null, timeCards[1].OpportunityLineItemScheduleId__c, '"OpportunityLineItemScheduleId__c" should have not been set for Internal Time Card');

		Test.startTest();

		OpportunityLineItemSchedule existingLineItemSchedule = [SELECT Id, Revenue FROM OpportunityLineItemSchedule WHERE Id = :lineItemSchedule.Id LIMIT 1];
		System.assertEquals(2.5, existingLineItemSchedule.Revenue, '(10 daily rate / 8 hours per day) * 1 hour of work * 2 Billable Time Cards = 2.5'); // Internal Time Card is not calculated

		TimeCard__c timeCardBillable2 = ((TimeCard__c) objectMap.get('Time Card')).clone(false, true);
		timeCardBillable2.Project__c = objectMap.get('Opportunity').Id;
		timeCardBillable2.Product__c = objectMap.get('Opportunity Line Item Custom').Id;
		timeCardBillable2.BillingType__c = TimecardService.TIME_CARD_BILLING_TYPE_BILLABLE;
		insert timeCardBillable2;

		existingLineItemSchedule = [SELECT Id, Revenue FROM OpportunityLineItemSchedule WHERE Id = :lineItemSchedule.Id LIMIT 1];
		System.assertEquals(3.75, existingLineItemSchedule.Revenue, '(10 daily rate / 8 hours per day) * 1 hour of work * 3 Billable Time Cards = 3.75'); // Should increase Revenue as Time Card is Billable

		TimeCard__c timeCardInternal2 = ((TimeCard__c) objectMap.get('Time Card')).clone(false, true);
		timeCardInternal2.Project__c = objectMap.get('Opportunity').Id;
		timeCardInternal2.Product__c = objectMap.get('Opportunity Line Item Custom').Id;
		timeCardInternal2.BillingType__c = TimecardService.TIME_CARD_BILLING_TYPE_INTERNAL;
		insert timeCardInternal2;

		existingLineItemSchedule = [SELECT Id, Revenue FROM OpportunityLineItemSchedule WHERE Id = :lineItemSchedule.Id LIMIT 1];
		System.assertEquals(3.75, existingLineItemSchedule.Revenue, '(10 daily rate / 8 hours per day) * 1 hour of work * 3 Billable Time Cards = 3.75'); // Should not increase Revenue as Time Card is Internal

		Test.stopTest();
	}

	@istest
	private static void shouldMaintainSchedulesRevenueAndAssociationBillability_Update()
	{
		Map<String, SObject> objectMap = prepareDataSmall();
		OpportunityLineItemSchedule lineItemSchedule;

		List<TimeCard__c> timeCards = new List<TimeCard__c>();

		TimeCard__c timeCardBillable = ((TimeCard__c) objectMap.get('Time Card')).clone(false, true);
		timeCardBillable.Project__c = objectMap.get('Opportunity').Id;
		timeCardBillable.Product__c = objectMap.get('Opportunity Line Item Custom').Id;
		timeCards.add(timeCardBillable);

		TimeCard__c timeCardInternal = ((TimeCard__c) objectMap.get('Time Card')).clone(false, true);
		timeCardInternal.Project__c = objectMap.get('Opportunity').Id;
		timeCardInternal.Product__c = objectMap.get('Opportunity Line Item Custom').Id;
		timeCardInternal.BillingType__c = TimecardService.TIME_CARD_BILLING_TYPE_INTERNAL;
		timeCards.add(timeCardInternal);

		insert timeCards;

		lineItemSchedule = new OpportunityLineItemSchedule(OpportunityLineItemId = objectMap.get('Opportunity Product').Id, Revenue = 100, ScheduleDate = System.today().addDays(10), Type = 'Revenue');
		insert lineItemSchedule;

		Test.startTest();

		OpportunityLineItemSchedule existingLineItemSchedule = [SELECT Id, Revenue FROM OpportunityLineItemSchedule WHERE Id = :lineItemSchedule.Id LIMIT 1];
		System.assertEquals(2.5, existingLineItemSchedule.Revenue, '(10 daily rate / 8 hours per day) * 1 hour of work * 2 Billable Time Cards = 2.5'); // Internal Time Card is not calculated

		// From Internal to Billable
		timeCardInternal.BillingType__c = TimecardService.TIME_CARD_BILLING_TYPE_BILLABLE;
		update timeCardInternal;

		existingLineItemSchedule = [SELECT Id, Revenue FROM OpportunityLineItemSchedule WHERE Id = :lineItemSchedule.Id LIMIT 1];
		System.assertEquals(3.75, existingLineItemSchedule.Revenue, '(10 daily rate / 8 hours per day) * 1 hour of work * 3 Billable Time Cards = 3.75'); // Should increase Revenue as one of Time Cards is Billable now

		// From Billable to Internal
		timeCardBillable.BillingType__c = TimecardService.TIME_CARD_BILLING_TYPE_INTERNAL;
		update timeCardBillable;

		timeCards = [
			SELECT Id, BillingType__c, OpportunityLineItemScheduleId__c
			FROM TimeCard__c
			WHERE ID IN :timeCards
			ORDER BY CreatedDate ASC
		];
		System.assertEquals(null, timeCards[0].OpportunityLineItemScheduleId__c, '"OpportunityLineItemScheduleId__c" should have not been set for Internal Time Card');
		System.assertNotEquals(null, timeCards[1].OpportunityLineItemScheduleId__c, '"OpportunityLineItemScheduleId__c" should have been set for Billable Time Card');

		existingLineItemSchedule = [SELECT Id, Revenue FROM OpportunityLineItemSchedule WHERE Id = :lineItemSchedule.Id LIMIT 1];
		System.assertEquals(2.5, existingLineItemSchedule.Revenue, '(10 daily rate / 8 hours per day) * 1 hour of work * 2 Billable Time Cards = 2.5'); // Should decrease Revenue as one of Time Cards is Internal now

		Test.stopTest();
	}

    /**
     * @author dme
     * @description Validate proper Schedule association based on different dates. Create Schedules -> Create TCs
     */
    @istest
    private static void validateProperScheduleAssociation()
    {
        Map<String, SObject> objectMap = prepareData();
        OpportunityLineItemSchedule lineItemSchedulePast;
        OpportunityLineItemSchedule lineItemSchedule;
        OpportunityLineItemSchedule lineItemScheduleFuture;
        TimeCard__c timeCardPastPast;
        TimeCard__c timeCardPast;
        TimeCard__c timeCard;
        TimeCard__c timeCardFuture;

        Test.startTest();
        lineItemSchedulePast = new OpportunityLineItemSchedule(OpportunityLineItemId = objectMap.get('Opportunity Product').Id, Revenue = 100, ScheduleDate = System.today().addDays(-30), Type = 'Revenue');
        lineItemSchedule = new OpportunityLineItemSchedule(OpportunityLineItemId = objectMap.get('Opportunity Product').Id, Revenue = 100, ScheduleDate = System.today(), Type = 'Revenue');
        lineItemScheduleFuture = new OpportunityLineItemSchedule(OpportunityLineItemId = objectMap.get('Opportunity Product').Id, Revenue = 100, ScheduleDate = System.today().addDays(30), Type = 'Revenue');
        insert new List<OpportunityLineItemSchedule> {lineItemSchedulePast, lineItemSchedule, lineItemScheduleFuture};

        timeCardPastPast = ((TimeCard__c) objectMap.get('Time Card')).clone(false, true);
        timeCardPastPast.Project__c = objectMap.get('Opportunity').Id;
        timeCardPastPast.Product__c = objectMap.get('Opportunity Line Item Custom').Id;
        timeCardPastPast.Date__c = System.today().addDays(-35);

        timeCardPast = ((TimeCard__c) objectMap.get('Time Card')).clone(false, true);
        timeCardPast.Project__c = objectMap.get('Opportunity').Id;
        timeCardPast.Product__c = objectMap.get('Opportunity Line Item Custom').Id;
        timeCardPast.Date__c = System.today().addDays(-5);

        timeCard = ((TimeCard__c) objectMap.get('Time Card')).clone(false, true);
        timeCard.Project__c = objectMap.get('Opportunity').Id;
        timeCard.Product__c = objectMap.get('Opportunity Line Item Custom').Id;
        timeCard.Date__c = System.today().addDays(5);

        timeCardFuture = ((TimeCard__c) objectMap.get('Time Card')).clone(false, true);
        timeCardFuture.Project__c = objectMap.get('Opportunity').Id;
        timeCardFuture.Product__c = objectMap.get('Opportunity Line Item Custom').Id;
        timeCardFuture.Date__c = System.today().addDays(35);

        List<TimeCard__c> timeCards = new List<TimeCard__c>{timeCardPastPast, timeCardPast, timeCard, timeCardFuture};
        insert timeCards;

        Test.stopTest();

        List<TimeCard__c> updatedTimeCards = [SELECT Id, Date__c, OpportunityLineItemScheduleId__c FROM TimeCard__c WHERE Id IN :timeCards];
        System.assertEquals(4, updatedTimeCards.size(), '4 Timecards are expected here');
        for (TimeCard__c card : updatedTimeCards)
        {
            if (card.Id == timeCardPastPast.Id)
            {
                System.assertEquals(lineItemSchedulePast.Id, card.OpportunityLineItemScheduleId__c, 'Wrong schedule was set to this timecard');
            }
            else if (card.Id == timeCardPast.Id)
            {
                System.assertEquals(lineItemSchedulePast.Id, card.OpportunityLineItemScheduleId__c, 'Wrong schedule was set to this timecard');
            }
            else if (card.Id == timeCard.Id)
            {
                System.assertEquals(lineItemSchedule.Id, card.OpportunityLineItemScheduleId__c, 'Wrong schedule was set to this timecard');
            }
            else if (card.Id == timeCardFuture.Id)
            {
                System.assertEquals(lineItemScheduleFuture.Id, card.OpportunityLineItemScheduleId__c, 'Wrong schedule was set to this timecard');
            }
        }
    }

    /**
     * @author dme
     * @description Validate proper Schedule association based on different dates. Create TCs -> Create Schedules
     */
    @istest
    private static void validateProperScheduleAssociation2()
    {
        Map<String, SObject> objectMap = prepareData();
        OpportunityLineItemSchedule lineItemSchedulePast;
        OpportunityLineItemSchedule lineItemSchedule;
        OpportunityLineItemSchedule lineItemScheduleFuture;
        TimeCard__c timeCardPastPast;
        TimeCard__c timeCardPast;
        TimeCard__c timeCard;
        TimeCard__c timeCardFuture;

        Test.startTest();

        timeCardPastPast = ((TimeCard__c) objectMap.get('Time Card')).clone(false, true);
        timeCardPastPast.Project__c = objectMap.get('Opportunity').Id;
        timeCardPastPast.Product__c = objectMap.get('Opportunity Line Item Custom').Id;
        timeCardPastPast.Date__c = System.today().addDays(-35);

        timeCardPast = ((TimeCard__c) objectMap.get('Time Card')).clone(false, true);
        timeCardPast.Project__c = objectMap.get('Opportunity').Id;
        timeCardPast.Product__c = objectMap.get('Opportunity Line Item Custom').Id;
        timeCardPast.Date__c = System.today().addDays(-5);

        timeCard = ((TimeCard__c) objectMap.get('Time Card')).clone(false, true);
        timeCard.Project__c = objectMap.get('Opportunity').Id;
        timeCard.Product__c = objectMap.get('Opportunity Line Item Custom').Id;
        timeCard.Date__c = System.today().addDays(5);

        timeCardFuture = ((TimeCard__c) objectMap.get('Time Card')).clone(false, true);
        timeCardFuture.Project__c = objectMap.get('Opportunity').Id;
        timeCardFuture.Product__c = objectMap.get('Opportunity Line Item Custom').Id;
        timeCardFuture.Date__c = System.today().addDays(35);

        List<TimeCard__c> timeCards = new List<TimeCard__c>{timeCardPastPast, timeCardPast, timeCard, timeCardFuture};
        insert timeCards;

        lineItemSchedulePast = new OpportunityLineItemSchedule(OpportunityLineItemId = objectMap.get('Opportunity Product').Id, Revenue = 100, ScheduleDate = System.today().addDays(-30), Type = 'Revenue');
        lineItemSchedule = new OpportunityLineItemSchedule(OpportunityLineItemId = objectMap.get('Opportunity Product').Id, Revenue = 100, ScheduleDate = System.today(), Type = 'Revenue');
        lineItemScheduleFuture = new OpportunityLineItemSchedule(OpportunityLineItemId = objectMap.get('Opportunity Product').Id, Revenue = 100, ScheduleDate = System.today().addDays(30), Type = 'Revenue');
        insert new List<OpportunityLineItemSchedule> {lineItemSchedulePast, lineItemSchedule, lineItemScheduleFuture};

        Test.stopTest();

        List<TimeCard__c> updatedTimeCards = [SELECT Id, Date__c, OpportunityLineItemScheduleId__c FROM TimeCard__c WHERE Id IN :timeCards];
        System.assertEquals(4, updatedTimeCards.size(), '4 Timecards are expected here');
        for (TimeCard__c card : updatedTimeCards)
        {
            if (card.Id == timeCardPastPast.Id)
            {
                System.assertEquals(lineItemSchedulePast.Id, card.OpportunityLineItemScheduleId__c, 'Wrong schedule was set to this timecard');
            }
            else if (card.Id == timeCardPast.Id)
            {
                System.assertEquals(lineItemSchedulePast.Id, card.OpportunityLineItemScheduleId__c, 'Wrong schedule was set to this timecard');
            }
            else if (card.Id == timeCard.Id)
            {
                System.assertEquals(lineItemSchedule.Id, card.OpportunityLineItemScheduleId__c, 'Wrong schedule was set to this timecard');
            }
            else if (card.Id == timeCardFuture.Id)
            {
                System.assertEquals(lineItemScheduleFuture.Id, card.OpportunityLineItemScheduleId__c, 'Wrong schedule was set to this timecard');
            }
        }
    }

}