/**
 * @author dme
 * @copyright PARX
 */

public without sharing class TimecardService
{

	public static final String TIME_CARD_BILLING_TYPE_BILLABLE = 'Billable';
	public static final String TIME_CARD_BILLING_TYPE_INTERNAL = 'Internal';

	public static List<TimeCard__c> setTimecardSchedules(List<TimeCard__c> timeCards, Map<String, List<OpportunityLineItemSchedule>> schedulesByLineItemIdMap, Map<String, Id> oldestSchedulesByLineItemIdMap)
	{
		List<TimeCard__c> timeCardsToUpdate = new List<TimeCard__c>();
		for (TimeCard__c timeCard : timeCards)
		{
			Boolean isScheduleSet = false;
			if (timeCard.Product__c == null || timeCard.BillingType__c != TIME_CARD_BILLING_TYPE_BILLABLE)
			{
				timeCard.OpportunityLineItemScheduleId__c = null;
				timeCardsToUpdate.add(timeCard);
			}
			else if (timeCard.Product__c != null && schedulesByLineItemIdMap.get(timeCard.Product__r.OpportunityProductId__c) != null)
			{
				for (OpportunityLineItemSchedule schedule : schedulesByLineItemIdMap.get(timeCard.Product__r.OpportunityProductId__c))
				{
					if (timeCard.Date__c >= schedule.ScheduleDate)
					{
						isScheduleSet = true;
						if (timeCard.OpportunityLineItemScheduleId__c != schedule.Id)
						{
							timeCard.OpportunityLineItemScheduleId__c = schedule.Id;
							timeCardsToUpdate.add(timeCard);
						}
						break;
					}
				}

				if (!isScheduleSet)
				{
					timeCard.OpportunityLineItemScheduleId__c = oldestSchedulesByLineItemIdMap.get(timeCard.Product__r.OpportunityProductId__c);
					timeCardsToUpdate.add(timeCard);
				}
			}
		}

		return timeCardsToUpdate;
	}
}