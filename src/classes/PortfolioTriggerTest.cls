/**
* Test class for PortfolioTrigger.cls
*
* @author: <Maksim Fedorenko> (maksim.fedorenko@parx.com)
*
* @history:
* version		    | author				                                | changes
* ====================================================================================
* 0.1 31.01.2020	| Maksim Fedorenko (maksim.fedorenko@parx.com)          | initial version.
* 0.2 18.02.2020	| Christian Schwabe (Christian.Schwabe@colliers.com)    | [SFINV/US 1977] Added testmethods to test apex-validations.
* 0.3 06.04.2020	| Christian Schwabe (Christian.Schwabe@colliers.com)    | [SFINV/US 2886] Changed picklist values for matching.
*/
@IsTest
public with sharing class PortfolioTriggerTest
{
    @TestSetup
    static void setup()
    {
        Embargo__c embargo = new Embargo__c();
        embargo.Name = 'Embargo';
        embargo.Countries__c = 'Egypt;Afghanistan;Belarus;Burundi;Iraq;Yemen';
        insert embargo;

        //Prepare rollup-helper settings.
        rh2__PS_Export_Rollups__c rollupExport = new rh2__PS_Export_Rollups__c();
        rollupExport.rh2__type__c = 'rollup';
        rollupExport.rh2__RollupOrFilterData__c = '{"attributes":{"type":"rh2__Date_Sync__c"},"LastModifiedDate":"2020-02-13T09:36:41.000+0000","rh2__rollupStatus__c":"Green","rh2__relationshipField__c":"PropertyObject__c.Portfolio__c","rh2__targetFieldType__c":"DOUBLE","Name":"Portfolio__c.NumberOfProperties__c9375","rh2__relationshipFieldName__c":"Portfolio__c","rh2__targetField__c":"Portfolio__c.NumberOfProperties__c","CreatedById":"' + UserInfo.getUserId() + '","rh2__relationshipName__c":"Property_Objects__r","rh2__escapeSpecialCharacters__c":true,"rh2__sourceField__c":"PropertyObject__c.isDeleted","rh2__sourceObjectName__c":"PropertyObject__c","rh2__triggerDisabled__c":false,"rh2__Use_Custom_Rollup_Targeting__c":false,"rh2__Dedup_Flag__c":true,"rh2__Overwrite__c":true,"rh2__append__c":false,"rh2__failedRecordUpdates__c":0.0,"rh2__logic__c":"CNT","rh2__sourceFieldType__c":"BOOLEAN","rh2__Needs_Initialized__c":false,"rh2__Only_Use_First_Matching_Result__c":false,"IsDeleted":false,"rh2__targetFieldLength__c":0.0,"rh2__Async__c":false,"rh2__Remove_Trailing_Delimeters__c":true,"rh2__Blank_Overwrite__c":true,"rh2__queryArchive__c":false,"rh2__SourceFieldObject__c":"PropertyObject__c","rh2__description__c":"Aggregate all child records from PropertyObject__c to Portfolio__c.","CurrencyIsoCode":"EUR","SystemModstamp":"2020-02-13T09:36:41.000+0000","rh2__Needs_Run_After_Criteria_Change__c":false,"rh2__targetObjectName__c":"Portfolio__c","rh2__conditional__c":true,"rh2__sourceFieldName__c":"isDeleted","CreatedDate":"2020-02-13T09:36:28.000+0000","rh2__isRealTime__c":false,"rh2__isValidTriggerObject__c":false,"rh2__RollupLabel__c":"Aggregate Objects.","rh2__disable_multipicklist_split__c":false,"rh2__targetFieldName__c":"NumberOfProperties__c","LastModifiedById":"' + UserInfo.getUserId() + '","rh2__Disable_Realtime__c":false}';
        insert rollupExport;
    }

    @IsTest
    static void copyDataToOpportunityTest()
    {
        Portfolio__c portfolio = new Portfolio__c(
                WALT__c = 1, GrossYield__c = 2, NetYield__c = 3, FactorActual__c = 4, FactorTarget__c = 5, IRR__c = 6, CoC__c = 7, AmountOfSpaceLet__c = 8,
                SqmPlot__c = 9, SqmInTotal__c = 10
        );
        insert portfolio;

        Opportunity opportunity = TestDataFactory.createOpportunity(null);
        opportunity.PortfolioLookup__c = portfolio.Id;
        insert opportunity;

        portfolio.WALT__c = 2;
        portfolio.NetYield__c = 4;
        portfolio.FactorTarget__c = 6;
        portfolio.CoC__c = 8;
        update portfolio;

        Opportunity updatedOpportunity = [
                SELECT Id, WALTInYears__c, GrossYield__c, NetYield__c, FactorActual__c, FactorTarget__c, IRR__c, CoC__c, AmountOfSpaceLet__c, PlotSqm__c,
                        TotalSqm__c, UnitMeasurement__c
                FROM Opportunity
                WHERE Id = :opportunity.Id
        ];

        System.assertEquals(portfolio.WALT__c, updatedOpportunity.WALTInYears__c, 'The values should be equal');
        System.assertEquals(portfolio.GrossYield__c, updatedOpportunity.GrossYield__c, 'The values should be equal');
        System.assertEquals(portfolio.NetYield__c, updatedOpportunity.NetYield__c, 'The values should be equal');
        System.assertEquals(portfolio.FactorActual__c, updatedOpportunity.FactorActual__c, 'The values should be equal');
        System.assertEquals(portfolio.FactorTarget__c, updatedOpportunity.FactorTarget__c, 'The values should be equal');
        System.assertEquals(portfolio.IRR__c, updatedOpportunity.IRR__c, 'The values should be equal');
        System.assertEquals(portfolio.CoC__c, updatedOpportunity.CoC__c, 'The values should be equal');
        System.assertEquals(portfolio.AmountOfSpaceLet__c, updatedOpportunity.AmountOfSpaceLet__c, 'The values should be equal');
        System.assertEquals(portfolio.SqmPlot__c, updatedOpportunity.PlotSqm__c, 'The values should be equal');
        System.assertEquals(portfolio.SqmInTotal__c, updatedOpportunity.TotalSqm__c, 'The values should be equal');
    }

    /**
     * Test apex validation rule on Portfolio__c-Trigger.
     * Allow update on Portfolio__c if no PropertyObject__c is related.
     */
    @IsTest
    static void testAllowEditOnPortfolioWithoutRelatedObjects(){
        Portfolio__c portfolio = new Portfolio__c();
        portfolio.Name = 'Test-Portfolio';
        insert portfolio;

        portfolio.Markets__c = 'Frankfurt';
        update portfolio;

        portfolio = [SELECT Id, Markets__c, NumberOfProperties__c FROM Portfolio__c WHERE Id = :portfolio.Id];
        System.assertEquals('Frankfurt', portfolio.Markets__c);
        System.assertEquals(null, portfolio.NumberOfProperties__c);
    }

    /**
     * Test apex validation rule on Portfolio__c-Trigger.
     * Don't allow direct update on Portfolio__c if there are more than one PropertyObject related.
     * The aggregation should continue to work.
     */
    @IsTest
    static void testAllowAggregationOnPortfolioWithRelatedObjects(){
        Portfolio__c portfolio = new Portfolio__c();
        portfolio.Name = 'Test-Portfolio';
        insert portfolio;

        Test.startTest();
            PropertyObject__c propertyObject1 = new PropertyObject__c();
            propertyObject1.Portfolio__c = portfolio.Id;
            propertyObject1.Country__c = 'Germany';
            propertyObject1.Market__c = 'Frankfurt';
            propertyObject1.Submarket__c = 'BER Adlershof';
            propertyObject1.FederalState__c = 'Hessen';
            propertyObject1.MacroLocation__c = 'B&C Cities';
            propertyObject1.MicroLocation__c = 'A-Location';
            insert propertyObject1;
        Test.stopTest();

        portfolio = [SELECT Id, Markets__c, PreventPartialChanging__c, NumberOfProperties__c FROM Portfolio__c WHERE Id = :portfolio.Id];
        System.assertEquals(1, portfolio.NumberOfProperties__c);

        portfolio.Markets__c = 'Berlin';
        Database.SaveResult saveResult = Database.update(portfolio, false);

        System.assertEquals(1, saveResult.getErrors().size(), 'There should be exact one apex-validation rule triggered.');
        Database.Error databaseError = saveResult.getErrors()[0];
        System.assertEquals(System.Label.PreventPartialChangeOnPortfolio , databaseError.getMessage());
    }
}