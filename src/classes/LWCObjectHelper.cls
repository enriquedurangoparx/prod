/**
* Helperclass to select related objects for area and portfolio.
*
* @author: <Christian Schwabe> (Christian.Schwabe@colliers.com)
*
* @history:
* version           | author                                                | changes
* ====================================================================================
* 0.1 25.11.2019    | Christian Schwabe (Christian.Schwabe@colliers.com)    | [INV/US 1243] initial version.
* 0.2 26.11.2019    | Christian Schwabe (Christian.Schwabe@colliers.com)    | [INV/US 1271] add sosl-query.
* 0.3 30.01.2020    | Christian Schwabe (Christian.Schwabe@colliers.com)    | [INV/BUG 2134] change annotation cacheable from true to false for searchRelatedObjectsBySoql.

*/
public with sharing class LWCObjectHelper {
    public LWCObjectHelper() {

    }

    @AuraEnabled(cacheable = false)
    public static List<PropertyObject__c> searchRelatedObjectsBySoql(Id recordId, String objApiName){
        System.debug('recordId: ' + recordId);
        List<PropertyObject__c> propertyList = new List<PropertyObject__c>();
        
        String query = 'SELECT {0} FROM PropertyObject__c WHERE ';
        if (objApiName == 'Portfolio__c')
        {
            query += 'Portfolio__c = :recordId AND ';
        }
        else if (objApiName == 'Area__c')
        {
            query += 'Area__c = :recordId AND ';
        }
        query += '((   Geolocation__Latitude__s != null AND Geolocation__Longitude__s != null  ) OR (   Street__c != null OR PostalCode__c != null OR City__c != null OR Country__c != null ))';
        
        Set<String> selectFieldsSet = new Set<String>{'Id', 'Name', 'MainThumbnail__c', 'Geolocation__Latitude__s', 'Geolocation__Longitude__s', 'Street__c', 'PostalCode__c', 'City__c', 'Country__c', 'PropertyOwner__c', 'PropertyOwner__r.Name', 'SqmInTotal__c', 'VacancySqm__c'};
        String fieldsString = String.join(new List<String>(selectFieldsSet), ', ');
        query = String.format(query, new List<String>{fieldsString});

        if (recordId != null)
        {
            System.debug(query);
            propertyList = (List<PropertyObject__c>) Database.query(query);
        }
        return propertyList;
    }

    @AuraEnabled
    public static List<List<sObject>> searchRelatedObjectsBySosl(String searchTerm){
        return [FIND :searchTerm IN ALL FIELDS RETURNING PropertyObject__c(Name, MainThumbnail__c, Street__c, PostalCode__c, City__c, Country__c, Status__c, SqmInTotal__c, VacancySqm__c, Area__c, Area__r.Name, Portfolio__c, Portfolio__r.Name, PropertyOwner__c, PropertyOwner__r.Name)];
    }

    /**
     * Get records for initial state for relatedObjects LWC component
     * @param  queryFields          API names of fields that involved in query  
     * @param  filteredField        API name of field by which we will filter
     * @param  recordId             Id of the record
     * @return List<PropertyObject__c> list of records
     */
    @AuraEnabled
    public static List<PropertyObject__c> getInitialState(List<String> queryFields, String filteredField, Id recordId)
    {
        String query = 'SELECT {0} FROM PropertyObject__c ';
        String fieldsString = String.join(queryFields, ', ');
        query = String.format(query, new List<String>{fieldsString});
        
        if (String.isNotBlank(filteredField))
        {
            query += 'WHERE ' + filteredField + ' = :recordId';
        }
        System.debug('query: ' + query);
        List<PropertyObject__c> objectsList = Database.query(query);
        System.debug('objectsList: ' + JSON.serialize(objectsList));


        
        return objectsList;
    }

    // updated : ema : added error handling
    @AuraEnabled
    public static String updateObjectsRelation(List<PropertyObject__c> objects, Id recordId, String objApiName)
    {
        System.debug('objects: ' + objects);

        for (PropertyObject__c obj : objects)
        {
            if (objApiName == 'Area__c')
            {
                obj.Area__c = recordId;
            }
            else if (objApiName == 'Portfolio__c')
            {
                obj.Portfolio__c = recordId;
            }
        }
        try {
            update objects;
            return 'success';
        } 
        catch (Exception e) {
            return e.getMessage();
        }
    }
}