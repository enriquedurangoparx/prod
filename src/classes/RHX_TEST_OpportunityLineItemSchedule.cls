@isTest//(SeeAllData=true)
private class RHX_TEST_OpportunityLineItemSchedule {
	@isTest
    private static void RHX_Testmethod() {

        Embargo__c embargo = new Embargo__c();
        embargo.Name = 'Embargo';
        embargo.Countries__c = 'Egypt;Afghanistan;Belarus;Burundi;Iraq;Yemen';
        insert embargo;

        Account account = new Account();
        account.Name = 'NewAcc';
        insert account;

        Pricebook2 pricebook = new Pricebook2();
        pricebook.Name = 'Valuation';
        pricebook.IsActive = true;
        insert pricebook;

        Product2 product =  new Product2();
        product.Name = 'NewProd';
        product.IsActive = true;
        product.CanUseRevenueSchedule = true;
        insert product;

        PricebookEntry standardPricebookEntry = new PricebookEntry();
        standardPricebookEntry.Product2Id = product.Id;
        standardPricebookEntry.IsActive = true;
        standardPricebookEntry.UnitPrice = 70;
        standardPricebookEntry.Pricebook2Id = Test.getStandardPricebookId();
        standardPricebookEntry.UseStandardPrice = false;

        PricebookEntry pricebookEntry = new PricebookEntry();
        pricebookEntry.Product2Id = product.Id;
        pricebookEntry.IsActive = true;
        pricebookEntry.UnitPrice = 70;
        pricebookEntry.Pricebook2Id = pricebook.Id;
        pricebookEntry.UseStandardPrice = false;
        insert new List<PricebookEntry>{ standardPricebookEntry, pricebookEntry };

        PropertyObject__c property = new PropertyObject__c();
        property.Name = 'Test Property';
        insert property;

        Opportunity opportunity = new Opportunity();
        opportunity.RecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('ValuationAcquisition').getRecordTypeId();
        opportunity.Name = 'NewOpp';
		opportunity.FrameworkContractType__c = 'Preferred in Panel';
    	opportunity.FrameworkContractPanel__c = 'Germany';
    	opportunity.Region__c = 'EMEA';
    	opportunity.Country__c = 'Germany';
        opportunity.AccountId = account.Id;
        opportunity.StageName =  SObjectType.Opportunity.fields.StageName.getPicklistValues().get(0).getValue();
        opportunity.ProjectStart__c = Date.today();
        opportunity.CloseDate = Date.today().addDays(10);
        opportunity.PrimaryProperty__c = property.Id;
        insert opportunity;

        OpportunityLineItem opportunityLineItem = new OpportunityLineItem();
        opportunityLineItem.UnitPrice = 57;
        opportunityLineItem.Quantity = 12;
        opportunityLineItem.OpportunityId = opportunity.Id;
        opportunityLineItem.PricebookEntryId = pricebookEntry.id;
        insert opportunityLineItem;

        OpportunityLineItemSchedule opportunityLinteItemSchedule = new OpportunityLineItemSchedule();
        opportunityLinteItemSchedule.OpportunityLineItemId = opportunityLineItem.Id;
        opportunityLinteItemSchedule.Type = 'Revenue';
        opportunityLinteItemSchedule.Revenue = 1000;
        opportunityLinteItemSchedule.ScheduleDate = Date.today().addDays(2);
        insert opportunityLinteItemSchedule;

        /*List<sObject> sourceList = [SELECT Id 
			FROM OpportunityLineItemSchedule ORDER BY LastModifiedDate DESC LIMIT 1];
        if(sourceList.size() == 0) {
            sourceList.add(
                    new OpportunityLineItemSchedule()
            );
        }*/
    	rh2.ParentUtil.UpsertRollupTestRecords( new List<OpportunityLineItemSchedule> { opportunityLinteItemSchedule } );//sourceList
    }
}