/**
*
*	@author npo
*	@copyright PARX
*/
({
    handleClick : function (cmp, event, helper) {
		console.log('...navigating');
		var evt = $A.get("e.force:navigateToComponent");
		console.log('...evt: ' + evt);
		evt.setParams({
			componentDef: "c:mobileSetObjectGeolocationAction",
				componentAttributes : {
					"recordId" : cmp.get("v.recordId")
				}
			});

	evt.fire();
    }
});