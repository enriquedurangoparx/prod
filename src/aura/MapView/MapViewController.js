/**
 * Created by yoon on 18.01.19.
 */
({
    init : function(component, event, helper) {
        console.log('init' + component.get('v.sObjectArray'));
        console.log('init' + JSON.stringify(component.get('v.sObjectArray')));
        var action = component.get("c.getLocations");
            action.setParam("currentId", component.get("v.recordId"));
            action.setParam("sObjectList",  JSON.stringify(component.get('v.sObjectArray')));
            action.setParam("sObjectName",  component.get('v.listsObjectName'));
            action.setParam("fields",  component.get('v.FieldsOnMap'));
            action.setParam("geolocationField",  component.get('v.GeoLocationField'));
            action.setCallback(this, function(response) {
                console.log('response'+response);
                var state = response.getState();
                console.log(state);
                if (state == "SUCCESS") {
                    var obj =response.getReturnValue() ;
                    console.log(obj);
                    component.set('v.mapMarkers',obj);
                }

            });
            $A.enqueueAction(action);

    },
    objectListChange : function (component, event, helper) {
		console.log('change' + component.get('v.sObjectArray'));
		var action = component.get("c.getLocations");
        	action.setParam("currentId", component.get("v.recordId"));
            action.setParam("sObjectList",  JSON.stringify(component.get('v.sObjectArray')));
            action.setParam("sObjectName",  component.get('v.listsObjectName'));
            action.setParam("fields",  component.get('v.FieldsOnMap'));
            action.setParam("geolocationField",  component.get('v.GeoLocationField'));
                    action.setCallback(this, function(response) {
                        console.log('response'+response);
                        var state = response.getState();
                        console.log(state);
                        if (state == "SUCCESS") {
                            var obj =response.getReturnValue() ;
                            console.log(obj);
                            component.set('v.mapMarkers',"");
                            component.set('v.mapMarkers',obj);
                        }

                    });
                    $A.enqueueAction(action);
    }
})