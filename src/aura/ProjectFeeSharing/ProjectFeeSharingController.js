/**
 * Created by ille on 2019-03-13.
 */
({
    onInit: function (component, event, helper) {
        helper.initialize(component);
    },

    onSave: function (component, event, helper) {
        helper.onSave(component);
    },

    onNew: function (component, event, helper) {
        helper.onNew(component);
    },

    onCancel: function (component, event, helper) {
        helper.onCancel(component);
    },

    onDelete: function (component, event, helper) {
        helper.onDelete(component, event);
    },

    onPercentChange: function (component, event, helper) {
        if (!isNaN(Math.abs(event.getSource().get('v.value'))) && Math.floor(event.getSource().get('v.value')) < 0)
        {
            event.getSource().set('v.value', Math.abs('' + event.getSource().get('v.value')));
        }

        helper.recalculateSum(component);
    }
})