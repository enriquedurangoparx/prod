/**
* Apex Trigger for FeeSharing__c records.
*
* @author: <Nadzeya Polautsava> (nadzeya.polautsava@parx.com)
*
* @history:
* version		    | author				                                | changes
* ====================================================================================
* 0.1 09.04.2020	| Nadzeya Polautsava (nadzeya.polautsava@parx.com)      | initial version.
*/
trigger onFeeSharing on FeeSharing__c (before insert, before update, before delete, after insert, after update, after delete, after undelete)
{
    FeeSharingTrigger handler = new FeeSharingTrigger();
    handler.disableForUserWithPermission(ATrigger.CAN_SKIP_TRIGGERS_PERMISSION_NAME);
    handler.execute();

}