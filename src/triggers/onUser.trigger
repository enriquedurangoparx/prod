/**
 * Created by dyas on 2019-05-31.
 */

trigger onUser on User (before insert, before update, before delete, after insert, after update, after delete)
{
    new UserTrigger().execute();
}