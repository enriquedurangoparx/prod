/**
* Apex Trigger for AccountToOpportunity__c.
*
* @author: <Egor Markuzov> (egor.markuzov@colliers.com)
*
* @history:
* version		    | author				                                | changes
* ====================================================================================
* 0.1 20.02.2020	| Egor Markuzov (egor.markuzov@colliers.com)	        | initial version.
*/
trigger onInvestor on AccountToOpportunity__c(before insert, before update, before delete, after insert, after update, after delete) {
    new AccountToOpportunityTrigger().execute();
}